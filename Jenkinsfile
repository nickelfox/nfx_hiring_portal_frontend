def COLOR_MAP = [
    'SUCCESS': 'good',
    'FAILURE': 'danger',
    'UNSTABLE': 'warning'
]

def MESSAGES_MAP = [
    'SUCCESS': "success",
    'FAILURE': "failed",
    'UNSTABLE': "unstable"
]

def NHP_DEV_BUCKET = 'dev.nhp.foxlabs.in'
def NHP_QA_BUCKET = 'qa.nhp.foxlabs.in'
def NHP_MASTER_BUCKET = 'admin.assessment.foxlabs.in'
def NHP_DEV_DISTRIBUTION = 'E2KUU0UWDBDVVJ'
def NHP_QA_DISTRIBUTION = 'E238Q87HA1U2U4'
def NHP_MASTER_DISTRIBUTION = 'E2EET6FC2FDF6J'
def AWS_REGION = 'ap-south-1'
def AWS_CREDENTIAL_ID = 'nhp-aws-iam'

pipeline {
    agent any
    options {
        // fail build if takes more than 30 minutes, mark reason as build timeout
        timeout(time: 20, unit: 'MINUTES')
        // Keep the 10 most recent builds
        buildDiscarder(logRotator(numToKeepStr: '10'))
        // Don't run any stage if found to be unstable
        skipStagesAfterUnstable()
    }
    environment {
        CI                      = "false"
        DISPLAY_SERVICE_NAME    = 'NFX Hiring Portal'
        npm_config_cache        = 'npm-cache'
        BRANCH_NAME             = sh(script: "echo ${env.BRANCH_NAME}", returnStdout: true).trim()
    }
    stages {
        stage('SCM Checkout') {
            steps {
                checkout scm
            }
        }
        stage("Environment Variables") {
            steps {
                sh "printenv"
            }
        }
        stage('Install Ckeditor Dependencies') {
            steps {
                echo 'NPM install Ckeditor dependencies.'
                sh   'cd ckeditor5 && npm install && npm run build'
            }
        }
        stage('Install Dependencies') {
            steps {
                echo 'NPM install dependencies.'
                sh   'npm install'
            }
        }
        stage('Build Application') {
            parallel {
                stage('Building Dev Artifacts') {
                    when {
                        branch 'develop'
                    }
                    steps {
                        sh 'CI=false npm run build:dev'
                    }
                }
                stage('Building QA Artifacts') {
                    when {
                        branch 'qa'
                    }
                    steps {
                        sh 'CI=false npm run build:qa'
                    }
                }
                stage('Building MASTER Artifacts') {
                    when {
                        branch 'master'
                    }
                    steps {
                        sh 'CI=false npm run build:prod'
                    }
                }
            }
        }
        stage('Pushing artifact to AWS S3') {
            parallel {
                    stage('Deploying on dev environment') {
                        when {
                            branch 'develop'
                        }
                        steps {
                            withAWS(region: AWS_REGION, credentials: AWS_CREDENTIAL_ID) {
                              s3Delete(bucket: NHP_DEV_BUCKET, path:'**/*')
                              s3Upload(bucket: NHP_DEV_BUCKET, workingDir:'build', includePathPattern:'**/*');
                              cfInvalidate(distribution: NHP_DEV_DISTRIBUTION, paths:['/*'], waitForCompletion: true);
                            }
                        }
                    }
                    stage('Deploying to QA Environment') {
                        when {
                            branch 'qa'
                        }
                        steps {
                            withAWS(region: AWS_REGION, credentials: AWS_CREDENTIAL_ID) {
                              s3Delete(bucket: NHP_QA_BUCKET, path:'**/*')
                              s3Upload(bucket: NHP_QA_BUCKET, workingDir:'build', includePathPattern:'**/*');
                              cfInvalidate(distribution: NHP_QA_DISTRIBUTION, paths:['/*'], waitForCompletion: true);
                            }
                        }
                    }
                    stage('Deploying to MASTER Environment') {
                        when {
                            branch 'master'
                        }
                        steps {
                            withAWS(region: AWS_REGION, credentials: AWS_CREDENTIAL_ID) {
                              s3Delete(bucket: NHP_MASTER_BUCKET, path:'**/*')
                              s3Upload(bucket: NHP_MASTER_BUCKET, workingDir:'build', includePathPattern:'**/*');
                              cfInvalidate(distribution: NHP_MASTER_DISTRIBUTION, paths:['/*'], waitForCompletion: true);
                            }
                        }
                    }
                }
        }
    }
    // post process for jenkins build, notify on slack and email
    post {
        success {
            // slackSend channel: SLACK_CHANNEL, color: COLOR_MAP[currentBuild.currentResult], message: "*${currentBuild.currentResult}:* Job - ${env.JOB_NAME} ${env.BUILD_NUMBER} successfully deployed, logs (<${env.BUILD_URL}|Open>)"
            // mail to:"me@example.com", subject:"SUCCESS: ${currentBuild.fullDisplayName}", body: "Yay, we passed."
            echo 'Success!'
        }
        failure {
            // slackSend channel: SLACK_CHANNEL, color: COLOR_MAP[currentBuild.currentResult], message: "*${currentBuild.currentResult}:* Job - ${env.JOB_NAME} ${env.BUILD_NUMBER} failed to deploy, logs (<${env.BUILD_URL}|Open>)"
            echo 'Failed!'
        }
        unstable {
            // slackSend channel: SLACK_CHANNEL, color: COLOR_MAP[currentBuild.currentResult], message: "*${currentBuild.currentResult}:* Job - ${env.JOB_NAME} ${env.BUILD_NUMBER} failed to build: unstable, logs (<${env.BUILD_URL}|Open>)"
            echo 'Unstable!'
        }
        always {
          deleteDir()
        }
    }
}
