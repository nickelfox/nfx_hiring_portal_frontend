// importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
// importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/8.7.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.7.1/firebase-messaging.js');

firebase.initializeApp({
    messagingSenderId: "1023411089931",
    authDomain: "hiring-portal-3327d.firebaseapp.com",
    projectId: "hiring-portal-3327d",
    storageBucket: "hiring-portal-3327d.appspot.com",
    appId: "1:1023411089931:web:813184852aa1c502f50d0e",
    measurementId: "G-FP8GYL4HLQ",
    databaseURL: "https://hiring-portal-3327d-default-rtdb.firebaseio.com",
    apiKey: "AIzaSyBHArfh7rORlzonz0xnVfGpxy4k38tz96s",
});

const messaging = firebase.messaging();

