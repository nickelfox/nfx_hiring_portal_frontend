import firebase from 'firebase';

const config = {
    messagingSenderId: "1023411089931",
    authDomain: "hiring-portal-3327d.firebaseapp.com",
    projectId: "hiring-portal-3327d",
    storageBucket: "hiring-portal-3327d.appspot.com",
    appId: "1:1023411089931:web:813184852aa1c502f50d0e",
    measurementId: "G-FP8GYL4HLQ",
    databaseURL: "https://hiring-portal-3327d-default-rtdb.firebaseio.com",
    apiKey: "AIzaSyBHArfh7rORlzonz0xnVfGpxy4k38tz96s",
}


if (firebase.messaging.isSupported()) {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    } else {
      firebase.app(); // if already initialized, use that one
    }
  }
// firebase.initializeApp(config)
// const messaging = firebase.messaging()

export const onMessageListener = () =>
new Promise((resolve, reject) => {
    if (firebase.messaging.isSupported()) {
      firebase.messaging().onMessage((payload) => {
        resolve(payload);
      });
    } else {
      reject(false);
    }
    });

