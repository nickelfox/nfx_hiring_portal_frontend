import  {useHistory,clsx,CssBaseline,Container,Grid,Paper ,Topbar,Sidebar,React, makeStyles, useEffect, apiUrl, useState, fetchUpdate} from 'allImport';
import fetchGet from 'apiFetch/fetchGet';
import {useStyles} from "style/Liststyle.js"
import { withSnackbar } from 'notistack';
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
const useNotificationStyles = makeStyles((theme) => ({
    notificationContainer: {
        display: "flex",
        padding: "30px",
        borderBottom: "1px solid #E5E5E5",
        "&:hover": {
            backgroundColor: "#D9001D0F",
        }
    },
    leftContainer: {
        height: "60px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        flexGrow: 1,
        cursor: "pointer"
    },
    rightContainer: {
        width: 90,
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    notificationRead: {
        fontFamily: "Open Sans",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "700",
        lineHeight: "21px",
        letterSpacing: "0.10000000149011612px",
        color: "#000000DE",
        textDecoration: "underline",
    },
    notificationDetail: {
        fontFamily: "Open Sans",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "32px",
        letterSpacing: "0.10000000149011612px",
        color: "#000000DE",
        textDecoration: "underline",
    },
    notificationDateRead: {
        fontFamily: "Open Sans",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "700",
        lineHeight: "32px",
        letterSpacing: "0.10000000149011612px",
        textDecoration: "underline"
    },
    notificationDate: {
        fontFamily: "Open Sans",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: "32px",
        letterSpacing: "0.10000000149011612px",
        textDecoration: "underline"
    },
    profileText: {
        fontFamily: "Open Sans",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "21px",
        letterSpacing: "0px",
        color: "#0000005C",
    },
    emptyBox: {
        maxWidth: "700px",
        margin: "50px 15px"
    },
    emptyContainer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "50vh"
    },
    notificationTextExternal: {
        fontFamily: "Mulish",
        fontSize: "20px",
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: "22px",
        letterSpacing: "0.25px",
        color: "#383838",
        padding: "15px 0 0 10px",
    },
    containerExternal: {
        margin: "10px 0",
        padding: 10,
        "&:hover": {
            backgroundColor: "#D9001D0F",
        }
    },
    emptyContainerExternal: {
        width: "100%",
        height: "600px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    emptyBoxExternal: {
        width: "90%",
    }
}))

const NotificationList = (props) => {
    const classes = useStyles();
    const notiClasses = useNotificationStyles();
    const [count, setCount] = useState(0);
    const [notification, setNotification] = useState([]);
    const history=useHistory()
    const [topbarVisible, setTopBarVisible] = useState(true);
    const role=localStorage.getItem('ROLE')?localStorage.getItem('ROLE').toLowerCase():null;
    const [dashboardNotifications, setDashboardNotifications] = useState([]);

    const markAsRead = (payload, id) => {
        console.log("read")
        const data = {
            logs: [id]
        }
        fetchUpdate.patch(apiUrl.notificationMarkAsRead, data)
        history.push({
            pathname: `/${role}/candidate/profile`,
            state: {
                candidateID: JSON.parse(payload.replace(/'/g, '"')).notification.candidate_id,
            },
        })
    }

    useEffect(() => {
        if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
            history.push("/login");
        }
        fetchGet.get(`${apiUrl.getNotifications}`).then((res) => {
            let notifications = res.data.data;
            notifications?.reverse();
            let dashboardNotifications = [];
            for(let i=0; i<10 && i<notifications.length; i++){
                dashboardNotifications.push(notifications[i]);
            }
            setDashboardNotifications(dashboardNotifications);
            setNotification(notifications)
            setCount(res.data.data.length);
        })
    }, [])

    // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
           setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])

    return (
        <>
        {props.external ? 
            <div>
                <div className={notiClasses.notificationTextExternal}>Notifications</div>
                {notification.length === 0 ? <Paper className={clsx(notiClasses.emptyContainerExternal)}><img className={notiClasses.emptyBoxExternal} alt="No Notifications Found" src="/assets/Icons/EmptyIconNotification.svg" /></Paper> :
                <>{dashboardNotifications.map((i, index) => (
                    <div key={index} className={notiClasses.containerExternal}>
                        <div onClick={() => markAsRead(i.payload, i.id)} className={notiClasses.leftContainer} style={{height: "auto"}}>
                            <div className={i.mark_as_read ? notiClasses.notificationDetail : notiClasses.notificationRead}>
                                {i.title}
                            </div>
                            <div className={notiClasses.profileText}>Go to Profile</div>
                        </div>
                    </div>
                ))}</>}
            </div>:
        <div className={classes.root}>
        <CssBaseline />
        {topbarVisible ? <Topbar notification={true}/> : null}
        <Sidebar />
        <main className={classes.content}  id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
        <Grid container >
        <Grid item lg={3} xl={3} md={3} sm={3} >
           <div style={{display:"flex", justifyContent:"left", alignItems:"center"}}><div className = {classes.page_heading}>
           Notifications
           </div>
           <div className={classes.examiner_nos} style={{marginLeft:"20px"}}>{count}</div></div>
         </Grid>
        </Grid>
            {notification.length === 0 ? <Paper className={clsx(classes.paper, notiClasses.emptyContainer)}><img className={notiClasses.emptyBox} alt="No Notifications Found" src="/assets/Icons/EmptyIconNotification.svg" /></Paper> :
            <Paper className={classes.paper}>

                {notification.map((i, index) => (
                    <div key={index} onClick={() => markAsRead(i.payload, i.id)} className={notiClasses.notificationContainer}>
                        <div className={notiClasses.leftContainer}>
                            <div className={i.mark_as_read ? notiClasses.notificationDetail : notiClasses.notificationRead}>
                                {i.title}
                            </div>
                            <div className={notiClasses.profileText}>Go to Profile</div>
                        </div>
                        <div className={clsx(notiClasses.rightContainer, i.mark_as_read ? notiClasses.notificationDate : notiClasses.notificationDateRead )}>
                            {new Date(i.created_at).toLocaleDateString('en-GB', {day: '2-digit', month: 'short', year: 'numeric'})}
                        </div>
                    </div>
                ))}

            </Paper>}
            </Container>
            </main>
        </div>}</>
    )
}

export default withSnackbar(NotificationList);
 