import { Zoom, fetchUpdate, apiUrl, useHistory, InputAdornment, VisibilityOff, Visibility, IconButton, Formik, Field, Yup, Form, Grid, makeStyles, Button, Typography, React } from "allImport";
import { TextField } from "formik-material-ui";
import { commonStyle } from "commonStyle.js";
import { withSnackbar } from "notistack";
const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),

  changepass_heading: {
    fontFamily: "Mulish",
    fontSize: "50px",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "63px",
    width: "435px",
    height: "63px",
    marginLeft: "120px",
    marginTop: "269px",

    color: "#18273B",
  },
  buttonbox: {
    marginLeft: "120px",
    paddingTop: "32px",
  },
  button: {
    color: "#FFFFFF",
    background: " #D9001D",
    height: "52px",
    width: "157px",
    left: "0px",
    top: "0px",
    borderRadius: "4px",
  },
  siginbox: {
    width: "430px",
    height: "63px",
  },
  newpasswordlabel: {
    marginLeft: "120px",
    marginTop: "320 px",
    color: "#444444",
    fontFamily: "Open Sans",
    fontSize: "15px",
    fontStyle: "normal",
    fontWeight: " 400",
    lineHeight: " 24px",
    letterSpacing: "0.5px",
    textAlign: "left",
  },
  passwordlabel: {
    marginLeft: "120px",
    marginTop: "32px",
    color: "#444444",
    fontFamily: "Open Sans",
    fontSize: "15px",
    fontStyle: "normal",
    fontWeight: " 400",
    lineHeight: " 24px",
    letterSpacing: "0.5px",
    textAlign: "left",
  },
  emailbox: {
    paddingTop: "10px",
    marginLeft: "120px",
    paddingBottom: "39px",
    width: "540px",
    height: "46px",
  },
  passwordbox: {
    marginLeft: "120px",
    marginTop: "10px",
    width: "540px",
    height: "46px",
  },

  inputfeedback: {
    marginTop: "10px",
    marginLeft: "120px",
  },

  form: {
    marginTop: "32px",
  },
}));

const Resetpassword = (props) => {
  const classes = useStyles();
  const [showPassword, setShowPassword] = React.useState(false);
  const [showPassword1, setShowPassword1] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);
  const handleClickShowPassword1 = () => setShowPassword1(!showPassword1);
  const handleMouseDownPassword1 = () => setShowPassword1(!showPassword1);
  const history = useHistory();
  return (
    <Grid container style={{ maxHeight: "100vh", overflow: "auto" }} lg={12} xl={12} md={12}>
      <Grid item lg={5} xl={5} md={6} sm={7}>
        <div className={classes.leftbox}>
          <img src="/assets/Icons/Nickelfoxlogo.svg" alt="nickelfoxlogo" className={classes.nickelfoxlogo} />
          <Typography className={classes.heading}>NickelFox</Typography>
          <Typography className={classes.subtext}>Hiring Portal</Typography>
          <Typography className={classes.leftbigtext}>Welcome Back!</Typography>
          <Typography className={classes.lefttext}>Update your password and log in to continue recruiting.</Typography>
        </div>
      </Grid>
      <Grid item lg={7} xl={7} md={6} sm={5}>
        <Typography variant="h3" className={classes.changepass_heading}>
          Reset Password
        </Typography>

        <Formik
          initialValues={{ new_password: "", retype_password: "" }}
          onSubmit={(values, { resetForm, setSubmitting }) => {
            var data = JSON.stringify({
              new_password: values.new_password,
              retype_password: values.retype_password,
            });

            fetchUpdate
              .patch(`${apiUrl.resetPassword}/${props.match.params.id}`, data)

              .then((response) => {
                props.enqueueSnackbar(response.data.data.message, {
                  variant: "success",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                  TransitionComponent: Zoom,
                });

                history.push("/login");
              })
              .catch((error) => {
                console.log(error);
              });
            resetForm();
          }}
          validationSchema={Yup.object().shape({
            new_password: Yup.string()
              .required("No password provided.")
              .min(7, "Password is too short - should be 7 chars minimum.")
              .matches(/(?=.*[0-9])/, "Password must contain a number."),
            retype_password: Yup.string()
              .oneOf([Yup.ref("new_password")], "Password does not match")
              .required("Required"),
          })}
        >
          {(props) => {
            const { touched, errors, values, handleChange, handleBlur, handleSubmit } = props;
            return (
              <Form onSubmit={handleSubmit} className={classes.form}>
                <label htmlFor="password" className={classes.newpasswordlabel}>
                  New Password*
                </label>{" "}
                <br />
                <Field
                  id="password"
                  autoComplete="off"
                  placeholder="Enter Your Password"
                  name="new_password"
                  value={values.new_password}
                  component={TextField}
                  margin="none"
                  variant="outlined"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={touched.new_password ? errors.new_password : ""}
                  error={touched.new_password && Boolean(errors.new_password)}
                  type={showPassword ? "text" : "password"}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}>
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  className={classes.emailbox}
                />
                <br />
                <label htmlFor="password" className={classes.passwordlabel}>
                  Re-enter Password*
                </label>{" "}
                <br />
                <Field
                  id="confirmpassword"
                  autoComplete="off"
                  placeholder="Re-Enter Your Password"
                  name="retype_password"
                  value={values.retype_password}
                  component={TextField}
                  margin="normal"
                  variant="outlined"
                  onBlur={handleBlur}
                  helperText={touched.retype_password ? errors.retype_password : ""}
                  error={touched.retype_password && Boolean(errors.retype_password)}
                  onChange={handleChange}
                  type={showPassword1 ? "text" : "password"}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword1} onMouseDown={handleMouseDownPassword1}>
                          {showPassword1 ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  className={classes.passwordbox}
                />
                <div className={classes.buttonbox}>
                  <Button type="submit" variant="contained" className={classes.button}>
                    LOG IN
                  </Button>
                </div>
              </Form>
            );
          }}
        </Formik>
      </Grid>
    </Grid>
  );
};

export default withSnackbar(Resetpassword);
