import { useEffect,CssBaseline, Topbar, Sidebar, React, useHistory } from "allImport";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function Createvacancy() {
  const classes = useStyles();
  const history=useHistory()
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;

  const data = {
    MainHeading: "Create Vacancy",
    MainSubheading1: "Vacancies Lobby",
    MainSubheading2: "Create Vacancy",
    PaperHeading: "Edit Vacancy",
    Labels: {
      label1: "Vacancy Title",
      label2: "Exam Associated",
      label3: "Experience Level",
    
      label5: "Department",
    },
    DetailBox: {
      box1: "",
      box2: [],
      box3: "",
    
      box5: "",
    },
    Button: {
      button1: "Save For Later",
      button2: "Publish",
    },
    Placeholder:{
      placeholder1:"Enter Vacancy title",
      placeholder2:"Choose departmentr",
      placeholder3:"Choose experience level",
    
      placeholder5:"Choose exam associated",
    },
    height:"312px",
    Type: "Job",
    field:"Add",
    fieldType:"AddJob",
    home:`/${role}/job`
  };
  useEffect(() => {
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
   
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen data={data} />
    </div>
  );
}
