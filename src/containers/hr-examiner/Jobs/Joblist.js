import {CircularProgress, useHistory,MenuItem, OutlinedInput, Select, Emptyscreen, CancelRoundedIcon, Link, useEffect, fetchClient, apiUrl, Commontable, CssBaseline, Box, Container, Grid, Paper, Button, IconButton, InputAdornment, TextField, FormControl, Topbar, Sidebar, Copyright, React, SearchIcon, Pagination } from "allImport";
import { useStyles } from "style/Liststyle.js";
import {MenuProps,MouseEnter,MouseLeave,capitalize,convertUTCDateToLocalDate} from "Common/CommonFunction"
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
var url = new URL(`${apiUrl.viewJobs}`);
var AS = [];
var searchValue = "";
var jobIDs = [];
var jobExperience = [];
export default function Joblist() {
  const classes = useStyles();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const [data, setAllData] = React.useState({});
  const [deptLists, setDeptlist] = React.useState([]);
  const [deptSelectionVal, setDept] = React.useState("");
  const [totalPage, setTotalPage] = React.useState("");
  const [level, setLevel] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [count, setCount] = React.useState("");
  const [examSearch, setExamSearch] = React.useState("");
  const [loader, setLoader] = React.useState(false);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const history = useHistory();
  const [page, setPage] = React.useState(1);
  const handleChangePage = (event, value) => {
    let url = `${apiUrl.viewJobs}?page=` + value;
    url += status === "" ? "" : `&status=${status}`;
    url += deptSelectionVal === "" ? "" : `&department=${deptSelectionVal}`;
    url += level === "" ? "" : `&job_level=${level}`;
    url += examSearch === "" ? "" : `&search=${examSearch}`;
    fetchClient.get(url).then((res) => {
      var JobsList = res && res.data && res.data.data && res.data.data.results;
      colmConversion(JobsList);
    });
    setPage(value);
  };

  function createData(ColA, ColB, ColC, ColD, ColE, ColF,ColG) {
    return { ColA, ColB, ColC, ColD, ColE, ColF,ColG };
  }

  const colmConversion = (arr) => {
    AS = [];
    console.log(arr.length);
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].id, arr[index].job_title, arr[index] && arr[index].department.name, arr[index] && arr[index].job_level, arr[index] && arr[index].status, arr[index] && convertUTCDateToLocalDate(new Date(arr[index].created_at)).toLocaleDateString("en-IN"),arr[index] && arr[index].experience));

      jobIDs.push(arr[index] && arr[index].id);
      jobExperience.push(arr[index] &&  arr[index].experience);
    }
    processDataList();
  };
  const filterJobs = (e) => {
    setDept("")
    setLevel("")
    setStatus("")
    setExamSearch(e.target.value);
    searchValue = e.target.value;
    fetchClient.get(`${apiUrl.viewJobs}?search=` + searchValue).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      var JobsList = res && res.data && res.data.data && res.data.data.results;
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(JobsList);
    });
  };

  const jobFilterSelection = (e, type) => {
    setExamSearch("")
    if (type === "jobStatus") {
      setStatus(e.target.value);
      if (e.target.value !== "") {
        url.searchParams.set("status", e.target.value);
      } else {
        url.searchParams.delete("status");
      }
    }
    if (type === "departmentSelect") {
      setDept(e.target.value);
      if (e.target.value !== "") {
        url.searchParams.set("department", e.target.value);
      } else {
        url.searchParams.delete("department");
      }
    }
    if (type === "levelSelect") {
      setLevel(e.target.value);
      if (e.target.value !== "") {
        url.searchParams.set("job_level", e.target.value);
      } else {
        url.searchParams.delete("job_level");
      }
    }

    fetchClient.get(url).then((res) => {
      var JobsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(JobsList);
    });
  };
  const clear = () => {
    setExamSearch("");
    fetchClient.get(`${apiUrl.viewJobs}`).then((res) => {
      var JobsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(JobsList);
    });
  };

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.view = allPermissions.includes("VIEW_JOB");
      permission.create = allPermissions.includes("CREATE_JOB");
      permission.edit = allPermissions.includes("EDIT_JOB");
      permission.delete = allPermissions.includes("DELETE_JOB");
    }
    return permission;
  }

  useEffect(() => {
    fetchClient.get(`${apiUrl.viewJobs}`).then((res) => {
      console.log(res);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count);
      const JobsArray = res && res.data && res.data.data && res.data.data.results;
      colmConversion(JobsArray);
      setLoader(true);
      console.log(JobsArray);
    }).catch(() => {
      setLoader(true);
    });
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
   
  }, []);
  const processDataList = () => {
    const permissions = getPermissionsForSubAdmin();
    const data = {
      columnnames: {
        col1: "ID",
        col2: "Job Title",
        col3: "Department",
        col4: "Job Level",
        col5: "Status",
        col6: "Date Added",
      },
      TableType: "Job",
      List: AS,
      ListKey: Object.keys(AS),
      EditPath: `/${role}/job/edit`,
      profile: `/${role}/job/profile`,
      permissions: permissions,
      jobIDs: jobIDs,
      jobExperience: jobExperience,
    };

    setAllData(data);
  };

  // Hide and Show Top Bar
 useEffect(() => {
  let scrollPos = 0;
  const listenToScroll = () => {
    const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    if (winScroll > scrollPos) {
        setTopBarVisible(false);
    } else {
      setTopBarVisible(true);  
    }  
    scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
  };
  document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
  return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
}, [])
  
  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading}>Vacancies</div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
              <div className="button_invite_examiner">
              {(role === "sub-admin" && data.permissions && !data.permissions.create) ?null: <Link
                  to={{
                    pathname: `/${role}/job/create`,
                  }}
                >
                  <Button className={classes.filledButton} variant="contained" color="primary">
                    Create Vacancy
                  </Button>
                </Link>}
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>Vacancies Lobby</div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper} >
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} style={{ display: "flex" }} align="left" justify="left">
                        <div className={classes.all_examiners_heading}>All Vacancies</div>
                        <div className={classes.examiner_nos} style={{ marginLeft: "-10px" }}>
                          {count}
                        </div>
                      </Grid>

                      <Grid item lg={3} xl={3} md={3} sm={3} align="center" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            variant="outlined"
                            onChange={(e) => filterJobs(e)}
                            value={examSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: examSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={() => clear()}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} align="right" justify="right">
                        <div className={classes.multiDropdown}>
                          <FormControl   style={{ textAlign: "center", marginTop: "-16px", width: "122px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={status} onChange={(e) => jobFilterSelection(e, "jobStatus")} MenuProps={MenuProps} input={<OutlinedInput />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Status
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="publish"
                              >
                                Publish
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="unpublish"
                              >
                                Unpublish
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="draft"
                              >
                                Drafts
                              </MenuItem>
                            </Select>
                          </FormControl>
                          <FormControl>
                            <FormControl  style={{ textAlign: "center", marginTop: "-16px", width: "149px" }}>
                              <Select id="demo-customized-select-native" displayEmpty value={deptSelectionVal} onChange={(e) => jobFilterSelection(e, "departmentSelect")} MenuProps={MenuProps} input={<OutlinedInput />}>
                                <MenuItem
                                  onMouseEnter={(e) => {
                                    MouseEnter(e);
                                  }}
                                  onMouseLeave={(e) => {
                                    MouseLeave(e);
                                  }}
                                  value=""
                                >
                                  Department
                                </MenuItem>
                                {deptLists &&
                                  deptLists.map((row, index) => (
                                    <MenuItem
                                      onMouseEnter={(e) => {
                                        MouseEnter(e);
                                      }}
                                      onMouseLeave={(e) => {
                                        MouseLeave(e);
                                      }}
                                      key={row.id} value={row.id}
                                    >
                                      {capitalize(row.name)}
                                    </MenuItem>
                                  ))}
                              </Select>
                            </FormControl>
                          </FormControl>
                          <FormControl  style={{ textAlign: "center", marginTop: "-16px", width: "142px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={level} onChange={(e) => jobFilterSelection(e, "levelSelect")} MenuProps={MenuProps} input={<OutlinedInput />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Level
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="senior"
                              >
                                Senior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="junior"
                              >
                                Junior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="internship"
                              >
                                Internship
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </div>
                      </Grid>
                    </Grid>

                    {loader ? <>{count ? (
                      <Grid container xs={12}>
                        <div className={classes.examiners_table_wrapper}>
                          <Commontable data={data} />
                        </div>
                      </Grid>
                    ) : (
                      <Emptyscreen image="/assets/Icons/JobEmptyIcon.svg" />
                    )}</>:
                    <Paper className={classes.paper} style={{height:'380px',justifyContent:'center',alignItems:'center'}} ><CircularProgress  color="error"/></Paper>}

                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
