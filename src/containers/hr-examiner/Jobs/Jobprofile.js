import  {CircularProgress,Paper,useHistory,useEffect,fetchClient,apiUrl,CssBaseline,Topbar,Sidebar,React} from 'allImport';
import Commonprofile from 'components/Commonprofile/Commonprofile';
import useStyles from  "style/Profilestyle.js"
import {capitalize} from "Common/CommonFunction"
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function Jobprofile(props) {
  const classes = useStyles();
  const [jobProfileDetails, setJobProfileDetails] = React.useState([]);
  const [jobProfileDetails1, setJobProfileDetails1] = React.useState([]);
  const [loader, setLoader] = React.useState(false);
  const history=useHistory()
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const jobID = props && props.location && props.location.state && props.location.state.jobID;
  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.edit = allPermissions.includes("EDIT_JOB");
      permission.delete = allPermissions.includes("DELETE_JOB");
    }
    return permission;
  }

  useEffect(() => {
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
    fetchClient.get(`${apiUrl.jobProfile}${jobID}/profile/`).then((res) => {
       setLoader(true)
      setJobProfileDetails(res && res.data && res.data.data  );
      setJobProfileDetails1(res && res.data && res.data.data&& res.data.data.department);
      
    })
   
  }, []);
  
  console.log("www1",jobProfileDetails1)
 
  const data ={
    MainHeading:"Vacancy - " + jobProfileDetails.job_title,
    MainSubheading1:"Vacancies Lobby",
    MainSubheading2:"Vacancy Profile",
    PaperHeading:' Vacancy Details',
    Labels:{
      label1:"Title",
      label2:"Department",
      label3:"Level",
      label4:"Required Experience ",
      label5:"Exam Associated" ,
      label6:"Status",
    },
    DetailBox:{
       box1:jobProfileDetails.job_title,
       box2:jobProfileDetails1.name,
       box3:capitalize(jobProfileDetails.job_level),
       box4:jobProfileDetails.experience,
       box5:jobProfileDetails1,
       box6:capitalize(jobProfileDetails.status)
     

    },
    EditPath:`/${role}/job/edit`,
    ProfileType:"Job",
    jobID:jobID,
    home:`/${role}/job`,
    permissions: getPermissionsForSubAdmin(),
    
  }
  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        {loader ? <Commonprofile data={data}/> :
         (<Paper className={classes.paper} style={{ height: "450px", justifyContent: "center", alignItems: "center",marginTop:'200px' }}>
              <CircularProgress color="error" />
                   </Paper>
                    )}
      </main>
    </div>
  );
}