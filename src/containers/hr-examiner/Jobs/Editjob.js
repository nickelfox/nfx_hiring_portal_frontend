import { useEffect,CssBaseline, Topbar, Sidebar, React, useHistory } from "allImport";
import "style/style.css";
import { useStyles } from "style/Editstyle.js";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function Editjob(props) {
  const classes = useStyles();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const history=useHistory()
  const jobID = props && props.location && props.location.state && props.location.state.jobID;
  const data = {
    JobLevel:props.location.state.jobLevel,
    Department: props.location.state.jobDepts,
    MainHeading: "Edit Vacancy",
    MainSubheading1: "Vacancies Lobby",
    MainSubheading2: "Edit Vacancy",
    PaperHeading: "Enter Vacancy Details",
   
    Labels: {
      label1: "Vacancy Title",
      label2: "Exam Associated",
      label3: "Experience Level",
      
      label5: "Department",
    },
    DetailBox: {
      box1: props.location.state.jobTitle,
      box2: [],
      box3:props.location.state.jobLevel,
     
      box5: props.location.state.jobDepts,
    },
    Button: {
      button1: "Cancel",
      button2: "Update",
    },
    Placeholder:{
      placeholder1:"Enter Vacancy title",
      placeholder2:"Choose exam associated",
      placeholder3:"Choose experience level",
     
      placeholder5:"Choose department",
    },
    height:"312px",
    Type: "Job",
    field : "Edit",
    fieldType: "EditJob",
    home:`/${role}/job`,
    jobID:jobID
  };
  useEffect(() => {
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
   
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen data={data} />
    </div>
  );
}
