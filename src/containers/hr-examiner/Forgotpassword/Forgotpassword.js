import {withSnackbar,Formik, Field,Yup, Form,Grid,makeStyles,Button,Typography,React,fetchPost,apiUrl, SuccessDialog} from 'allImport';
import {TextField} from 'formik-material-ui';
import {commonStyle} from "commonStyle.js";
const useStyles = makeStyles( (theme)=>({
  ...commonStyle(theme),
    forgotpassword_heading:
    {
     fontFamily: 'Mulish',
     fontSize: '50px',
     fontStyle: 'normal',
      fontWeight: 700,
     lineHeight: '63px',
     width: '450px',
     height: '63px', 
     marginLeft: '120px',
     marginTop:'277px',
     color:'#18273B',
    },
    buttonbox:
    {
      marginLeft:'120px'  ,paddingTop:'32px',
    },
    button:
    {
     color: '#FFFFFF',
     background:' #D9001D',
       height: '52px',
       width: '157px',
       left: '0px',
       top: '0px',
       borderRadius: '4px',
    },
     siginbox:
     {
        width: '430px',
        height: '63px', 
     },
    emaillabel:
    {
        marginLeft: '120px',
        marginTop:'320 px',
        color: '#444444', 
       fontFamily: 'Open Sans',
       fontSize: '15px',
       fontStyle: 'normal',
        fontWeight:' 400',
       lineHeight:' 24px',
       letterSpacing: '0.5px',
       textAlign: 'left'
    }
    ,
    passwordlabel:
    {
        marginLeft: '120px',
       marginTop:'32px',
        color: '#444444',
        fontFamily: 'Open Sans',
       fontSize: '15px',
       fontStyle: 'normal',
        fontWeight:' 400',
       lineHeight:' 24px',
       letterSpacing: '0.5px',
       textAlign: 'left'
    },
    emailbox:{
        paddingTop:'10px',
        marginLeft: '120px',
       paddingBottom:'39px',
        width: '540px',
        height: '46px',
    },
    passwordbox:
    {
        marginLeft: '120px',
        marginTop:'10px',
        width: '540px',
        height: '46px',
    },
    discription:
    {
    fontFamily: 'Open Sans',
    fontSize: '20px',
   fontStyle: 'normal',
    fontWeight: 600,
    lineHeight: '32px',
    letterSpacing: '0.10000000149011612px',
    textAlign: 'left',
     color: '#A8A8A8',
     marginLeft: '120px',
     marginTop:'24px',
     marginRight:'237px',
     height:'64px',
     width:'582px'
    },
    inputfeedback:
    {
       marginTop:'10px',
       marginLeft:'120px'
    },
   form:{
       marginTop:'32px',
   },
  }));
const ForgotPassword = (props) => {
    const classes= useStyles();
    const [emailSent, setEmailSent] = React.useState(false);
    return (
      <>{emailSent ? <SuccessDialog type="field" heading="Email has been sent" path={`/login`} /> : null}
      <Grid container style={{maxHeight: '100vh', overflow: 'auto'}} lg={12} xl={12} md={12} sm={12}>
      <Grid  item   lg={5} xl={5} md={6} sm={8} >
          <div  className={classes.leftbox}>
           <img src="assets/Icons/Nickelfoxlogo.svg" alt='nickelfoxlogo' className={classes.nickelfoxlogo}/>
           <Typography className={classes.heading}>NickelFox</Typography>
          <Typography className={classes.subtext}>Hiring Portal</Typography>
          <Typography className={classes.leftbigtext}>Can’t Recollect your Password?</Typography><br/>
          <Typography className={classes.lefttext} >We have got you covered! Enter your registered ID and then check your mail for the Password Reset Link.</Typography>
          </div>
     </Grid>
          <Grid item lg={7} xl={7} md={6} sm={4} >
          <Typography variant ="h3" className={classes.forgotpassword_heading}>Forgot Password ?</Typography>
          <Typography className={classes.discription}>Please enter your Registered Email ID. A link will be sent to your mail through which you can change your password</Typography>
          <Formik
           initialValues={{ email: ""}}
           onSubmit={(values, { resetForm,setSubmitting }) => {
            var data = JSON.stringify({email:values.email});
            fetchPost.post(`${apiUrl.forgotPassword}`, data)
            .then((response) => { 
              setEmailSent(true)
          })
           .catch( (error)=> {  if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }});
           resetForm();
          }}
         validationSchema={Yup.object().shape({
          email: Yup.string()
           .email()
          .required("Required"),
         })}
        >
          {props => {
            const {
                touched,
                errors,
                values,
                handleChange,
                handleBlur,
                handleSubmit,
            } = props;
            return (
            <Form onSubmit={handleSubmit} className={classes.form}>
                <label htmlFor="email" className={classes.emaillabel}>Email ID*</label><br/>
              <Field
                 autoComplete='off'
                placeholder="Enter Your Email"
                name="email"
                type="text"
                component={TextField}
                margin="none"
                variant="outlined"
                value={values.email}
                onChange={handleChange}
                 onBlur={handleBlur}
                helperText={touched.email ? errors.email : ""}
                error={touched.email && Boolean(errors.email)}
                className={classes.emailbox}
              />
             <div className={classes.buttonbox}><Button type="submit" variant="contained" className={classes.button}>
                GET LINK
              </Button>
             </div>
            </Form>
            );
          }}
    </Formik>
          </Grid>
          </Grid></>
    )
}
export default withSnackbar(ForgotPassword);