import { Emptyscreen,CircularProgress,useHistory, clsx, Link, CancelRoundedIcon, OutlinedInput, apiUrl, useEffect, fetchClient, Select, MenuItem, Commontable, CssBaseline, Box, Container, Grid, Paper, IconButton, InputAdornment, TextField, FormControl, Topbar, Sidebar, Copyright, React, SearchIcon, Pagination } from "allImport";
import { MenuProps, MouseEnter, MouseLeave,capitalize } from "Common/CommonFunction";
import { useStyles} from "style/Liststyle.js";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
var AS = [];
var searchValue = "";

var url = new URL(`${apiUrl.viewSubmittedExams}`);
export default function ExamCandidateList(props) {
  const examiner_name = localStorage.getItem("ADMIN_ID");
  const attempt_level=['L1','L2','L3','L4']
  const classes = useStyles();
  const [data, setAllData] = React.useState({});
  const history = useHistory();
  const [count, setCount] = React.useState("");
  const [score, setScore] = React.useState("");
  const [deptLists, setDeptlist] = React.useState([]);
  const [deptSelectionVal, setDept] = React.useState("");
  const [totalPage, setTotalPage] = React.useState("");
  const [examLevel, setExamLevel] = React.useState("");
  const [attemptLevel, setAttemptLevel] = React.useState("");
  const [ExamCandidateSearch, setExamCandidateSearch] = React.useState("");
  const [page, setPage] = React.useState(1);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const [loader, setLoader] = React.useState(false);
  const handleChangePage = (event, value) => {
    let url = `${apiUrl.viewSubmittedExams}?page=` + value;
    url += attemptLevel === "" ? "" : `&Level=${attemptLevel}`;
    url += deptSelectionVal === "" ? "" : `&department=${deptSelectionVal}`;
    url += examLevel === "" ? "" : `&experience_level=${examLevel}`;
    url += score === "" ? "" : `&percentage=${score}`;
    url += ExamCandidateSearch === "" ? "" : `&search=${ExamCandidateSearch}`;
    fetchClient.get(url).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const examcandidatelist = res && res.data && res.data.data && res.data.data.results;
      colmConversion(examcandidatelist);
    });
    setPage(value);
  };
  function createData(ColA, ColB, ColC, ColD, ColE, ColF, ColG,ColH,ColI) {
    return { ColA, ColB, ColC, ColD, ColE, ColF, ColG,ColH ,ColI};
  }

  const colmConversion = (arr) => {
    AS = [];
    console.log("occo",arr.length);
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].candidate[0] && arr[index].candidate[0].candidate_id, arr[index] && arr[index].candidate[0] && arr[index].candidate[0].candidate_full_name, arr[index] && arr[index].candidate[0] && arr[index].candidate[0].department_name,arr[index] && arr[index].candidate[0] && arr[index].candidate[0].exam_name,arr[index] && arr[index].candidate[0] && arr[index].candidate[0].experience_level,arr[index] && arr[index].candidate[0] && arr[index].candidate[0].attempt_level,  arr[index] && arr[index].candidate[0] && arr[index].candidate[0].total_questions, arr[index] && arr[index].candidate[0] &&arr[index].candidate[0].percentage.toFixed(2) +" %",arr[index] && arr[index].candidate[0] && arr[index].candidate[0].exam_id));
    }
    processDataList();
  };
  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.view = allPermissions.includes("VIEW_RESULT");
      permission.create = allPermissions.includes("CREATE_RESULT");
      permission.edit = allPermissions.includes("EDIT_RESULT");
      permission.delete = allPermissions.includes("DELETE_RESULT");
    }
    return permission;
  }
  const processDataList = () => {
    const permissions = getPermissionsForSubAdmin();
    const data = {
      columnnames: {
        col1: "ID",
        col2: "Name",
        col3: "Department",
        col4: "Exam Name",
        col5: "Exam Level",
        col6: "Attempt Level",
        col7: "Total Questions",
      },
      TableType: "ExamCandidate",
      List: AS,
      permissions: permissions,
      ListKey: Object.keys(AS),
      profile: `/${role}/submitted-exam/result`,
    };
    setAllData(data);
  };

  const filterExamsCandidate = (e) => {
    setScore("")
    setDept("")
    setExamLevel("")
    setAttemptLevel("")
    setExamCandidateSearch(e.target.value);
    url.searchParams.delete("percentage");
    url.searchParams.delete("department");
    url.searchParams.delete("experience_level");
    url.searchParams.delete("attempt_level");
    searchValue = e.target.value;
    url.searchParams.set("search",searchValue);
    if (role === "examiner")
    {
      url.searchParams.set("examiner",examiner_name);
    }
    fetchClient.get(url).then((res) => {
      const examcandidatelist = res && res.data && res.data.data && res.data.data.results ;
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(examcandidatelist);
    });
  };
  const ExamCandidateFilterSelection = (e, type) => {
    url.searchParams.delete("search");
    setExamCandidateSearch("")
    if (type === "Score") {
      setScore(e.target.value);
      if (e.target.value !== "") {
       url.searchParams.set("percentage", e.target.value);
      } else {
       url.searchParams.delete("percentage");
      }
    }

    if (type === "department") {
      setDept(e.target.value);
      if (e.target.value !== "") {
       url.searchParams.set("department", e.target.value);
      } else {
       url.searchParams.delete("department");
      }
    }
    if (type === "ExamLevel") {
      setExamLevel(e.target.value);
      if (e.target.value !== "") {
       url.searchParams.set("experience_level", e.target.value);
      } else {
       url.searchParams.delete("experience_level");
      }
    }
    if (type === "AttemptLevel") {
      setAttemptLevel(e.target.value);
      if (e.target.value !== "") {
       url.searchParams.set("attempt_level", e.target.value);
      } else {
       url.searchParams.delete("attempt_level");
      }
    }
    if (role === "examiner") {
      url.searchParams.set("examiner", examiner_name);
    }
    fetchClient.get(url).then((res) => {
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      const examcandidatelist = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(examcandidatelist);
    });
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    let url= new URL(`${apiUrl.viewSubmittedExams}`);
    if(role==="examiner")
    {
      url.searchParams.set("examiner",examiner_name);
    }
    fetchClient.get(url).then((res) => {
      setLoader(true);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      const examcandidatelist = res && res.data && res.data.data && res.data.data.results && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(examcandidatelist);
    }).catch(() => {
      setLoader(true);
    });
    fetchClient.get(`${apiUrl.AllExamStatus}?page_size=10000`)
    fetchClient.get(`${apiUrl.deptList}?page_size=10000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
  }, []);
  const clear = () => {
    url.searchParams.delete("search");
    setExamCandidateSearch("");
    if (role === "examiner")
    {
      url.searchParams.set("examiner",examiner_name);
    }
    fetchClient.get(url).then((res) => {
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      const examcandidatelist = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(examcandidatelist);
    });
  };

  // Hide and Show Top Bar
 useEffect(() => {
  let scrollPos = 0;
  const listenToScroll = () => {
    const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    if (winScroll > scrollPos) {
        setTopBarVisible(false);
    } else {
      setTopBarVisible(true);  
    }  
    scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
  };
  document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
  return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
}, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container>
            <Grid item xs={12} sm={12}>
              <div className={classes.page_heading}>Submitted Exams</div>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={12} sm={6}>
              <div className={classes.page_subheading}>
                <span className={classes.goBackURL}>
                  {" "}
                  <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname: `/${role}/result/exam/list` }}>
                    Submitted Exams Lobby
                  </Link>
                </span>
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper}>
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} align="left" justify="left" style={{ display: "flex" }}>
                        <div className={classes.all_examiners_heading} >Recent Submitted </div>
                        <div className={classes.examiner_nos}>{count}</div>
                      </Grid>
                      <Grid item lg={3} xl={3} md={3} sm={3} align="center" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            onChange={(e) => filterExamsCandidate(e)}
                            variant="outlined"
                            value={ExamCandidateSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: ExamCandidateSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={clear}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} align="right" justify="right">
                        <div className={classes.multiDropdown}>
                      {role==="examiner" ? null:  <FormControl style={{ textAlign: "center", width: "162px", marginTop: "-16px" }}>
                            <Select displayEmpty id="demo-customized-select-native" value={deptSelectionVal} onChange={(e) => ExamCandidateFilterSelection(e, "department")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}}/>}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                key="0"
                                value=""
                              >
                                Department
                              </MenuItem>
                              {deptLists &&
                                deptLists.map((row, index) => (
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                    key={index + 1}
                                    value={row.id}
                                  >
                                    {capitalize(row.name)}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>}
                          <FormControl displayEmpty style={{ textAlign: "center", marginTop: "-16px", width: "156px" }}>
                            <Select displayEmpty id="demo-customized-select-native" value={examLevel} onChange={(e) => ExamCandidateFilterSelection(e, "ExamLevel")}  MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                key="0"
                                value=""
                              >
                                Exam Level
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="senior"
                              >
                                Senior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="junior"
                              >
                                Junior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="internship"
                              >
                                Internship
                              </MenuItem>
                            </Select>
                          </FormControl>
                          <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "156px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={attemptLevel} onChange={(e) => ExamCandidateFilterSelection(e, "AttemptLevel")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Attempt Level
                              </MenuItem>
                              {attempt_level &&
                                attempt_level.map((row, index) => (
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                    key={row}
                                    value={row}
                                  >
                                    {row}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                        </div>
                      </Grid>
                    </Grid>
                    {loader ? <>{count ? (
                      <Grid container xs={12}>
                        <div className={classes.examiners_table_wrapper}>
                          <Commontable data={data} />
                        </div>
                      </Grid>
                    ) : (
                      <Emptyscreen />
                    )}</>:
                    <Paper className={classes.paper} style={{height:'380px',justifyContent:'center',alignItems:'center'}} ><CircularProgress  color="error"/></Paper>}
                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
