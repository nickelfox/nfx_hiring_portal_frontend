
import {Divider,CircularProgress,useHistory,makeStyles,Link,clsx,fetchPost,useEffect,fetchClient,apiUrl,CssBaseline,Topbar,Sidebar, React, Grid, Container, Box, Copyright, Button, Paper } from "allImport";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME,SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import {convertUTCDateToLocalDate } from "Common/CommonFunction";
import StorageManager from "Storage/StorageManager.js";
import ProfileStyle from "style/Profilestyle.js";
import Alert from "components/Dialog/alert";
const useAnswerStyles = makeStyles(()=> ({
  container: {
    width: "100%",
  },
  
  answersHeading: {
    fontFamily: "Mulish",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "32px",
    letterSpacing: "0.10000000149011612px",
    color: "#000000",
    margin: "32px  0",
    alignSelf:'center'
  },
  question: {
    fontFamily: "Open Sans",
    fontSize: "16px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "21px",
    letterSpacing: "0.10000000149011612px",
    margin: "60px 0 15px 32px",
  },
  answerText: {
    fontFamily: "Open Sans",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "400",
    lineHeight: "28px",
    letterSpacing: "0.15000000596046448px",
  },
  selectedAnswerText: {
    fontFamily: "Open Sans",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "700",
    lineHeight: "28px",
    letterSpacing: "0.15000000596046448px",
  },
  checkImage: {
    height: 15,
    width: 15,
    borderRadius: "50%",
    marginRight: 8,
    marginTop: 20,
  },
  answerContainerInner: {
    display: "flex",
    alignItems: "flex-start",
  },
  answerContainer: {
    padding: "0 63px 0 32px",
    marginBottom: 15,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  answerStatus: {
    fontFamily: "Open Sans",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "28px",
    letterSpacing: "0.15000000596046448px",
  },
  
}))

export default function CandidateResult(props) {
 
  const classes =ProfileStyle();
  const answerClasses = useAnswerStyles();
  const [status1, setStatus1] = React.useState("");
  const [loader, setLoader] = React.useState(false);
  const history=useHistory()
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const [candidateResult,setCandidateResult]=React.useState([]);
  const [candidateResult1,setCandidateResult1]=React.useState([]);
  const [candidateResult2,setCandidateResult2]=React.useState([]);
  const [candidateResult3,setCandidateResult3]=React.useState([]);
  const [candidateResult4,setCandidateResult4]=React.useState([]);
  const [correctQuestions, setCorrectQuestions] = React.useState([]);
  const [incorrectQuestions, setIncorrectQuestions] = React.useState([]);
  const [unattemptedQuestions, setUnattemptedQuestions] = React.useState([]);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const [Status,setExamStatus]=  React.useState([]);
  const [pass,setpass]=React.useState(false)
  const [fail,setfail]=React.useState(false)
  const data ={
    PaperHeading: ' Candidate Details',
    label1:"Full Name",
    label2:"Contact Number",
    label3:"Email ID",
    label4:"Examiner",
    label5:"Associated Vacancy",
    label6:"Department",
    label7:"Attempt Level",
    label8:"Status",
    ProfileType:"Candidate"
  }
  const data1 ={
    PaperHeading: '  Stats',
    label1:"Exam Attempted",
    label2:"Level",
    label3:"Attempt Date",
    label4:"Time Limit",
    label5:"Attempt Time",
    label6:"Questions Attempted",
    label7:"Incorrect Answers",
    label8:"Correct Answers",
    label9:"Unattemptted Answers",
    ProfileType:"Candidate"
  }
  const ExamCandidateID = props.location.state.ExamCandidateID
  const ExamID =props.location.state.ExamIDr
  function secondsToHms(d) {
    d = Number(d);
    if(d===0)
    {
      return "0 sec"
    }
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + " hr " : "";
    var mDisplay = m > 0 ? m + " min " : "";
    var sDisplay = s > 0 ? s + " sec" : "";
   
    return hDisplay + mDisplay + sDisplay; 
}
  const Pass =()=>
  {
    var d;
    Status && Status.map((row, index) => (
      row.status === "PASSED" ? d =row.id : null
    ))
    const data = {
      candidate:ExamCandidateID,
      candidate_exam:candidateResult2.id,
      exam_status:d,
    };
    fetchPost.post(`${apiUrl.ExamStatus}`,data).then(
      setpass(true)
    )
  }
  const Fail =()=>
  {
    var d;
    Status &&
    Status.map((row, index) => (
      row.status === "FAILED" ? d=row.id : null
     ))
     const data = {
      candidate:ExamCandidateID,
      candidate_exam:candidateResult2.id,
      exam_status:d,
      };
      fetchPost.post(`${apiUrl.ExamStatus}`,data).then(
        setfail(true)
      )
  }
  
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      var pass_fail = allPermissions.includes("PASS_FAIL_CANDIDATE");
    }
   

  const splitQuestions = (questions) => {
    let incorrect = [];
    let correct = [];
    let unattempted = [];
    for(let i of questions){
      if(i.answer_status === "WRONG"){
        incorrect.push(i.question);
      }else if(i.answer_status === "CORRECT") {
        correct.push(i.question);
      }else if(i.answer_status === "UNATTEMPTED"){
        unattempted.push(i.question);
      }
    }
    setIncorrectQuestions([...incorrect]);
    setCorrectQuestions([...correct]);
    setUnattemptedQuestions([...unattempted]);
  }
  
  useEffect(() => {
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
    }
    fetchClient.get(`${apiUrl.AllExamStatus}?page_size=10000`).then((res) => {  
      setExamStatus(res && res.data && res.data.data && res.data.data.results)
     })
    var id;
    fetchClient.get(`${apiUrl.CandidateResult}` + ExamCandidateID + `/exam/`  + ExamID + `/result/`).then((res) => {
      setLoader(true)
       id=res && res.data && res.data.data &&  res.data.data.results && res.data.data.results[0].id
       setStatus1(res && res.data && res.data.data && res.data.data.results && res.data.data.results[0] &&  res.data.data.results[0].candidate.latest_status.candidate_status)
      setCandidateResult(res && res.data && res.data.data && res.data.data.results && res.data.data.results[0] && res.data.data.results[0].candidate && res.data.data.results[0].candidate.user);
      setCandidateResult1(res && res.data && res.data.data && res.data.data.results && res.data.data.results[0] &&  res.data.data.results[0].candidate);
      setCandidateResult2(res && res.data && res.data.data &&  res.data.data.results && res.data.data.results[0]);
      setCandidateResult3(res && res.data && res.data.data && res.data.data.results && res.data.data.results[0] && res.data.data.results[0].exam)
      setCandidateResult4(res && res.data && res.data.data && res.data.data.results && res.data.data.results[0] &&  res.data.data.results[0].candidate.examiner && res.data.data.results[0].candidate.examiner);
    }).then((res)=>{
      fetchClient.get(`${apiUrl.AnswerScript}` + id + `/result/overview`).then((resp) => {
        splitQuestions(resp.data.data.results);
      })
    })
  }, []);

  // Hide and Show Top Bar
 useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
          setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])
  
  return (
    <div className={classes.root}>
    <CssBaseline />
    {topbarVisible ? <Topbar /> : null}
    <Sidebar />
   
    <main className={classes.content} id="main-content">
      <div className={classes.appBarSpacer} />
      {pass ? <Alert successText="Candidate Passed" heading="Are you sure you want to Pass this Candidate ?" actionName="Pass" action={() => Pass()} cancel={() => setpass(false)}/> : null}
      {fail ? <Alert successText="Candidate Failed" heading="Are you sure you want to Fail this Candidate ?" actionName="Fail" action={() => Fail()} cancel={() => setfail(false)}/> : null}
      {loader ?<Container maxWidth="xl" className={classes.container}>
        <Grid container xs={12} style={{ paddingBottom: "10px" }}>
          <Grid item xl={6} lg={6} md={6} sm={12} align="left" justify="left">
            <div className={classes.page_heading}>{candidateResult.full_name} Detailed Result</div>
          </Grid>
          { (role==="sub-admin" && !(pass_fail))  ? null :
          <>{status1==="PASSED" || status1==="FAILED" || status1==="DISQUALIFY" ? null:<Grid item xl={6} lg={6} md={6} sm={12} className={classes.actionbutton}>
            <div className="button_invite_examiner" style={{ paddingRight: "80px" }}>
              <Button onClick={() => setfail(true)} className={classes.outlinedButton} variant="contained" color="primary">
               Fail
              </Button>
            </div>
            <div className="button_invite_examiner">
              <Button onClick={() => setpass(true)} className={classes.filledButton} variant="contained" color="primary">
                Pass
              </Button>
            </div>
          </Grid>}</>}
 
        </Grid>

        <Grid container xs={12}>
          <Grid item xs={12} sm={6} align="left" justify="left">
            <div className={classes.page_subheading} style={{ width: "120%" }}>
            <span className={classes.goBackURL}> <Link className={clsx(classes.page_subheading , classes.LinkStyle)} to={{ pathname: `/${role}/submitted-exam` }} >Submitted Exams Lobby</Link></span> <span className="spc"></span> &gt; <span className="spc"></span> {candidateResult.full_name} 
            </div>
          </Grid>
        </Grid>
        
        <Paper className={classes.paper}  style={{  overflow: "hidden" }} >
        <div className={classes.paper_heading}>{data.PaperHeading}</div>
        <Divider className={classes.divider_position}/>
        <Grid container style={{ marginTop: "28px", marginLeft: "32px" }} xs={12}>
            <Grid item xl={4} lg={4} md={6} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label1} </label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult.full_name}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label2} </label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult.phone_number}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label3}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult.email}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label4}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult4.examiner_name}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label5}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult1.job}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label6} </label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult4.examiner_department}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label7} </label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult1.attempt_level}
              </div>
            </Grid>
          
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data.label8}</label>
              </div>
             
                <div className={classes.profile_detail_content} style={{color: (status1==="PASSED" || status1==="EXAM_SUBMITTED") ? '#29CB1B' : '#D9001D'}}>
                {candidateResult1 && candidateResult1.latest_status && candidateResult1.latest_status.candidate_status}
                </div> 
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper} style={{  overflow: "hidden" }}>
             <div className={classes.paper_heading}>{data1.PaperHeading}</div>
             <Divider className={classes.divider_position}/>
             <Grid container style={{ marginTop: "28px", marginLeft: "32px" }} xs={12}>
            <Grid item xl={4} lg={4} md={6} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label1} </label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult3.exam_name}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label2} </label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult3.experience_level}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label3}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {convertUTCDateToLocalDate(new Date(candidateResult3.created_at)).toLocaleDateString("en-IN")}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label4}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {secondsToHms(candidateResult3.duration)} 
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label5}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {secondsToHms(candidateResult3.duration - candidateResult2.exam_remaining_time)} 
              </div>
            </Grid>
          <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label6}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult2.total_question - candidateResult2.unattempted_question} / {candidateResult2.total_question}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label7}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult2.wrong_answer}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label8}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult2.correct_answer}
              </div>
            </Grid>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ marginBottom:25 }}>
              <div className={classes.profile_detail_label}>
                <label>{data1.label9}</label>
              </div>
              <div className={classes.profile_detail_content}>
                {candidateResult2.unattempted_question}
              </div>
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.paper}>
          <div className={answerClasses.answersHeading}>Incorrect Answers ({incorrectQuestions.length})</div>
              
            <div className={answerClasses.container}>
              {incorrectQuestions.map((i,index) => (
                <>
                  <div className={answerClasses.question} style={{display:'flex',alignItems:'flex-start'}}>
                  <p>{index+1}. </p><span dangerouslySetInnerHTML={{ __html: i.question.question_name?.replace("<img", `<img style="max-width: 350px; margin-left:-25px; border-radius: 4px; border: 1px solid #0000001A"`) }}/>
                  </div>
                  {i.answer_text.answers.map((answer) => (
                    <div className={answerClasses.answerContainer} style={answer.is_correct ? {backgroundColor: "#29CB1B33"} : answer.is_selected ? {backgroundColor: "#F1002033"}: {}}>
                      <div className={answerClasses.answerContainerInner}>
                        {answer.is_correct ?
                          <img src="/assets/Icons/check_icon.svg" className={answerClasses.checkImage} alt="" />: answer.is_selected ?
                          <img src="/assets/Icons/uncheck_icon.svg" className={answerClasses.checkImage} alt=""/> :
                          <div className={answerClasses.checkImage} style={{border: "1px solid #000000"}}></div>
                        }
                        <div className={answerClasses.answerText}><span dangerouslySetInnerHTML={{ __html: answer.answer_text?.replace("<img", `<img style="max-width: 350px; border-radius: 4px; border: 1px solid #0000001A"`) }}/></div>
                      </div>
                      {answer.is_correct ?
                        <div className={answerClasses.answerStatus}>Correct Answer</div>: answer.is_selected ?
                        <div className={answerClasses.answerStatus}>Chosen Answer</div>: null
                      }
                    </div>
                  ))}
                </>
              ))}
            </div>              
          </Paper>

          <Paper className={classes.paper}>
          <div className={answerClasses.answersHeading}>Correct Answers ({correctQuestions.length})</div>
              
            <div className={answerClasses.container}>
              {correctQuestions.map((i,index) => (
                <>
                  <div className={answerClasses.question} style={{display:'flex',alignItems:'flex-start'}}>
                  <p>{index+1}. </p><span dangerouslySetInnerHTML={{ __html: i.question.question_name?.replace("<img", `<img style="max-width: 350px; border-radius: 4px; margin-left:-25px; border: 1px solid #0000001A"`) }}/>
                  </div>
                  {i.answer_text.answers.map((answer) => (
                    <div className={answerClasses.answerContainer} style={answer.is_selected ? {backgroundColor: "#29CB1B33"} : {}}>
                      <div className={answerClasses.answerContainerInner}>
                        {answer.is_selected ?
                          <img src="/assets/Icons/check_icon.svg" className={answerClasses.checkImage} alt="" /> :
                          <div className={answerClasses.checkImage} style={{border: "1px solid #000000"}}></div>
                        }
                        <div className={answerClasses.answerText}><span dangerouslySetInnerHTML={{ __html: answer.answer_text?.replace("<img", `<img style="max-width: 350px; border-radius: 4px; border: 1px solid #0000001A"`) }}/></div>
                      </div>
                      {answer.is_selected ?
                        <div className={answerClasses.answerStatus}>Correct Answer</div>: null
                      }
                    </div>
                  ))}
                </>
              ))}
            </div>              
          </Paper>

          <Paper className={classes.paper}>
          <div className={answerClasses.answersHeading}>Unattempted Answers ({unattemptedQuestions.length})</div>
              
            <div className={answerClasses.container}>
              {unattemptedQuestions.map((i,index) => (
                <>
                  <div className={answerClasses.question} style={{display:'flex',alignItems:'flex-start'}}>
                  <p>{index+1}. </p><span dangerouslySetInnerHTML={{ __html: i.question.question_name?.replace("<img", `<img style="max-width: 350px; border-radius: 4px; margin-left:-25px; border: 1px solid #0000001A"`) }}/>
                  </div>
                  {i.answer_text.answers.map((answer) => (
                    <div className={answerClasses.answerContainer}>
                      <div className={answerClasses.answerContainerInner}>
                          <div className={answerClasses.checkImage} style={{border: "1px solid #000000"}}></div>
                        <div className={answerClasses.answerText}><span dangerouslySetInnerHTML={{ __html: answer.answer_text?.replace("<img", `<img style="max-width: 350px; border-radius: 4px; border: 1px solid #0000001A"`) }}/></div>
                      </div>
                    </div>
                  ))}
                </>
              ))}
            </div>              
          </Paper>

        <Box pt={4}>
          <Copyright />
        </Box>
      </Container>: (<div className={classes.paper} style={{ height: "450px", justifyContent: "center", alignItems: "center",marginTop:'200px' }}>
    <CircularProgress color="error" />
         </div>
          )
}
    </main>
   
    </div>
  );
}
