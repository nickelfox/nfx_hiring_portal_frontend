import {useHistory, fetchUpdate, Zoom, Link, useEffect, OutlinedInput, FormControl, MenuItem, SuccessDialog, fetchDelete, Divider, apiUrl, fetchPost, clsx, Radio, RadioGroup, FormControlLabel, CssBaseline, Box, Container, Grid, makeStyles, Paper, Button, TextField, Typography, Topbar, Sidebar, Copyright, React, fetchClient } from "allImport";
import { useLocation } from "react-router-dom";
import { withSnackbar } from "notistack";
import ChipInput from "material-ui-chip-input";
import { commonStyle } from "commonStyle.js";
import { capitalize } from "Common/CommonFunction";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import "style/content-styles.css";
import _ from "lodash";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
import { Autocomplete } from "@material-ui/lab";
var obj1 = [];

const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),
  question_label: {
    fontFamily: "Open Sans",
    width: "681px",
    height: "30px",
    marginBottom: "7px",
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "21px",
  },
  select_style: {
    marginTop: "12px",
    width:'307px'
  },
  option_label: {
    fontFamily: "Open Sans",
    marginLeft: "30px",
    marginTop: "7px",
    width: "651px",
    height: "30px",
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "21px",
  },
  options_style: {
    display: "flex",
    width: "745px",
  },
  editor_style: {
    marginLeft: "30px",
    marginTop: "7px",
    width: "651px",
  },

  time_label: {
    width: "27px",
    height: "23px",
    borderRadius: "4px",
  },
  text_label: {
    fontFamily: "Open Sans",
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "19px",
  },
  chip: {
    margin: theme.spacing(0.5),
  },
  tag_label: {
    display: "flex",
    marginTop: "9px",
    marginBottom: "36px",
    marginLeft: "28px",
  },
  checkbox_style: {
    height: "18px",
    width: "18px",
    verticalAlign: "middle",
    marginTop: "13px",
    marginRight: "-19px",
    marginLeft: "0px",
  },
  button_style: {
    marginLeft: "30px",
    marginTop: "10px",
  },
  exam_name_label: {
    fontFamily: "Open Sans",
    width: "681px",
    height: "30px",
    marginBottom: "7px",
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "21px",
    paddingTop: "32px",
    paddingLeft: "32px",
  },
  experience_level_label: {
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 600,
    lineHeight: "32px",
    letterSpacing: "0.10000000149011612px",
    textAlign: "left",
    paddingTop: "32px",
    paddingLeft: "32px",
    height: "32px",
  },
  exam_name_box: {
    paddingTop: "45.37px",
    paddingLeft: "32px",
  },
}));

const QuestionBankEditQuestion = (props) => {
  const classes = useStyles();
  const location = useLocation();

  const questionID = location.state.questionID;
  const questionLevel = location.state.questionLevel;
  var timeDuration = location.state.timeDuration;
  var examDept = location.state.Department;
  const editFromExam = location.state.editFromExam;
  const questionStatus = location.state.questionStatus;
  console.log(examDept);
  var h = Math.floor(timeDuration / (60 * 60));
  timeDuration -= h * (60 * 60);
  var m = Math.floor(timeDuration / 60);
  timeDuration -= m * 60;
  const history=useHistory()
  const [correct_Count,setCorrect_count] = React.useState(0);
  const [tagList,setTaglist]=React.useState([]);
  const [value0, setValue0] = React.useState("");
  const [deptLists, setDeptlist] = React.useState([]);
  const [dept, setDept] = React.useState();
  const [count, setCount] = React.useState();
  const [Level, setLevel] = React.useState(questionLevel);
  const [t1, setT1] = React.useState(h);
  const [t2, setT2] = React.useState(m);
  const [t3, setT3] = React.useState(timeDuration);
  const [question, setQuestion] = React.useState([]);
  const [tag, settag] = React.useState([]);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const [tags, setTags] = React.useState([]);
  const [unpublish, setUnpublish] = React.useState(false);
  const [update, setUpdate] = React.useState(false);
  const [obj, setobj] = React.useState({});
 const role=localStorage.getItem("ROLE").toLowerCase()

  const handleIncrement = () => {
    setCount(prevcount=>prevcount+1)
    const obja = _.cloneDeep(obj);
    obja[_.uniqueId()] = { answer_text: "", is_correct: false };

    setobj(obja);
  };
  const checkbox = (e, id) => {
    e.target.checked ? setCorrect_count(prevcount=>prevcount+1): setCorrect_count(prevcount=>prevcount-1)
    const obja = _.cloneDeep(obj);
    obja[id].is_correct = e.target.checked;
    setobj(obja);
  };

  const handleAddChip = (chip) => {
    setTaglist([])
   if(!tag.includes(chip))
   {
    settag([...tag, chip]);
    setTags([...tags,chip],)
   }
  };
  const handleImageTextInput =(editor,field, id)=>
  {
   var data = editor.getData()
    var s= ""
    if(data.match(/<img[^>]+src="?([^"\s]+)"?\s*>/g))
    {
      editor.isReadOnly = true;
        var data1=data.matchAll(/<img[^>]+src="?([^"\s]+)"?\s*>/g);
        data1=[...data1]
        data1.map(s1=>s+=s1);
        data=s.split(",")[0];
       
      
    }
    handleCkEditor(data,field,id)
  }
  const handleDeleteChip = (chip, index) => {
    tagList && tagList.map((row, index) => (row.tag === chip ?  fetchDelete.delete(apiUrl.deleteTag + row.id+`/delete/`): null));    
    tag.splice(index, 1);
    tags.splice(index, 1);
    settag([...tag]);
    setTags([...tags]);
  };

    const tagChange =(e)=>
    {
      if(e.target.value)
      {
      fetchClient.get(`${apiUrl.tag}?search=`+ e.target.value ).then((res) => {
        setTaglist(res && res.data && res.data.data ); });
      }
      else
      {
        setTaglist([])
      }
     
    
    }
  function handleRemove(id) {
    setCount(prevcount=>prevcount-1)
    console.log(id);
    const obja = _.cloneDeep(obj);
    delete obja[id];

    setobj(obja);
    console.log("array", obja);
  }

  function handleCkEditor(data, i, id) {
    obj1 = _.cloneDeep(obj);
    if (id) {
      obj1[i] = { answer_id: id, answer_text: data, is_correct: false };
    } else {
      obj1[i] = { answer_text: data, is_correct: false };
    }

    setobj(obj1);

    console.log("ckeditorarr", obj);
  }
  
  var  setID =(item, index)=> {
    var fullname = {"tag_name": item}
    return fullname;
  }
  var convert = (tag) => {
   var output =tag.map(setID)
  
    return output;
  };
  const changeQuestionStatus = () => {
    console.log(" jsfbhjf :::: ", questionStatus)
    const data1 = {
      question_name: value0,
      department: dept ? dept.id : "",
      experience_level: Level,
      question_status: questionStatus === "publish" ? "unpublish" : "publish",
      time_duration: Number(t1) * 3600 + Number(t2) * 60 + Number(t3),
    };
    count >=2 && correct_Count>0  ?
    fetchUpdate.patch(apiUrl.createQuestion + questionID, data1).then((res) => {
      setUnpublish(true);
      const id = res.data.data.id;
      fetchPost.post(apiUrl.addTag + id + "/tag/", convert(tags));
      fetchPost.post(apiUrl.createOption + id + "/answers", Object.values(obj));
    }).catch((error) => {
      setUnpublish(false)
      if(error.response.status === 404)
      {
        props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", { 
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
        },
        TransitionComponent: Zoom,
        autoHideDuration:5000,
        resumeHideDuration: 2000
           })
      }
      else
      {
      props.enqueueSnackbar(error.response.data.error.message, { 
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
      },
      TransitionComponent: Zoom,
      autoHideDuration:5000,
      resumeHideDuration: 2000
         })
        }
    }):props.enqueueSnackbar("Please Add Atleast Two Options And Select Atleast One Correct Answer ", { 
      variant: 'error',
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'right',
    },
    TransitionComponent: Zoom,
    autoHideDuration:5000,
    resumeHideDuration: 2000
       })
  };
  const Update = () => {
    const data1 = {
      question_name: value0,
      department: dept.id,
      experience_level: Level,
      time_duration: Number(t1) * 3600 + Number(t2) * 60 + Number(t3),
    };
    count >=2 && correct_Count>0? 
    fetchUpdate.patch(apiUrl.createQuestion + questionID, data1).then((res) => {
      setUpdate(true);
      const id = res.data.data.id;
      fetchPost.post(apiUrl.addTag + id + "/tag/", convert(tags));
      fetchPost.post(apiUrl.createOption + id + "/answers", Object.values(obj));
    }).catch((error) => {
      setUpdate(false)
      if(error.response.status === 404)
      {
        props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", { 
          variant: 'error',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
        },
        TransitionComponent: Zoom,
        autoHideDuration:5000,
        resumeHideDuration: 2000
           })
      }
      else
      {
      props.enqueueSnackbar(error.response.data.error.message, { 
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
      },
      TransitionComponent: Zoom,
      autoHideDuration:5000,
      resumeHideDuration: 2000
         })
        }
    }):props.enqueueSnackbar("Please Add Atleast Two Options And Select Atleast One Correct Answer", { 
      variant: 'error',
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'right',
    },
    TransitionComponent: Zoom,
    autoHideDuration:5000,
    resumeHideDuration: 2000
       })
  };

  useEffect(() => {
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
    fetchClient.get(`${apiUrl.getQuestion}` + questionID + "/answers").then((res) => {
      setQuestion(res && res.data && res.data.data && res.data.data.question);
      setValue0(res && res.data && res.data.data && res.data.data.question.question_name)
      res.data.data.answers.map((a)=>a.is_correct?setCorrect_count(prevcount=>prevcount+1):null)
      if (res.data.data.tags[0] != null && res.data.data.tags[0].length !== 0) {
        const ob = Object.assign(...res.data.data.tags[0].map((a) => ({ [_.uniqueId()]: a.tag })));
        settag(Object.values(ob));
      }
      setCount(res.data.data.answers.length)
      if (res.data.data.answers != null && res.data.data.answers.length !== 0) {
        const obj1 = Object.assign(...res.data.data.answers.map((a) => ({ [_.uniqueId()]: { answer_id: a.answer_id, answer_text: a.answer_text, is_correct: a.is_correct } })));
        setobj(obj1);
      }
    });
    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
      setDept(res && res.data && res.data.data && res.data.data.results.find((e) => e.name === examDept))
    });
  }, []);

  // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
          setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content}  id="main-content">
        <div className={classes.appBarSpacer} />
        {unpublish ? <SuccessDialog heading={`Question ${questionStatus === "publish" ? "Unpublished" : "Published"}`} path={`/${role}/question-bank/questions`} /> : null}
        {update ? <SuccessDialog heading="Question Updated" path={`/${role}/question-bank/questions`} /> : null}

        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12} style={{ paddingBottom: "10px" }}>
            <Grid item xl={6} lg={6} md={6} sm={12} align="left" justify="left" style={{ display: "flex" }}>
              <div className={classes.page_heading}>Edit Question</div>
            </Grid>
            <Grid item xl={6} lg={6} md={6} sm={12} className={classes.actionbutton}>
              <div className="button_invite_examiner" style={{ paddingRight: "80px" }}>
                <Button onClick={Update} className={classes.outlinedButton} variant="contained" color="primary">
                  Update
                </Button>
              </div>
              {editFromExam ? null : <div className="button_invite_examiner">
                <Button onClick={changeQuestionStatus} className={classes.filledButton} variant="contained" color="primary">
                  {questionStatus === "publish" ? "Unpublish" : "Publish"}
                </Button>
              </div>}
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>
                <span className={classes.goBackURL}>
                  <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname: `/${role}/question-bank/questions` }}>
                    Question Bank Lobby
                  </Link>
                </span>{" "}
                <span className="spc"></span> &gt; <span className="spc"></span>Add Question
              </div>
            </Grid>
          </Grid>

          <Paper className={classes.paper}>
            <Grid container xs={12}>
             {role==='examiner' ||editFromExam?null: <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left" style={{ paddingLeft: "60px", paddingTop: "32px" }}>
                <h1 style={{ fontWeight: "450", fontSize: "22px" }}>Choose Department</h1>
               
                  <div className={classes.examiner_detail_label}>
                    <label>
                      Department
                      <span className={classes.mandotory}>*</span>
                    </label>
                  </div>

                <FormControl>
                  <Autocomplete
                  disableClearable
                    input={<OutlinedInput /> }
                    value={dept ? deptLists.find((e) => e.id === dept.id) : null}
                    options={deptLists}
                    style={{marginTop:'10px',
                    width: "300px"}}                  
                    getOptionLabel={option => capitalize(option.name) || ""}
                    onChange={(e, newValue) => newValue ? setDept(newValue):null }
                    renderInput={params => (
                      <TextField
                        {...params}
                        variant="outlined"
                      />)}
                  />
                </FormControl>
               
              </Grid>}
              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px", paddingTop: "32px" }}>
                <div className={classes.editor_style}>
                  <h1 style={{ fontWeight: "450", fontSize: "22px" }}>Edit Question</h1>
                  <CKEditor
                    className="ck-content"
                    key={question.question_name}
                    data={question.question_name}
                    editor={Editor}
                    config={{
                      simpleUpload : {
                        uploadUrl: apiUrl.ckEditorImageUpload,
                        withCredentials: true,
                        headers: {
                          Authorization: `Bearer ${localStorage.getItem('API_TOKEN')}`
                        }
                      },
                    }}
                    onReady={(editor) => {
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      setValue0(data);
                    }}
                  />
                </div>
              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px", paddingTop: "10px" }}>
                <div className={classes.editor_style}>
                  <h1 style={{ fontWeight: "450", fontSize: "22px" }}>Edit Answers</h1>
                  {Object.keys(obj).map((field, index) => {
                    return (
                      <div style={{display:'flex'}} key={index }>
                      <div className={clsx("option_wrapper", classes.options_style)}>
                        <label className="container">
                          <input
                            type="checkbox"
                            checked={obj[field].is_correct}
                            onChange={(e) => {
                              checkbox(e, field);
                            }}
                          />
                          <span className="mark"></span>
                        </label>

                        <div className={classes.editor_style}>
                          <CKEditor
                            id={field}
                            className="ck-content"
                            editor={Editor}
                            config={{
                              simpleUpload : {
                                uploadUrl: apiUrl.ckEditorImageUpload,
                                withCredentials: true,
                                headers: {
                                  Authorization: `Bearer ${localStorage.getItem('API_TOKEN')}`
                                }
                              },
                            }}
                            data={obj[field].answer_text}
                            onReady={(editor) => {}}
                            onChange={(event, editor) => {
                             
                              handleImageTextInput(editor,field, obj[field].answer_id)
                            
                            }}
                            onBlur={(event, editor) => {}}
                            onFocusout={(event, editor) => {}}
                          />
                        </div>

                        <div key={field}>
                          <Button style={{ marginTop: "10px" }} onClick={() => handleRemove(field)}>
                            <img src="/assets/Icons/cross_icon.svg" alt="" />
                          </Button>
                        </div>
                      </div>
                      <div> {obj[field].is_correct ? <h4 style={{width:'140px',fontSize:'20px',color:'#D9001D'}}>correct answer</h4>:null}</div>
                      </div>
                    );
                  })}

                  <Button
                    onClick={() => {
                      handleIncrement();
                    }}
                    style={{ marginLeft: "20px", marginTop: "10px" }}
                  >
                    <img src="/assets/Icons/add_icon.svg" alt="" style={{ marginRight: "10px" }} />
                    <span className={classes.page_subheading}>Add Option</span>
                  </Button>
                </div>
                <Divider style={{ marginTop: "23px" }} />
              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px" }}>
                <div className="timer_wrapper" style={{ display: "flex", marginLeft: "31px", marginTop: "20px", width: "532px" }}>
                  <div className={classes.text_label}>Timer</div>
                  <input
                   type="number"
                   min="0" max="10" 
                    style={{ marginLeft: "31px",width:'auto' }}
                    onChange={(e) => {
                      setT1(e.target.value);
                    }}
                  
                    value={Math.min(10,t1)}
                    className={classes.time_label}
                  />
                  <span style={{ marginLeft: "5px", marginRight: "6px" }}>h :</span>
                  <input
                   type="number"
                    min="0" max="59" 
                   style={{width:'auto'}}
                    onChange={(e) => {
                      setT2(e.target.value);
                    }}
                    value={  Math.min(59,t2)}


                    className={classes.time_label}
                  />
                  <span style={{ marginLeft: "5px", marginRight: "6px" }}>m :</span>
                  <input
                   type="number"
                   min="0" max="59" 
                   style={{width:"auto"}}
                    onChange={(e) => {
                      setT3(e.target.value);
                    }}
                    value={ Math.min(59,t3)}
                    className={classes.time_label}
                  />
                    <span style={{ marginLeft: "5px"}}>s</span>
                </div>
                <Divider style={{ marginTop: "20px" }} />
                <div className={classes.text_label} style={{ marginLeft: "28px", marginTop: "20px" }}>
                  Tags
                </div>
                <div className={classes.tag_label} style={{display:'block'}}>
                <ChipInput
                 disableUnderline
                 placeholder='Type here'
                 alwaysShowPlaceholder={true}
                onKeyUp ={ (e)=> tagChange(e)}
                 value={tag}
                 onAdd={(chip) => handleAddChip(chip)}
                 onDelete={(chip, index) => handleDeleteChip(chip, index)}
               
                 />
                 
                 {
                   tagList.map((qw)=>
                   <MenuItem value={qw.tag_name}   onClick={  (e)=>{handleAddChip(qw.tag_name)}}>
                     {qw.tag_name}
                     </MenuItem>
                   )
                 }
               
                </div>
                <Divider style={{ marginTop: "20px" }} />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px" }}>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <Typography className={classes.experience_level_label}>Question Level</Typography>
                  <div className={classes.exam_name_box} style={{ marginBottom: "20px" }}>
                    <RadioGroup
                      value={Level}
                      onChange={(e) => {
                        setLevel(e.target.value);
                      }}
                      aria-label="gender"
                      name="gender1"
                    >
                      <FormControlLabel value="senior" control={<Radio />} label="Senior" />
                      <FormControlLabel value="junior" control={<Radio />} label="Junior" />
                      <FormControlLabel value="internship" control={<Radio />} label="Internship" />
                    </RadioGroup>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Paper>

          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
};
export default withSnackbar(QuestionBankEditQuestion);
