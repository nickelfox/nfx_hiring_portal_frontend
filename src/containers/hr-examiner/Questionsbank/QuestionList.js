
import {withSnackbar,Pagination,DeleteAlert,MenuItem,Select,OutlinedInput,CancelRoundedIcon,useEffect,fetchClient,apiUrl, KeyboardArrowDownIcon, KeyboardArrowUpIcon, Table, TableBody, TableCell, CircularProgress, TableContainer, TableHead, TableRow, Link, useHistory, CssBaseline, Box, Container, Grid, Paper, Button, IconButton, InputAdornment, TextField, FormControl, Topbar, Sidebar, Copyright, React, SearchIcon } from "allImport";
import {  useStyles } from "style/Liststyle.js";
import {MenuProps,MouseEnter,MouseLeave,capitalize} from "Common/CommonFunction"
import "style/style.css";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";

var AS =[];
var searchValue = "";
var questionIDs = [];
var url = new URL(`${apiUrl.viewQuestions}`);

 const  QuestionList =(props)=> {
  const classes = useStyles();
  const [open,setOpen] = React.useState([]);
  const [deptLists, setDeptlist] = React.useState([]);
  const [deptSelectionVal, setDept] = React.useState("");
  const [totalPage, setTotalPage] = React.useState("");
  const [level, setLevel] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [count , setCount] = React.useState("");
  const [questionSearch, setQuestionSearch] = React.useState("");
  const [Arr,setArr] = React.useState([]);
  const [questionSelect, setquestionSelect] = React.useState(false);
  const [id,setID]=React.useState("");
  const [loader, setLoader] = React.useState(false);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const examiner_dept = localStorage.getItem("EXAMINER_DEPT");
  const [permission, setPermission] = React.useState({})
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const history=useHistory()
  const [page, setPage] = React.useState(1);
  const handleChangePage = (event, value) => {
    let url = `${apiUrl.viewQuestions}?page=` + value;
    url += questionSearch === "" ? "" : `&search=${questionSearch}`;
    url += status === "" ? "" : `&question_status=${status}`;
    url += deptSelectionVal === "" ? "" : `&department=${deptSelectionVal}`;
    url += level === "" ? "" : `&experience_level=${level}`;
    if(role==="examiner")
    {
    url += examiner_dept === "" ? "" : `&department=${examiner_dept}`;
    }
    fetchClient.get(url).then((res) => {
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      colmConversion(QuestionsList,);
    });
    setPage(value);
   
  };


  function createData(ColA, ColB, ColC, ColD, ColE,ColG,ColH,ColI) {
    return { ColA, ColB, ColC, ColD, ColE,ColG,ColH,ColI};
  }

  const colmConversion = (arr) => {
    AS = [];
    let isExpanded = [];
    for (var index = 0; index < arr.length; index++) {
     
      AS.push(createData(arr[index] && arr[index].id ,arr[index]  && arr[index].question_name,arr[index].question_status, arr[index] && arr[index].department.name, arr[index] && arr[index].experience_level, arr[index] && new Date(arr[index].created_at).toLocaleDateString("en-IN"),arr[index] && arr[index].time_duration,arr[index] && arr[index].ongoing_exam));
      questionIDs.push(arr[index] && arr[index].id);
      isExpanded.push(false);
    }
    console.log(AS)
    setArr(AS)
    setOpen(isExpanded)
  };

  const filterQuestions = (e) => {
    url.searchParams.delete("question_status");
    url.searchParams.delete("experience_level");
    url.searchParams.delete("department");
    setStatus("")
    setDept("")
    setLevel("");
    setQuestionSearch(e.target.value);
    searchValue = e.target.value;
    url.searchParams.set("search",searchValue);
    if (role === "examiner")
    {
      url.searchParams.set("department",examiner_dept);
    }
   
    fetchClient.get(url).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count)
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(QuestionsList);
    });
  };

  const questionFilterSelection = (e, type) => {
    setQuestionSearch("")
    if (type === "jobStatus") {
      setStatus(e.target.value);     
      if (e.target.value !== "") {
        url.searchParams.set("question_status", e.target.value);
      } else {
        url.searchParams.delete("question_status");
      }
    }
    if (type === "departmentSelect") {
      setDept(e.target.value);
      if (e.target.value !== "") {
        url.searchParams.set("department", e.target.value);
      } else {
        url.searchParams.delete("department");
      }
    }
    if (type === "levelSelect") {
      setLevel(e.target.value);
      if (e.target.value !== "") {
        url.searchParams.set("experience_level", e.target.value);
      } else {
        url.searchParams.delete("experience_level");
      }
    }
  if(role==='examiner' )
 {
  url.searchParams.set("department", examiner_dept);
 }

    fetchClient.get(url).then((res) => {
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count)
      colmConversion(QuestionsList);
    })
  
  };
  const clear = () => {
    setQuestionSearch("")
    url.searchParams.delete("search");
    if(role==='examiner' )
 {
  url.searchParams.set("department", examiner_dept);
 }
    fetchClient.get(url).then((res) => {
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count)
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(QuestionsList);
    });
  };

  const handleExpand = (index) => {
    let temp = open;
    temp[index] = !temp[index];
    setOpen([...temp]);
  }

  const setPermissionsForSubAdmin = () => {
    if(role === "sub-admin"){
      let permission = {};
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.create = allPermissions.includes("CREATE_QUESTION_BANK");
      permission.view = allPermissions.includes("VIEW_QUESTION_BANK");
      permission.edit = allPermissions.includes("EDIT_QUESTION_BANK");
      permission.delete = allPermissions.includes("DELETE_QUESTION_BANK");
      setPermission(permission)
    }
  }
  const handleEdit =(row)=>{
   
  history.push({
    pathname: `/${role}/question-bank/questions/edit`,
    state:{
    questionID:row.ColA,
    questionLevel:row.ColE,
    timeDuration:row.ColH,
    Department:row.ColD,
    questionStatus: row.ColC,}
  })
   
}

const DeleteQuestion = (row) => {
  setID(row.ColA);
 
  !(row.ColI) ?
  setquestionSelect(!questionSelect)
    :props.enqueueSnackbar("Question can not be remove Exam is going on", {
      variant: "error",
       autoHideDuration:3000,
     
    });
  }
  useEffect(() => {
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
  let url = new URL(`${apiUrl.viewQuestions}`);
  if(role==='examiner' )
  {
   url.searchParams.set("department", examiner_dept);
  }
    
    fetchClient.get(url).then((res) => {     
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count)
      const questionList = res && res.data && res.data.data && res.data.data.results;
      colmConversion(questionList);
      setLoader(true);
    })
    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
    setPermissionsForSubAdmin();
  }, []);
  
  // Hide and Show Top Bar
 useEffect(() => {
  let scrollPos = 0;
  const listenToScroll = () => {
    const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    if (winScroll > scrollPos) {
        setTopBarVisible(false);
    } else {
      setTopBarVisible(true);  
    }  
    scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
  };
  document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
  return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
}, [])
 
  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        {questionSelect  ? <DeleteAlert type="Question" heading="Are you sure you want to Delete this Question ? "  id= {id} /> : null}
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading} style={{ width: "140%" }}>
                Question Bank
              </div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
              {(role === "sub-admin" && !permission.create) ? null : <div className="button_invite_examiner">
                <Link to={{
                  pathname:`/${role}/question-bank/questions/add`,
                }}
                >
                <Button className={classes.filledButton} variant="contained" color="primary">
                  New Question
                </Button></Link>
              </div>}
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>Question Bank Lobby</div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper} >
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} align="left" justify="left" style={{ display: "flex" }}>
                        <div className={classes.all_examiners_heading}> All Questions</div>
                        <div className={classes.examiner_nos}>{count}</div>
                      </Grid>
                      <Grid item lg={3} xl={3} md={3} sm={3} align="center" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            onChange={(e) => filterQuestions(e)}
                            variant="outlined"
                            value={questionSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: questionSearch && ( <IconButton aria-label="toggle password visibility" onClick={clear} ><CancelRoundedIcon/></IconButton> ) }}
                          />
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} align="right" justify="right">
                        <div className={classes.multiDropdown}>
                        <FormControl   style={{ textAlign: "center", marginTop: "-16px", width: "122px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={status} onChange={(e) => questionFilterSelection(e, "jobStatus")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Status
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="publish"
                              >
                                Publish
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="unpublish"
                              >
                                Unpublish
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="draft"
                              >
                                Drafts
                              </MenuItem>
                            </Select>
                          </FormControl>
                           {role==="examiner" ? null: <FormControl  style={{ textAlign: "center", marginTop: "-16px", width: "149px" }}>
                              <Select id="demo-customized-select-native" displayEmpty value={deptSelectionVal} onChange={(e) => questionFilterSelection(e, "departmentSelect")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                                <MenuItem
                                  onMouseEnter={(e) => {
                                    MouseEnter(e);
                                  }}
                                  onMouseLeave={(e) => {
                                    MouseLeave(e);
                                  }}
                                  value=""
                                >
                                  Department
                                </MenuItem>
                                {deptLists &&
                                  deptLists.map((row, index) => (
                                    <MenuItem
                                      onMouseEnter={(e) => {
                                        MouseEnter(e);
                                      }}
                                      onMouseLeave={(e) => {
                                        MouseLeave(e);
                                      }}
                                      key={row.id} value={row.id}
                                    >
                                      {capitalize(row.name)}
                                    </MenuItem>
                                  ))}
                              </Select>
                            </FormControl>}
                         
                          <FormControl  style={{ textAlign: "center", marginTop: "-16px", width: "152px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={level} onChange={(e) => questionFilterSelection(e, "levelSelect")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Question Level
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="senior"
                              >
                                Senior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="junior"
                              >
                                Junior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="internship"
                              >
                                Internship
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </div>
                      </Grid>
                    </Grid>

                    {!loader ? <Paper className={classes.paper} style={{height: '400px', justifyContent: 'center',alignItems:'center'}} >
                      <CircularProgress color="error"/></Paper> :
                      <Grid container xs={12}>
                        <div className={classes.examiners_table_wrapper}>
                          <TableContainer component={Paper}>
                            <Table className={classes.table} aria-label="simple table">
                              <TableHead>
                                <TableRow>
                                  <TableCell />
                                  <TableCell align="left">ID</TableCell>
                                  <TableCell align="left">Questions</TableCell>
                                  <TableCell align="left">Status</TableCell>
                                {role==="examiner" ? null: <TableCell align="left">Department</TableCell>}
                                  <TableCell align="left">Question Level</TableCell>
                                  <TableCell align="left">Date Added</TableCell>
                                  <TableCell />
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {Arr.map((row,index) => (
                                  <TableRow key={row.ColA}>
                                    <TableCell>

                                      <div style={{width: "100%", height: "100%"}}>
                                      <IconButton aria-label="expand row" size="small" onClick={() => handleExpand(index)}>
                                        {open[index] ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                                      </IconButton></div>
                                    </TableCell> 
                                    
                                      <TableCell align="left">{row.ColA.slice(3,8)}  </TableCell>
                                  

                                    <TableCell align="left" ><div className={!open[index] ? classes.collapseRow : ""} style={{maxWidth: 260}}><span  dangerouslySetInnerHTML={{__html: row.ColB.replace("<img", `<img style="max-width: 350px; margin-left:-55px; border-radius: 4px; border: 1px solid #0000001A"`)}}></span></div></TableCell>
                                    <TableCell align="left">{capitalize(row.ColC)}</TableCell>
                                    {role==="examiner" ? null:<TableCell align="left">{capitalize(row.ColD)}</TableCell>}
                                    <TableCell align="left">{capitalize(row.ColE)}</TableCell>
                                    <TableCell align="left">{capitalize(row.ColG)}</TableCell>
                                    <TableCell align="right">
                                      <div style={{ display: "flex" }}>
                                        {(role === "sub-admin" && !permission.edit) ? null : 
                                          <img onClick={()=>handleEdit(row)} src="/assets/Icons/editIcon.svg" alt="" />
                                       }
                                        {(role === "sub-admin" && !permission.delete) ? null : <img onClick={()=>{DeleteQuestion(row)}}  className={classes.deleteAction} src="/assets/Icons/deleteIcon.svg" alt="" />}
                                      </div>
                                    </TableCell>
                                  </TableRow>
                                ))}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        </div>
                      </Grid>}
                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
export default withSnackbar(QuestionList)