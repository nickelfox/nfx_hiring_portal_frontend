import {useHistory,Zoom, Link,useEffect,OutlinedInput,FormControl,MenuItem,SuccessDialog,Divider,apiUrl,fetchPost, clsx, Radio, RadioGroup, FormControlLabel, CssBaseline, Box, Container, Grid, makeStyles, Paper, Button, TextField, Typography, Topbar, Sidebar, Copyright, React, fetchClient } from "allImport";
import { withSnackbar } from 'notistack';
import ChipInput from 'material-ui-chip-input'
import { capitalize } from "Common/CommonFunction";
import { commonStyle } from "commonStyle.js";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import "style/content-styles.css";
import _ from "lodash";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
import { Autocomplete } from "@material-ui/lab";
var obj1 = []

const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),
  question_label: {
    fontFamily: "Open Sans",
    width: "681px",
    height: "30px",
    marginBottom: "7px",
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "21px",
  },
  select_style:{
    marginTop:"12px",
    width:'307px'
  },
  option_label: {
    fontFamily: "Open Sans",
    marginLeft: "30px",
    marginTop: "7px",
    width: "651px",
    height: "30px",
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "21px",
  },
  options_style: {
    display: "flex",
    width: "745px",
  },
  editor_style: {
    marginLeft: "30px",
    marginTop: "7px",
    width: "651px",
  },

  time_label: {
    width: "27px",
    height: "23px",
    borderRadius: "4px",
  },
  text_label: {
    fontFamily: "Open Sans",
    fontSize: "14px",
    fontWeight: "600",
    lineHeight: "19px",
  },
  chip: {
    margin: theme.spacing(0.5),
  },
  tag_label: {
    display: "flex",
    marginTop: "9px",
    marginBottom: "36px",
    marginLeft:'28px'
  },
  checkbox_style: {
    height: "18px",
    width: "18px",
    verticalAlign: "middle",
    marginTop: "13px",
    marginRight: "-19px",
    marginLeft: "0px",
  },
  button_style: {
    marginLeft: "30px",
    marginTop: "10px",
  },
  exam_name_label: {
    fontFamily: "Open Sans",
    width: "681px",
    height: "30px",
    marginBottom: "7px",
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "21px",
    paddingTop: "32px",
    paddingLeft: "32px",
  },
  experience_level_label: {
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 600,
    lineHeight: "32px",
    letterSpacing: "0.10000000149011612px",
    textAlign: "left",
    paddingTop: "32px",
    paddingLeft: "32px",
    height: "32px",
  },
  exam_name_box: {
    paddingTop: "45.37px",
    paddingLeft: "32px",
  },
}));


const AddNewQuestion = (props) => {
  const classes = useStyles();
  const history=useHistory()
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const examiner_dept = localStorage.getItem("EXAMINER_DEPT") ? localStorage.getItem("EXAMINER_DEPT"):null;
  const [value0, setValue0] = React.useState("");
  const [deptLists, setDeptlist] = React.useState([]);
  const [dept, setDept] = React.useState();
  const [count, setCount] = React.useState(0);
  const [Level,setLevel] = React.useState("");
  const [t1,setT1]= React.useState("");
  const [t2,setT2]= React.useState("");
  const [t3,setT3]= React.useState("");
  const [tag, settag] = React.useState([]);
  const [tags, setTags] = React.useState([]);
  const [publish,setPublish] = React.useState(false);
  const [draft,setDraft] = React.useState(false);
  const [correct_Count,setCorrect_count] = React.useState(0);
  const [obj,setobj]=React.useState({})
  const [tagList,setTaglist]=React.useState([]);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
 
  const handleIncrement = () => {
    setCount(prevcount=>prevcount+1)
     const obja= _.cloneDeep(obj);
     obja[_.uniqueId()]={answer_text:"",is_correct:false}
    
     setobj(obja)
  };
const checkbox =(e,id)=>{
  e.target.checked ? setCorrect_count(prevcount=>prevcount+1): setCorrect_count(prevcount=>prevcount-1)
  const obja= _.cloneDeep(obj);
  obja[id].is_correct = e.target.checked;
  setobj(obja)
}

const tagChange =(e)=>{
  if(e.target.value)
  {
  fetchClient.get(`${apiUrl.tag}?search=`+ e.target.value ).then((res) => {
    setTaglist(res && res.data && res.data.data ); });
  }
  else
  {
    setTaglist([])
  }
}

  const handleAddChip = (chip) => {
    setTaglist([])
    const obj = {"tag_name":chip}
    if(!tag.includes(chip))
    {
   setTags([...tags,obj])
  
   settag([...tag,chip])
    }
  };

const handleDeleteChip = (chip, index) => {
    tags.splice(index,1);
    tag.splice(index,1);
    settag([...tag])
    setTags([...tags])
   
};

  function handleRemove(id) {
    setCount(prevcount=>prevcount+1)
    console.log(id)
    const obja= _.cloneDeep(obj)
    delete obja[id]
  
    setobj(obja)
    console.log("array",obja)
    
    
    
  }
 
  function handleCkEditor(data,i) {
    
   
                               
     obj1=_.cloneDeep(obj)
    
    obj1[i] = {'answer_text':data, 'is_correct':false}
    setobj(obj1)
    
   console.log("ckeditorarr", obj);
  
  
  }

  const Publish = () =>{
   
    const data1 = {
    question_name:value0,
    department:dept ? dept :examiner_dept,
    experience_level:Level,
    question_status:"publish",
    time_duration:Number(t1)*3600+Number(t2)*60+Number(t3)
      
      };
      count >=2 && correct_Count>0 ?
      fetchPost.post(apiUrl.createQuestion, data1).then((res)=>{
        setPublish(true)
        const id = res.data.data.id;
        fetchPost.post(apiUrl.addTag + id + "/tag/",tags )
        fetchPost.post(apiUrl.createOption + id + "/answers", Object.values(obj))
  }).catch((error) => {
    setPublish(false)
    if(error.response.status === 404)
    {
      props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", { 
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
      },
      TransitionComponent: Zoom,
      autoHideDuration:5000,
      resumeHideDuration: 2000
         })
    }
    else
    {
    props.enqueueSnackbar(error.response.data.error.message, { 
      variant: 'error',
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'right',
    },
    TransitionComponent: Zoom,
    autoHideDuration:5000,
    resumeHideDuration: 2000
       })
      }
  }):   props.enqueueSnackbar("Please Add Atleast Two Options And Select Atleast One Correct Answer", { 
    variant: 'error',
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'right',
  },
  TransitionComponent: Zoom,
  autoHideDuration:5000,
  resumeHideDuration: 2000
     })
      
        
  }
  const handleImageTextInput =(editor,field)=>
  {
    var data=editor.getData()
    var s= ""
    if(data.match(/<img[^>]+src="?([^"\s]+)"?\s*>/g))
    {
      editor.isReadOnly = true;
        var data1=data.matchAll(/<img[^>]+src="?([^"\s]+)"?\s*>/g);
        data1=[...data1]
        data1.map(s1=>s+=s1);
        data=s.split(",")[0];
       
      
    }
    handleCkEditor(data,field)
  }
  
  const Draft = () =>{
   
    const data1 = {
    question_name:value0,
    department:dept ? dept :examiner_dept,
    experience_level:Level,
    question_status:"draft",
    time_duration:Number(t1)*3600+Number(t2)*60+Number(t3)

      };
     
      fetchPost.post(apiUrl.createQuestion, data1).then((res)=>{
        setDraft(true)
        const id = res.data.data.id;
        fetchPost.post(apiUrl.addTag + id + "/tag/",tags )
        fetchPost.post(apiUrl.createOption + id + "/answers", Object.values(obj))
  }).catch((error) => {
    setDraft(false)
    if(error.response.status === 404)
    {
      props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", { 
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
      },
      TransitionComponent: Zoom,
      autoHideDuration:5000,
      resumeHideDuration: 2000
         })
    }
    else
    {
    props.enqueueSnackbar(error.response.data.error.message, { 
      variant: 'error',
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'right',
    },
    TransitionComponent: Zoom,
    autoHideDuration:5000,
    resumeHideDuration: 2000
       })
      }
  })

  }
  useEffect(() => {

    
    if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
      history.push("/login");
  }
   fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
     setDeptlist(res && res.data && res.data.data && res.data.data.results );
   });
   
 }, []);


 // Hide and Show Top Bar
 useEffect(() => {
  let scrollPos = 0;
  const listenToScroll = () => {
    const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    if (winScroll > scrollPos) {
        setTopBarVisible(false);
    } else {
      setTopBarVisible(true);  
    }  
    scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
  };
  document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
  return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
}, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        {publish ? <SuccessDialog  heading="Question Published" path={`/${role}/question-bank/questions`} />:null}
        {draft ? <SuccessDialog  heading="Question Saved In Draft" path={`/${role}/question-bank/questions`}/>:null}
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12} style={{ paddingBottom: "10px" }}>
            <Grid item xl={6} lg={6} md={6} sm={12} align="left" justify="left" style={{ display: "flex" }}>
              <div className={classes.page_heading}>Add New Question</div>
            </Grid>
            <Grid item xl={6} lg={6} md={6} sm={12} className={classes.actionbutton}>
              <div className="button_invite_examiner" style={{ paddingRight: "80px" }}>
                <Button onClick={Draft} className={classes.outlinedButton} variant="contained" color="primary">
                  Save Draft
                </Button>
              </div>
              <div className="button_invite_examiner">
                <Button onClick={ Publish} className={classes.filledButton} variant="contained" color="primary">
                  publish
                </Button>
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
          
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>
                <span className={classes.goBackURL}><Link className={clsx(classes.page_subheading , classes.LinkStyle)}  to={{pathname:`/${role}/question-bank/questions`}}>Question Bank Lobby</Link></span> <span className="spc"></span> &gt; <span className="spc"></span>Add Question
              </div>
            </Grid>
          </Grid>

          <Paper className={classes.paper}>
            <Grid container xs={12}>
           {role==='examiner' ?null: <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left"  style={{ paddingLeft: "60px", paddingTop: "32px" }}>
            <h1 style={{fontWeight:'450',fontSize:'22px'}}>Choose Department</h1>
              
                <div className={classes.examiner_detail_label}>
                  <label>
                   Department
                    <span className={classes.mandotory}>*</span>
                  </label>
                </div>
                <FormControl>
                  <Autocomplete
                  disableClearable
                    input={<OutlinedInput /> }
                    value={dept}
                    options={deptLists}
                    style={{marginTop:'10px',
                    width: "300px"}}                  
                    getOptionLabel={option => capitalize(option.name) || ""}
                    defaultValue = {dept}
                    onChange={(e, newValue) => newValue ? setDept(newValue.id):null }
                    renderInput={params => (
                      <TextField
                        {...params}
                        variant="outlined"
                      />)}
                  />
                </FormControl>
            </Grid>}
              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px", paddingTop: "32px" }}>

                  <div className={classes.editor_style}>
                  <h1 style={{fontWeight:'450',fontSize:'22px'}}>Add Question</h1>
                    <CKEditor
                      className="ck-content"
                      editor={Editor}
                      config={{
                        simpleUpload : {
                          uploadUrl: apiUrl.ckEditorImageUpload,
                          withCredentials: true,
                          headers: {
                            Authorization: `Bearer ${localStorage.getItem('API_TOKEN')}`
                          }
                        },
                      }}
                      data=""
                      onReady={(editor) => {}}
                      onChange={(event, editor) => {
                        var data = editor.getData();
                        setValue0(data);
                      }}
                    />
                  </div>

              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px", paddingTop: "10px" }}>
              <div className={classes.editor_style}>
              <h1  style={{fontWeight:'450',fontSize:'22px'}}>Add Answers</h1>
               {Object.keys(obj).map((field, index) => {
                  return (
                    <div style={{display:'flex'}} key={index}>
                    <div className={clsx("option_wrapper", classes.options_style)}>
                      <label className="container">
                       <input type="checkbox" checked ={obj[field].is_correct} onChange ={(e)=>{checkbox(e,field)}}   />
                           <span className="mark"></span>
                            </label>
                       <div className={classes.editor_style}>
                         <CKEditor
                         id={field}
                          className="ck-content"
                          editor={Editor}
                          config={{
                            simpleUpload : {
                              uploadUrl: apiUrl.ckEditorImageUpload,
                              withCredentials: true,
                              headers: {
                                Authorization: `Bearer ${localStorage.getItem('API_TOKEN')}`
                              }
                            },
                          }}
                          data={obj[field].answer_text}
                          onReady={(editor) => {}}
                          onChange={ ( event, editor) => { 
                             handleImageTextInput(editor,field)
                          }}
                          onBlur={(event, editor) => {}}
                          onFocus={(event, editor) => {}}
                        /></div>
                       <div key={field} >
                        <Button style={{marginTop:'10px'}} onClick={() => handleRemove(field) }>
                          <img src="/assets/Icons/cross_icon.svg" alt="" />
                        </Button>
                        </div>                      
                    </div>
                    <div> {obj[field].is_correct ? <h4 style={{width:'140px',fontSize:'20px',color:'#D9001D'}}>correct answer</h4>:null}</div>
                    </div>
                  );
              })}
                <Button
                  onClick={() => {handleIncrement()}}
                  style={{ marginLeft: "20px", marginTop: "10px" }}
                >
                  <img src="/assets/Icons/add_icon.svg" alt="" style={{ marginRight: "10px" }} />
                  <span className={classes.page_subheading}>Add Option</span>
                </Button>
                </div>
                <Divider  style={{marginTop:'23px'}} />
              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px" }}>
                <div className="timer_wrapper" style={{ display: "flex", marginLeft: "31px", marginTop: "20px", width: "532px" }}>
                  
                  <div className={classes.text_label} >
                    Timer
                  </div>
                  <input
                   type="number"
                   min="0" max="10" 
                    style={{ marginLeft: "31px",width:'auto' }}
                    onChange={(e) => {
                      setT1(e.target.value);
                    }}
                  
                    value={ t1 ? Math.min(10,t1):null}
                    className={classes.time_label}
                  />
                  <span style={{ marginLeft: "5px", marginRight: "6px" }}>h :</span>
                  <input
                   type="number"
                    min="0" max="59" 
                   style={{width:'auto'}}
                    onChange={(e) => {
                      setT2(e.target.value);
                    }}
                    value={ t2 ? Math.min(59,t2):null}
                    className={classes.time_label}
                  />
                  <span style={{ marginLeft: "5px", marginRight: "6px" }}>m :</span>
                  <input
                   type="number"
                   min="0" max="59" 
                   style={{width:"auto"}}
                    onChange={(e) => {
                      setT3(e.target.value);
                    }}
                    value={t3 ? Math.min(59,t3):null}
                    className={classes.time_label}
                  />
                    <span style={{ marginLeft: "5px"}}>s</span>
                </div>
                <Divider  style={{marginTop: "20px"}}  />
                <div className={classes.text_label} style={{ marginLeft: "28px", marginTop: "20px" }}>
                  Tags
                </div>
                <div className={classes.tag_label} style={{display:'block'}}>
                <ChipInput
                 disableUnderline
                 placeholder='Type here'
                 alwaysShowPlaceholder={true}
                 delayBeforeAdd={true}
                onKeyUp ={ (e)=> tagChange(e)}
                 value={tag}
                 onAdd={(chip) => handleAddChip(chip)}
                 onDelete={(chip, index) => handleDeleteChip(chip, index)}
                 />
                 {
                   tagList.map((qw)=>
                   <MenuItem value={qw.tag_name}   onClick={  (e)=>{handleAddChip(qw.tag_name)}}>
                     {qw.tag_name}
                     </MenuItem>
                   )
                 }
                </div>
                <Divider  style={{marginTop: "20px"}}  />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="left" justify="left" style={{ paddingLeft: "32px" }}>
              <div style={{ display: "flex", flexDirection: "column" }}>
                  <Typography className={classes.experience_level_label}>Question Level</Typography>
                  <div className={classes.exam_name_box} style={{marginBottom:'20px'}}>
                    <RadioGroup  value={Level} onChange={(e)=>{setLevel(e.target.value)}} aria-label="gender" name="gender1" >
                      <FormControlLabel value="senior" control={<Radio />} label="Senior" />
                      <FormControlLabel value="junior" control={<Radio />} label="Junior" />
                      <FormControlLabel value="internship" control={<Radio />} label="Internship" />
                    </RadioGroup>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Paper>

          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
export default  withSnackbar(AddNewQuestion)