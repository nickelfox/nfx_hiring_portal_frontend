import { useHistory, Checkbox, InputAdornment, VisibilityOff, Visibility, IconButton, Formik, Field, Yup, Form, Grid, makeStyles, Button, Typography, React, Zoom, fetchPost, apiUrl, useEffect } from "allImport";
import { askForPermissioToReceiveNotifications } from "pushNotification";
import { TextField } from "formik-material-ui";
import "style/style.css";
import { EXAMINER_DEPT, ROLE, ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
import { commonStyle } from "commonStyle.js";
import { withSnackbar } from "notistack";

const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),

  signin_heading: {
    fontFamily: "Mulish",
    fontSize: "50px",
    fontStyle: "normal",
    fontWeight: "700",
    lineHeight: "63px",
    width: "430px",
    height: "63px",
    marginLeft: "120px",
    marginTop: "197px",

    color: "#18273B",
  },
  signin_button_box: {
    marginLeft: "120px",
    paddingTop: "32px",
  },
  signin_button: {
    color: "#FFFFFF",
    background: " #D9001D",
    height: "52px",
    width: "157px",
    left: "0px",
    top: "0px",
    borderRadius: "4px",
  },

  signin_mail_label: {
    marginLeft: "120px",
    marginTop: "320 px",
    color: "#444444",
    fontFamily: "Open Sans",
    fontSize: "15px",
    fontStyle: "normal",
    fontWeight: " 400",
    lineHeight: " 24px",
    letterSpacing: "0.5px",
    textAlign: "left",
  },
  signin_password_label: {
    marginLeft: "120px",
    marginTop: "24px",
    color: "#444444",
    fontFamily: "Open Sans",
    fontSize: "15px",
    fontStyle: "normal",
    fontWeight: " 400",
    lineHeight: " 24px",
    letterSpacing: "0.5px",
    textAlign: "left",
  },
  signin_mail_box: {
    paddingTop: "10px",
    marginLeft: "120px",
    paddingBottom: "39px",
    width: "540px",
    height: "46px",
  },
  signin_password_box: {
    marginLeft: "120px",
    marginTop: "10px",
    width: "540px",
    height: "46px",
  },
  signin_description: {
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 600,
    lineHeight: "32px",
    letterSpacing: "0.10000000149011612px",
    textAlign: "left",
    color: "#A8A8A8",
    marginLeft: "120px",
    marginTop: "24px",

    height: "64px",
    width: "471px",
  },
  inputfeedback: {
    marginTop: "10px",
    marginLeft: "120px",
  },

  signin_checkbox: {
    display: "flex",
    marginLeft: "120px",

    width: "19px",
    height: "18px",
    padding: "0px",

    backgroundColor: "white",
    marginTop: "41.61px",
  },

  signin_checkbox_line: {
    display: "flex",
  },
  signin_form: {
    marginTop: "32px",
  },
  signin_rememberme: {
    paddingLeft: "8px",
    paddingTop: "41px",
    fontSize: "14px",
  },
  signin_forgotpassword: {
    paddingLeft: "295px",
    paddingTop: "41px",
    fontSize: "14px",
  },
}));
const LogIn = (props) => {
  const classes = useStyles();
  const [Checked, setChecked] = React.useState(false);
  const role = localStorage.getItem("ROLE") ? localStorage.getItem("ROLE").toLowerCase() : null;
  const [showPassword, setShowPassword] = React.useState(false);
  const [deviceToken, setDeviceToken] = React.useState("123");
  const history = useHistory();
  const togglePasswordVisiblity = () => {
    setShowPassword(showPassword ? false : true);
  };

  const setupFirebase = async () => {
    let deviceToken = await askForPermissioToReceiveNotifications();
    if (deviceToken) {
      setDeviceToken(deviceToken);
    }
  };
  const HandleRememberme = () => {
    setChecked(!Checked);
  };
  useEffect(() => {
    setupFirebase();
    if (StorageManager.get(API_TOKEN) && StorageManager.get(ADMIN_ID) && StorageManager.get(ADMIN_NAME) && StorageManager.get(LOGOUT_TOKEN)) {
      history.push(`/${role}/dashboard`);
    } else {
      StorageManager.clearStore();
      history.push("/login");
    }
  }, []);

  return (
    <Grid container style={{ maxHeight: "100vh", overflow: "auto" }} lg={12} xl={12} md={12}>
      <Grid item sm={8} lg={5} xl={5} md={6}>
        <div className={classes.leftbox}>
          <img src="assets/Icons/Nickelfoxlogo.svg" alt="nickelfoxlogo" className={classes.nickelfoxlogo} />
          <Typography className={classes.heading}>NickelFox</Typography>
          <Typography className={classes.subtext}>Hiring Portal</Typography>
          <Typography className={classes.leftbigtext}>We’ve been Expecting you Fox!</Typography>
          <Typography className={classes.lefttext} style={{}}>
            Sign into your account and continue recruiting Fresh Foxes for the Team.
          </Typography>
        </div>
      </Grid>
      <Grid item sm={4} md={6} lg={7} xl={7}>
        <Typography variant="h3" className={classes.signin_heading}>
          Sign In
        </Typography>

        <Typography className={classes.signin_description}>This is an Internal Nickelfox Portal accesible only to the Employees of the Company</Typography>

        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={(values, { resetForm, setSubmitting }) => {
            var data = JSON.stringify({
              email: values.email,
              password: values.password,
              device: {
                device_token: deviceToken,
                device_type: "WEB",
                is_safari: 0,
              },
            });
            fetchPost
              .post(`${apiUrl.login}`, data)
              .then((res) => {
                if (res.data.success) {
                  const { refresh, access } = res.data.data.tokens;
                  const { role, id, full_name, department } = res.data.data;

                  localStorage.setItem("rememberMe", Checked);
                  localStorage.setItem("user", Checked ? values.email : "");
                  localStorage.setItem("Password", Checked ? values.password : "");
                  StorageManager.put(API_TOKEN, access);
                  StorageManager.put(ROLE, role);
                  StorageManager.put(ADMIN_ID, id);
                  StorageManager.put(ADMIN_NAME, full_name);
                  StorageManager.put(LOGOUT_TOKEN, refresh);
                  if (role === "SUB-ADMIN") {
                    StorageManager.putJSON(SUB_ADMIN_PERMISSION, res.data.data.permissions.permission);
                  }
                  if (role === "EXAMINER") {
                    StorageManager.put(EXAMINER_DEPT, department);
                  }

                  props.enqueueSnackbar("successfully logged in.", {
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                    TransitionComponent: Zoom,
                    autoHideDuration: 5000,
                  });

                  history.go(`/${role.toLowerCase()}/dashboard`);
                } else {
                  props.enqueueSnackbar("token expired.", {
                    variant: "error",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                    TransitionComponent: Zoom,
                    autoHideDuration: 5000,
                    resumeHideDuration: 2000,
                  });
                  resetForm();
                }
              })
              .catch((error) => {
                props.enqueueSnackbar(error.response.data.error.message, {
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                  autoHideDuration: 1000,
                  TransitionComponent: Zoom,
                });
                resetForm();
              });
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string().email().required("Required"),
            password: Yup.string()
              .required("No password provided.")
              .min(7, "Password is too short - should be 7 chars minimum.")
              .matches(/(?=.*[0-9])/, "Password must contain a number."),
          })}
        >
          {(props) => {
            const {
              touched,
              errors,
              values,
              handleChange,
              handleBlur,

              handleSubmit,
            } = props;
            return (
              <Form onSubmit={handleSubmit} className={classes.signin_form}>
                <label htmlFor="email" className={classes.signin_mail_label}>
                  Email ID*
                </label>{" "}
                <br />
                <Field placeholder="Enter Your Email" name="email" type="text" component={TextField} margin="none" value={(values.email = localStorage.getItem("rememberMe") === "true" ? localStorage.getItem("user") : values.email)} variant="outlined" onChange={handleChange} onBlur={handleBlur} helperText={touched.email ? errors.email : ""} error={touched.email && Boolean(errors.email)} className={classes.signin_mail_box} /> <br />
                <label htmlFor="password" className={classes.signin_password_label}>
                  Password*
                </label>{" "}
                <br />
                <Field
                  placeholder="Enter Your Password"
                  name="password"
                  component={TextField}
                  value={(values.password = localStorage.getItem("rememberMe") === "true" ? localStorage.getItem("Password") : values.password)}
                  margin="normal"
                  variant="outlined"
                  onBlur={handleBlur}
                  helperText={touched.password ? errors.password : ""}
                  error={touched.password && Boolean(errors.password)}
                  onChange={handleChange}
                  type={showPassword ? "text" : "password"}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton aria-label="toggle password visibility" onClick={togglePasswordVisiblity}>
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  className={classes.signin_password_box}
                />
                <div className={classes.signin_checkbox_line}>
                  <Checkbox className={classes.signin_checkbox} checked={Checked} onChange={HandleRememberme} />
                  <Typography className={classes.signin_rememberme}>Remember me</Typography>
                  <Typography onClick={() => (window.location.href = "/forgot-password")} className={classes.signin_forgotpassword}>
                    <span style={{ cursor: "pointer" }}>Forgot Password?</span>
                  </Typography>
                </div>
                <div className={classes.signin_button_box}>
                  <Button type="submit" variant="contained" className={classes.signin_button}>
                    SIGN IN
                  </Button>
                </div>
              </Form>
            );
          }}
        </Formik>
      </Grid>
    </Grid>
  );
};

export default withSnackbar(LogIn);
