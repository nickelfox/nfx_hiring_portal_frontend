import {Autocomplete,withSnackbar, clsx, Zoom, useHistory, FormControl, fetchClient, apiUrl, useEffect, OutlinedInput, fetchPost, FormControlLabel, Link, RadioGroup, Radio, CssBaseline, Box, Container, Grid, makeStyles, Paper, Button, TextField, Typography, Topbar, Sidebar, Copyright, React } from "allImport";
import { commonStyle } from "commonStyle.js";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
import "style/style.css";
const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),

  exam_name_label: {
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 600,
    lineHeight: "32px",
    letterSpacing: "0.10000000149011612px",
    textAlign: "left",
    paddingTop: "34.63px",
    paddingLeft: "32px",
    height: "34.63px",
  },
  experience_level_label: {
    fontFamily: "Open Sans",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: 600,
    lineHeight: "32px",
    letterSpacing: "0.10000000149011612px",
    textAlign: "left",
    paddingTop: "32px",
    paddingLeft: "32px",
    height: "32px",
  },
  exam_name_box: {
    paddingTop: "45.37px",
    paddingLeft: "32px",
  },
}));

const Createexam = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [newExamName, setnewExamName] = React.useState("");
  const [deptLists, setDeptlist] = React.useState([]);
  const [dept, setDept] = React.useState();
  const [newExperienceLevel, setnewExperienceLevel] = React.useState("");
  const adminID = localStorage.getItem("ADMIN_ID");
  const examiner_dept = localStorage.getItem("EXAMINER_DEPT") ? localStorage.getItem("EXAMINER_DEPT"):null;
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const createExamField = (e) => {
    setnewExamName(e.target.value);
  };

  const experienceLevelSelect = (e) => {
    setnewExperienceLevel(e.target.value);
  };

  const Publish = () => {
    const data = {
      exam_name: newExamName,
      experience_level: newExperienceLevel,
      created_by: adminID,
      duration: 10,
      department: dept ? dept :examiner_dept,
      status: "unpublish",
    };
    console.log(data);
    if(!data.department){
      props.enqueueSnackbar("Please select a department", {
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
        TransitionComponent: Zoom,
        autoHideDuration: 3000,
      });
      return;
    }
    fetchPost
      .post(apiUrl.createExam, data)
      .then((res) => {
        history.replace({
          pathname:  `/${role}/exam/question`,
          state: { ExamID: res.data.data.id,Department:res.data.data.department },
        });
      })
      .catch((error) => {
        props.enqueueSnackbar(error.response.data.error.message, {
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
          TransitionComponent: Zoom,
          autoHideDuration: 3000,
        });
      });
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }

    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading}>Create Exam</div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
              <div className="button_invite_examiner">
                <div className="button_invite_examiner">
                  <Button onClick={Publish} className={classes.filledButton} variant="contained" color="primary">
                    Next
                  </Button>
                </div>
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>
                <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname:  `/${role.toLowerCase()}/exam` }}>
                  Exams Home
                </Link>{" "}
                <span className="spc"></span> &gt; <span className="spc"></span> Create Exam
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} xs={12} sm={12}>
              <Paper className={classes.paper} style={{ height: "158px" }}>
                <Grid container xs={12}>
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    <Typography className={classes.exam_name_label}>Exam Name</Typography>
                    <div className={classes.exam_name_box}>
                      <TextField onChange={(e) => createExamField(e)} variant="outlined" placeholder="Exam Name" style={{ width: "291px", height: "46px" }} />
                    </div>
                  </div>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} xs={12} sm={12}>
            {role==='examiner' ? null:<Paper className={classes.paper} style={{ height: "158px" }}>
               <Grid container xs={12}>
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    <Typography className={classes.exam_name_label}>Department</Typography>
                    <div className={classes.exam_name_box}>
                    <FormControl>
                      <Autocomplete
                      disableClearable
                        input={<OutlinedInput /> }
                        value={dept}
                        options={deptLists}
                        style={{marginTop:'10px',
                        width: "300px"}}                      
                        getOptionLabel={option => option.name || ""}
                        defaultValue = {dept}
                        onChange={(e, newValue) => newValue ? setDept(newValue.id):null }
                        
                        renderInput={params => (
                          <TextField
                          
                            {...params}
                            variant="outlined"
                         />)}
                      />
                    </FormControl> 
                    </div>
                  </div>
                </Grid>
              </Paper>}
            </Grid>
          </Grid>
          <Grid container xs={12} style={{ paddingTop: "20px" }}>
            <Grid item lg={12} xl={12} xs={12} sm={12}>
              <Paper className={classes.paper} style={{ height: "215px" }}>
                <div style={{ display: "flex", flexDirection: "column" }}>
                  <Typography className={classes.experience_level_label}>Exam Level</Typography>
                  <div className={classes.exam_name_box}>
                    <RadioGroup aria-label="gender" name="gender1" value={newExperienceLevel} onChange={(e) => experienceLevelSelect(e)}>
                      <FormControlLabel value="senior" control={<Radio />} label="Senior" />
                      <FormControlLabel value="junior" control={<Radio />} label="Junior" />
                      <FormControlLabel value="internship" control={<Radio />} label="Internship" />
                    </RadioGroup>
                  </div>
                </div>
              </Paper>
            </Grid>
          </Grid>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
};
export default withSnackbar(Createexam);
