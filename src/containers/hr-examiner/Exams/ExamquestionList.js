import {Emptyscreen, clsx, fetchUpdate, Pagination, DeleteAlert, fetchDelete, Menu, MenuItem, Select, OutlinedInput, CancelRoundedIcon, useEffect, fetchClient, apiUrl, Collapse, KeyboardArrowDownIcon, KeyboardArrowUpIcon, withStyles, FormControlLabel, Checkbox, Chip, Table, TableBody, TableCell, CircularProgress, TableContainer, TableHead, TableRow, Link, useHistory, CssBaseline, Box, Container, Grid, Paper, Button, IconButton, InputAdornment, TextField, FormControl, NativeSelect, Topbar, Sidebar, Copyright, React, SearchIcon } from "allImport";
import { useStyles } from "style/Liststyle.js";
import { MenuProps, MouseEnter, MouseLeave } from "Common/CommonFunction";
import { useLocation } from "react-router-dom";
import "style/style.css";
import { ROLE, ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
import { withSnackbar } from "notistack";
var AS = [];
var searchValue = "";
var url = new URL(`${apiUrl.viewQuestions}`);

const  ExamquestionList=(props)=> {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const [deptLists, setDeptlist] = React.useState([]);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const [TagList, setTagslist] = React.useState([]);
  const [open, setOpen] = React.useState([]);
  const [totalPage, setTotalPage] = React.useState("");
  const [level, setLevel] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [count, setCount] = React.useState("");
  const [questionSearch, setQuestionSearch] = React.useState("");
  const [Checked, setChecked] = React.useState(false);
  const [Arr, setArr] = React.useState([]);
  const [questions, setQuestions] = React.useState([]);
  const qIDS = location.state.questionIDs;
  
  var [Questionids, setQuestionIDs] = React.useState([]);

  const ExamID = location.state.ExamID;
  
  const department = localStorage.getItem("EXAMINER_DEPT") ? localStorage.getItem("EXAMINER_DEPT"):location.state.Department;
  // const [id,setID]=React.useState("");

  const [QuestionsList, setQuestionslist] = React.useState([]);
  const [page, setPage] = React.useState(1);

  console.log("UU", qIDS);
  var handleOnload = (id) => {
    let tempQuestionIDs = Questionids;

    if (tempQuestionIDs.indexOf(id) > -1) {
      console.log("ID true ::::::::::: ", id);

      return true;
    } else {
      console.log("ID false ::::::::::: ", id);

      return false;
    }
  };

  const handleChange = (id) => {
    console.log("Handle ID", id);
    //if(event.target.checked)
    // {
    let tempQuestionIDs = Questionids;
    if (tempQuestionIDs.some((item) => item === id)) {
      const index = tempQuestionIDs.indexOf(id);
      tempQuestionIDs.splice(index, 1);

      fetchDelete.delete(`${apiUrl.removeExamQuestion}` + ExamID + "/question/map", { data: { question: [id] } });
    } else {
      tempQuestionIDs.push(id);
    }
    setQuestionIDs(tempQuestionIDs);
    localStorage.setItem(ExamID, tempQuestionIDs);
    console.log("handleChange", tempQuestionIDs);
  };

  const handleChangePage = (event, value) => {
    console.log("handleChangePage", Questionids);
    if (Questionids) {
      const data = {
        question: Questionids,
      };
      fetchUpdate.patch(`${apiUrl.mapQuestion}` + ExamID + "/question/map/", data);
    }
    let url = `${apiUrl.viewQuestions}?question_status=publish&page=` + value;
   
    url += level === "" ? "" : `&experience_level=${level}`;
   
    url += questionSearch === "" ? "" : `&search=${questionSearch}`;
    url += department === "" ? "" : `&department=${department}`;

    fetchClient.get(url).then((res) => {
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setQuestionslist(res && res.data && res.data.data && res.data.data.results);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(QuestionsList);
      console.log("Page ::::::::::: ", res.data.data.results);
    });
    setPage(value);
  };

  function createData(ColA, ColB, ColC, ColD, id) {
    return { ColA, ColB, ColC, ColD, id };
  }

  const colmConversion = (arr) => {
    console.log("56", Questionids);
    AS = [];
    console.log(arr);
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].question_name, arr[index] && arr[index].experience_level, arr[index] && new Date(arr[index].created_at).toLocaleDateString("en-IN"), arr[index] && arr[index].tags, arr[index] && arr[index].id));
      console.log(arr[index].tags);

      console.log("||", arr[index] && arr[index].id);
    }

    setArr(AS);
  };

  const changePage = () => {
    filterQuestions("");
  };

  const filterQuestions = (e) => {
    setLevel("")
    setQuestionSearch(e.target.value);
    searchValue = e.target.value;
    fetchClient.get(`${apiUrl.viewQuestions}?search=` + searchValue + `&question_status=publish&&department=`+department).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      colmConversion(QuestionsList);
    });
   
    // }
  };
  const examID = props && props.location && props.location.state && props.location.state.ExamID;
  const [chipData, setChipData] = React.useState([
    { key: 0, label: "java" },
    { key: 1, label: "SQL" },
    { key: 2, label: "Javascript" },
    { key: 3, label: "React" },
    { key: 4, label: "Vue.js" },
  ]);
  const questionFilterSelection = (e) => {
    setLevel(e.target.value);
    setQuestionSearch("")
    if (e.target.value !== "") {
      url.searchParams.set("experience_level", e.target.value);
      fetchClient.get(url+`&question_status=publish&department=`+department).then((res) => {
        var QuestionsList = res && res.data && res.data.data && res.data.data.results;
        setQuestionslist(res && res.data && res.data.data && res.data.data.results);
        setTotalPage(res && res.data && res.data.data && res.data.data.pages);
        setCount(res && res.data && res.data.data && res.data.data.count);
        colmConversion(QuestionsList);
      });
    } else {
      url.searchParams.delete("experience_level");
      fetchClient.get(url+`?question_status=publish&department=`+department).then((res) => {
        var QuestionsList = res && res.data && res.data.data && res.data.data.results;
        setQuestionslist(res && res.data && res.data.data && res.data.data.results);
        setTotalPage(res && res.data && res.data.data && res.data.data.pages);
        setCount(res && res.data && res.data.data && res.data.data.count);
        colmConversion(QuestionsList);
      });
    }

   
  };

  const handleExpand = (index) => {
    let temp = open;
    temp[index] = !temp[index];
    setOpen([...temp]);
  };

  const clear = () => {
    setQuestionSearch("");
    fetchClient.get(`${apiUrl.viewQuestions}?question_status=publish&department=`+department).then((res) => {
      var QuestionsList = res && res.data && res.data.data && res.data.data.results;
      setQuestionslist(res && res.data && res.data.data && res.data.data.results);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(QuestionsList);
    });
  };

  console.log("rrr", ExamID);
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    let questionids = [];
    setQuestionIDs(qIDS ? questionids.concat(qIDS) : questionids);

    fetchClient.get(`${apiUrl.viewQuestions}?question_status=publish&department=`+department).then((res) => {
      const temp = res.data;
      const temp1 = temp.data;
      const temp2 = temp1.results;
      const temp3 = temp2.tag;

      console.log(res);
      setQuestionslist(res && res.data && res.data.data && res.data.data.results);
      setTagslist(temp3);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count);
      const questionList = res && res.data && res.data.data && res.data.data.results;
      colmConversion(questionList);
    });

    fetchClient.get(`${apiUrl.deptList}`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
    
  }, []);
  const mapQuestion = () => {
    const data = {
      question: Questionids,
    };
    if(Questionids.length>0)
    {
   fetchUpdate.patch(`${apiUrl.mapQuestion}` + ExamID + "/question/map/", data).then((res) => {
      history.replace({
        pathname: `/${role}/exam/question/select-question`,
        state: { ExamID: res.data.data.exam.id, ExamName: res.data.data.exam.exam_name },
      });
    })
  }
  else
  {
    props.enqueueSnackbar("Please Add Atleast One Question", {
      variant: "error",
       autoHideDuration:3000,
     
    });
  }
  };

  // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
           setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading} style={{ width: "140%" }}>
                Question Bank - Select Questions
              </div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
             <div className="button_invite_examiner">
                <div className="button_invite_examiner">
                {count===0 ? 
                 <Link to={{
                  pathname:`/${role}/question-bank/questions/add`,
                }}
                ><Button  className={classes.filledButton} variant="contained" color="primary">
                   Create Question
                  </Button></Link>: <Button onClick={mapQuestion} className={classes.filledButton} variant="contained" color="primary">
                    Next
                  </Button>}
                </div>
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>
                <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname: `/${role}/exam` }}>
                  Exams Home
                </Link>{" "}
                <span className="spc"></span> &gt; <span className="spc"></span>{" "}
                <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname:  `/${role}/exam/create`}}>
                  {" "}
                  Create Exam{" "}
                </Link>
                <span className="spc"></span> &gt; <span className="spc"></span> Question Bank
              </div>
            </Grid>
          </Grid>
         <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper}>
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} align="left" justify="left" style={{ display: "flex" }}>
                        <div className={classes.all_examiners_heading}> All Questions</div>
                        <div className={classes.examiner_nos}>{count}</div>
                      </Grid>

                      <Grid item lg={3} xl={3} md={3} sm={3} align="center" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            // type="search"
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            onChange={(e) => filterQuestions(e)}
                            variant="outlined"
                            value={questionSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: questionSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={clear}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} align="right" justify="right" style={{ paddingRight: "24px" }}>
                        <div className={classes.examinerDept}>
                          <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "142px" }}>
                            {/* <InputLabel htmlFor="name-multiple">Division</InputLabel> */}
                            <Select id="demo-customized-select-native" displayEmpty value={level} onChange={(e) => questionFilterSelection(e)} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Level
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="senior"
                              >
                                Senior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="junior"
                              >
                                Junior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="internship"
                              >
                                Internship
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </div>
                      </Grid>
                    </Grid>

                    { count===0 ?<Emptyscreen/>: <Grid container xs={12}>
                      <div className={classes.examiners_table_wrapper}>
                        <TableContainer component={Paper}>
                          <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                              <TableRow>
                                <TableCell style={{ width: "0px" }} />
                                <TableCell />
                                <TableCell align="left">Questions</TableCell>
                                <TableCell align="left">Level</TableCell>
                                <TableCell align="left">Date Added</TableCell>
                                <TableCell align="left">Tag</TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {Arr.map((row, index) => (
                                <TableRow key={row.name}>
                                  <TableCell style={{ paddingTop: "9px", paddingBottom: "9px", paddingLeft: "24px" }} component="th" scope="row">
                                    {/* <Checkbox value="checkbox" style={{color:'black'}}/> */}
                                    <label className="container">
                                      <input key={`exam-ques-${row.id}`} type="checkbox" defaultChecked={handleOnload(row.id)} onChange={() => handleChange(row.id)} />
                                      <span className="mark"></span>
                                    </label>
                                  </TableCell>
                                  <TableCell>
                                    <div style={{ width: "100%", height: "100%" }}>
                                      <IconButton aria-label="expand row" size="small" onClick={() => handleExpand(index)}>
                                        {open[index] ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                                      </IconButton>
                                    </div>
                                  </TableCell>
                                    <TableCell align="left">
                                      <div className={!open[index] ? classes.collapseRow : ""} style={{ maxWidth: 440 }}>
                                        <span dangerouslySetInnerHTML={{ __html: row.ColA.replace("<img", `<img style="max-width: 350px; border-radius: 4px; border: 1px solid #0000001A"`) }}></span>
                                      </div>
                                    </TableCell>
                                  <TableCell align="left">{row.ColB}</TableCell>
                                  <TableCell align="left">{row.ColC}</TableCell>
                                  <TableCell align="left">
                                    <div className={classes.tagsContainer}>
                                      {row.ColD.map((i) => (
                                        <div className={classes.tag}>{i.tag}</div>
                                      ))}
                                    </div>
                                  </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </div>
                    </Grid>}
                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
export default withSnackbar(ExamquestionList);