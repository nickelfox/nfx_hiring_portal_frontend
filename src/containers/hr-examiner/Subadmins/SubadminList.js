import { CircularProgress, useHistory, Emptyscreen, CancelRoundedIcon, Select, MenuItem, OutlinedInput, Link, useEffect, fetchClient, apiUrl, Commontable, CssBaseline, Box, Container, Grid, Paper, Button, IconButton, InputAdornment, TextField, FormControl, Topbar, Sidebar, Copyright, React, SearchIcon, Pagination } from "allImport";
import { useStyles } from "style/Liststyle.js";
import { MenuProps, MouseEnter, MouseLeave, convertUTCDateToLocalDate } from "Common/CommonFunction";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
var url = new URL(`${apiUrl.viewSubadmins}`);
var AS = [];
var searchValue = "";
var jobIDs = [];
export default function SubadminList() {
  const classes = useStyles();
  const history = useHistory();
  const [data, setAllData] = React.useState({});
  const role = localStorage.getItem("ROLE") ? localStorage.getItem("ROLE").toLowerCase() : null;
  const [deptSelectionVal, setDept] = React.useState("");
  const [totalPage, setTotalPage] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [count, setCount] = React.useState("");
  const [subadminSearch, setSubadminSearch] = React.useState("");
  const [loader, setLoader] = React.useState(false);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const [page, setPage] = React.useState(1);
  const handleChangePage = (event, value) => {
    let url = `${apiUrl.viewSubadmins}?page=` + value;
    url += subadminSearch === "" ? "" : `&search=${subadminSearch}`;
    url += status === "" ? "" : `&status=${status}`;
    url += deptSelectionVal === "" ? "" : `&department=${deptSelectionVal}`;
    fetchClient.get(url).then((res) => {
      var SubadminsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(SubadminsList);
    });
    setPage(value);
  };

  function createData(ColA, ColB, ColC, ColD, ColE, ColF) {
    return { ColA, ColB, ColC, ColD, ColE, ColF };
  }

  const colmConversion = (arr) => {
    AS = [];
    console.log(arr.length);
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].id, arr[index].full_name, arr[index] && arr[index].status, arr[index] && convertUTCDateToLocalDate(new Date(arr[index].created_at)).toLocaleDateString("en-IN")));
      jobIDs.push(arr[index] && arr[index].id);
    }
    processDataList();
  };

  const filterSubadmins = (e) => {
    setStatus("");
    setDept("");
    setSubadminSearch(e.target.value);
    searchValue = e.target.value;
    fetchClient.get(`${apiUrl.viewSubadmins}?search=` + searchValue).then((res) => {
      var subadminsList = res && res.data && res.data.data && res.data.data.results;
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(subadminsList);
    });
  };

  const subadminFilterSelection = (e, type) => {
    setSubadminSearch("");
    if (type === "Status") {
      setStatus(e.target.value);
      if (e.target.value !== "") {
        url.searchParams.set("status", e.target.value);
      } else {
        url.searchParams.delete("status");
      }
    }
    if (type === "departmentSelect") {
      setDept(e.target.value);

      if (e.target.value !== "") {
        url.searchParams.set("department", e.target.value);
      } else {
        url.searchParams.delete("department");
      }
    }

    fetchClient.get(url).then((res) => {
      var subadminsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(subadminsList);
    });
  };
  const clear = () => {
    setSubadminSearch("");
    fetchClient.get(`${apiUrl.viewSubadmins}`).then((res) => {
      var subadminsList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(subadminsList);
    });
  };

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if (role === "sub-admin") {
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.view = allPermissions.includes("VIEW_SUBADMIN");
      permission.create = allPermissions.includes("CREATE_SUBADMIN");
      permission.edit = allPermissions.includes("EDIT_SUBADMIN");
      permission.delete = allPermissions.includes("DELETE_SUBADMIN");
    }
    return permission;
  };

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    fetchClient
      .get(`${apiUrl.viewSubadmins}`)
      .then((res) => {
        setTotalPage(res && res.data && res.data.data && res.data.data.pages);
        setCount(res && res.data && res.data.data && res.data.data.count);
        const SubadminsArray = res && res.data && res.data.data && res.data.data.results;
        colmConversion(SubadminsArray);
        setLoader(true);
      })
      .catch(() => {
        setLoader(true);
      });
  }, []);
  const processDataList = () => {
    const permissions = getPermissionsForSubAdmin();
    const data = {
      columnnames: {
        col1: "ID",
        col2: "Name",
        col3: "Status",
        col4: "DateAdded",
      },

      List: AS,
      ListKey: Object.keys(AS),
      permissions: permissions,
      TableType: "Subadmin",
      EditPath: `/${role}/subadmins/edit-subadmin`,
      profile: `/${role}/subadmins/profile`,
    };
    setAllData(data);
  };

  // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector("#main-content")?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
        setTopBarVisible(false);
      } else {
        setTopBarVisible(true);
      }
      scrollPos = document.querySelector("#main-content")?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector("#main-content")?.addEventListener("scroll", listenToScroll);
    return () => document.querySelector("#main-content")?.removeEventListener("scroll", listenToScroll);
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading}>Sub Admins</div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
              <div className="button_invite_examiner">
                {role === "sub-admin" && data.permissions && !data.permissions.create ? null : (
                  <Link to={{ pathname: `/${role}/subadmins/create-subadmin` }}>
                    <Button className={classes.filledButton} variant="contained" color="primary">
                      Add Sub Admin
                    </Button>
                  </Link>
                )}
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>Sub Admins Lobby</div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper}>
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} style={{ display: "flex" }} align="left" justify="left">
                        <div className={classes.all_examiners_heading}>All Subadmins</div>
                        <div className={classes.examiner_nos} style={{ marginLeft: "-10px" }}>
                          {count}
                        </div>
                      </Grid>

                      <Grid item lg={3} xl={3} md={3} sm={3} align="center" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            variant="outlined"
                            value={subadminSearch}
                            onChange={(e) => filterSubadmins(e)}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: subadminSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={() => clear()}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                        </div>
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} align="right" justify="right">
                        <div className={classes.multiDropdown}>
                          <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "122px" }}>
                            {/*<InputLabel htmlFor="demo-customized-select-native">Division</InputLabel>*/}
                            <Select displayEmpty id="demo-customized-select-native" value={status} onChange={(e) => subadminFilterSelection(e, "Status")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                key="0"
                                value=""
                              >
                                Status
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="Active"
                              >
                                Active
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="Inactive"
                              >
                                Inactive
                              </MenuItem>
                            </Select>
                          </FormControl>
                        </div>
                      </Grid>
                    </Grid>

                    {loader ? (
                      <>
                        {count ? (
                          <Grid container xs={12}>
                            <div className={classes.examiners_table_wrapper}>
                              <Commontable data={data} />
                            </div>
                          </Grid>
                        ) : (
                          <Emptyscreen image="/assets/Icons/SubadminEmptyIcon.svg" />
                        )}
                      </>
                    ) : (
                      <Paper className={classes.paper} style={{ height: "380px", justifyContent: "center", alignItems: "center" }}>
                        <CircularProgress color="error" />
                      </Paper>
                    )}
                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
