import { CssBaseline, Box, Topbar, Sidebar, Copyright, React, Emptyscreen } from "allImport";
import { useStyles } from "style/Liststyle.js";

export default function Subadmin() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Emptyscreen />
        <Box pt={4}>
          <Copyright />
        </Box>
      </main>
    </div>
  );
}
