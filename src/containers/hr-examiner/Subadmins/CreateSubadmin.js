import  {useHistory,useEffect,CssBaseline,Topbar,Sidebar,React} from 'allImport';
import { useStyles } from "style/Editstyle.js";
import Commonfieldscreen from 'components/CommonScreen/Commonfieldscreen';
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function CreateSubadmin(props) {
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const classes = useStyles();
  const history=useHistory();
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const data ={
    MainHeading:"Add Sub Admin" ,
    MainSubheading1:"Sub Admins Lobby",
    MainSubheading2:"Add Subadmin",
    PaperHeading:"Personal Details",
    Type:"Subadmin",
    
    Labels: {
        label1: "Full Name",
        label3: "Email ID ",
        label4: "Designation", label5: "Department",
      },
      DetailBox: {
        box1: "",
        box3: "",
        box4: "",
      
      },
      Placeholder:{
        placeholder1:"Enter full name",
        placeholder3:"Enter registered email id",
        placeholder4:"Enter designation",
       
      },
      Button: {
        button1: "Cancel",
        button2: "Save",
      },
     
      height:"312px",
      //${role}/examinerID:examinerID,
      field : "Add",
      fieldType: "addSubadmin",
      home:`/${role}/subadmins`
    };
  
    useEffect(() => {
      if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
        history.push("/login");
    }
    }, [])

    // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
          setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
          <Commonfieldscreen data={data}/>
      </main>
    </div>
  );
}