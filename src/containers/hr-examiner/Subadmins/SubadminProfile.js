import { Paper, CircularProgress, useHistory, CssBaseline, Topbar, Sidebar, React, fetchClient, apiUrl, useEffect, useState } from "allImport";
import Commonprofile from "components/Commonprofile/Commonprofile";
import useStyles from "style/Profilestyle.js";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function SubadminProfile(props) {
  const classes = useStyles();
  const [data, setData] = useState({});
  const [loader, setLoader] = React.useState(false);
  const history = useHistory();
  const role = localStorage.getItem("ROLE") ? localStorage.getItem("ROLE").toLowerCase() : null;
  const [topbarVisible, setTopBarVisible] = React.useState(true);

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if (role === "sub-admin") {
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.edit = allPermissions.includes("EDIT_SUBADMIN");
      permission.delete = allPermissions.includes("DELETE_SUBADMIN");
    }
    return permission;
  };

  const setSubAdminData = (subAdminData) => {
    const data1 = {
      MainHeading: `Profile - ${subAdminData.full_name}`,
      MainSubheading1: "Sub-Admins Lobby",
      MainSubheading2: "Sub Admin Profile",
      PaperHeading: " Personal Details",
      Labels: {
        label1: "Full Name",
        label3: "Designation",
        label4: "Email ID ",
        label5: "Department",
      },
      DetailBox: {
        box1: subAdminData.full_name,
        box3: subAdminData.designation,
        box4: subAdminData.email,
        box5: subAdminData.department,
      },
      Permissions: subAdminData.permission.permission,
      IsActive: subAdminData.status,
      home: `/${role}/subadmins`,
      EditPath: `/${role}/subadmins/edit-subadmin`,
      ProfileType: "Subadmin",
      SubAdminID: subAdminData.id,
      permissions: getPermissionsForSubAdmin(),
    };
    setData(data1);
  };

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    fetchClient.get(`${apiUrl.viewSubAdmin}/${props.location.state.SubAdminID}/`).then((res) => {
      setSubAdminData(res.data.data);
      setLoader(true);
    });
  }, []);

  // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector("#main-content")?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
        setTopBarVisible(false);
      } else {
        setTopBarVisible(true);
      }
      scrollPos = document.querySelector("#main-content")?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector("#main-content")?.addEventListener("scroll", listenToScroll);
    return () => document.querySelector("#main-content")?.removeEventListener("scroll", listenToScroll);
  }, [loader]);

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />

      <main className={classes.content}>
        {loader ? (
          <Commonprofile data={data} />
        ) : (
          <Paper className={classes.paper} style={{ height: "450px", justifyContent: "center", alignItems: "center", marginTop: "200px" }}>
            <CircularProgress color="error" />
          </Paper>
        )}
      </main>
    </div>
  );
}
