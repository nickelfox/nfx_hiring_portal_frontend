import  {useHistory,useEffect,fetchClient,apiUrl,CssBaseline,Topbar,Sidebar,React} from 'allImport';
import { useStyles } from "style/Editstyle.js";
import Commonfieldscreen from 'components/CommonScreen/Commonfieldscreen';
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function EditSubadmin(props) {
  const classes = useStyles();
  const [data, setData] = React.useState({});
  const [isDataSet, setIsDataSet] = React.useState(false);
  const history=useHistory();
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.givePermissions = allPermissions.includes("GIVE_PERMISSIONS");
    }
    return permission;
  }

    const setSubAdminData = (subAdminData) => {
      const data1 = {
        MainHeading:`Edit Sub Admin - ${subAdminData.full_name}` ,
        MainSubheading1:"Sub Admins Lobby",
        MainSubheading2:"Sub Admin Profile",
        MainSubheading3:"Edit Subadmin",
        PaperHeading:"Personal Details",
        Type:"Subadmin",
        Labels: {
          label1: "Full Name",
          label3: "Email ID ",
          label4: "Designation",
        
        },
        DetailBox: {
          box1: subAdminData.full_name ,
          box3: subAdminData.email,
          box4:  subAdminData.designation ,
         
        },
        Placeholder:{
          placeholder1:"Enter full name",
          placeholder3:"Enter registered email id",
          placeholder4:"Enter designation",
         
        },
        Button: {
          button1: "Cancel",
          button2: "Save",
        },
       
        height:"312px",
        field : "Edit",
        fieldType: "editSubadmin",
        home:`/${role}/subadmins`,
        SubAdminUserPermission: getPermissionsForSubAdmin(),
        Permissions: subAdminData.permission.permission,
        SubAdminID: subAdminData.id,
      };
      setData(data1);
    }
  

    useEffect(() => {
      if(!(StorageManager.get(API_TOKEN)) && !(StorageManager.get(ADMIN_ID)) && !(StorageManager.get(ADMIN_NAME)) && !(StorageManager.get(LOGOUT_TOKEN))){
        history.push("/login");
    }
      fetchClient.get(`${apiUrl.viewSubAdmin}/${props.location.state.SubAdminID}/`).then((res) => {
        setSubAdminData(res.data.data);
        setIsDataSet(true);
      })
    }, [])
  
     // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
          setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])
  
  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
          {isDataSet ? <Commonfieldscreen data={data}/> : null}
      </main>
    </div>
  );
}