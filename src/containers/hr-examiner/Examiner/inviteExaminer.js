import { useHistory, CssBaseline, Topbar, Sidebar, React, useEffect } from "allImport";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
export default function Dashboard(props) {
  const classes = useStyles();
  const history = useHistory();
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
  }, []);

  const data = {
    MainHeading: "Invite Examiner - Enter Details",
    MainSubheading1: "Examiners Home",
    MainSubheading2: "Invite Examiner",
    PaperHeading: "Enter Deatails",

    Labels: {
      label1: "Full Name",
      label2: "Phone Number",
      label3: "Email ID ",
      label4: "Designation",
      label5: "Department",
    },
    DetailBox: {
      box1: "",
      box2: "",
      box3: "",
      box4: "",
      box5: "",
    },
    Placeholder: {
      placeholder1: "Enter full name",
      placeholder2: "Enter phone number",
      placeholder3: "Enter registered email id",
      placeholder4: "Enter designation",
      placeholder5: "Choose Department",
    },
    Button: {
      button1: "Cancel",
      button2: "Invite",
    },
    Type: "Examiner",
    field: "Add",
    fieldType: "AddExaminer",
    height: "312px",
    home: `/${role}/examiner`,
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen data={data} />
    </div>
  );
}
