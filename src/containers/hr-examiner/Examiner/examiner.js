import { CircularProgress,useHistory, Emptyscreen, MenuItem, Select, CancelRoundedIcon, CssBaseline, Box, Container, Grid, Paper, Button, IconButton, InputAdornment, TextField, FormControl, Topbar, Sidebar, Copyright, React, SearchIcon, Pagination, fetchClient, apiUrl, useEffect, Link, Commontable } from "allImport";
import { useStyles } from "style/Liststyle.js";
import { MenuProps, MouseEnter, MouseLeave,capitalize,convertUTCDateToLocalDate } from "Common/CommonFunction";
import { OutlinedInput } from "@material-ui/core";
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
function createData(ColA, ColB, ColC, ColD, ColE, ColF, ColG) {
  return { ColA, ColB, ColC, ColD, ColE, ColF, ColG };
}
var AS = [];
var examinerIDs = [];
var examinerEmails = [];
var examinerNo = [];
export default function Dashboard() {
  const history = useHistory();
  const classes = useStyles();
  const [deptLists, setDeptlist] = React.useState([]);
  const [deptSelectionVal, setDept] = React.useState("");
  const [data, setAllData] = React.useState({});
  const [count , setCount] = React.useState("");
  const [totalPages, setTotalPages] = React.useState(1);
  const [page, setPage] = React.useState(1);
  const [examinerSearch,setExaminerSearch]= React.useState("");
  const [loader, setLoader] = React.useState(false);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const handleChangePage = (event, value) => {
    setPage(value);
    let url = `${apiUrl.viewExaminer}?page=${value}`;
    url += (deptSelectionVal === "") ? "" : `&department=${deptSelectionVal}`;
    url += examinerSearch === "" ? "" : `&search=${examinerSearch}`;
    fetchClient.get(url).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const examinerArray = res && res.data && res.data.data && res.data.data.results;
      setTotalPages(res.data.data.pages);
      colmConversion(examinerArray);
    });
  };
  const searchExaminer = (e) => {
    setDept("")
    setExaminerSearch(e.target.value);
    fetchClient.get(`${apiUrl.viewExaminer}?search=` + e.target.value + "&page=" + page).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const examinerArray = res && res.data && res.data.data && res.data.data.results;
      setTotalPages(res.data.data.pages);
      colmConversion(examinerArray);
    });
  };
  const deptSelection = (e) => {
    setExaminerSearch("")
    setDept(e.target.value);
    const deptSelection = e.target.value;
    if(deptSelection)
    {
    fetchClient.get(`${apiUrl.viewExaminer}?department=` + deptSelection).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const examinerArray = res && res.data && res.data.data && res.data.data.results;
      setTotalPages(res.data.data.pages);
      colmConversion(examinerArray);
    });
  }
  else
  {
    fetchClient.get(`${apiUrl.viewExaminer}`).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const examinerArray = res && res.data && res.data.data && res.data.data.results;
      setTotalPages(res.data.data.pages);
      colmConversion(examinerArray);
    });
  }
  };
  const colmConversion = (arr) => {
    AS = [];
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].id, arr[index] && arr[index].full_name, arr[index] && arr[index].designation, arr[index] && arr[index].department, arr[index] && convertUTCDateToLocalDate(new Date( arr[index].updated_at)).toLocaleDateString("en-IN"), arr[index] && arr[index].email, arr[index] && arr[index].phone_number));
      examinerIDs.push(arr[index] && arr[index].id);
      examinerEmails.push(arr[index] && arr[index].email);
      examinerNo.push(arr[index] && arr[index].phone_number);
    }
    processDataList();
  };
  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.view = allPermissions.includes("VIEW_EXAMINER");
      permission.create = allPermissions.includes("CREATE_EXAMINER");
      permission.edit = allPermissions.includes("EDIT_EXAMINER");
      permission.delete = allPermissions.includes("DELETE_EXAMINER");
    }
    return permission;
  }
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    fetchClient.get(`${apiUrl.viewExaminer}?page=` + page).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPages(res.data.data.pages);
      const examinerArray = res && res.data && res.data.data && res.data.data.results;
      colmConversion(examinerArray);
      setLoader(true)
    }).catch(() => {
      setLoader(true);
    });
    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
  }, []);
  const clear = () => {
    setExaminerSearch("");
    fetchClient.get(`${apiUrl.examinerList}`).then((res) => {
      var examinersList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPages(res.data.data.pages);
      colmConversion(examinersList);
    });
  };
  const processDataList = () => {
    const permissions = getPermissionsForSubAdmin();
    const data = {
      columnnames: {
        col1: "ID",
        col2: "Name",
        col3: "Designation",
        col4: "Department",
        col5: "Last Updated",
      },
      DeptData: deptLists,
      ExaminerIDs: examinerIDs,
      ExaminerEmails: examinerEmails,
      ExaminerNo:examinerNo,
      permissions: permissions,
      List: AS,
      ListKey: Object.keys(AS),
      TableType: "Examiner",
      EditPath: `/${role}/examiner/edit-profile`,
      profile: `/${role}/examiner/profile`,
    };
    setAllData(data);
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading}>Examiners</div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
              <div className="button_invite_examiner">
                {(role === "sub-admin" && data.permissions && !data.permissions.create) ? null: <Link
                  to={{
                    pathname: `/${role}/examiner/invite`,
                    state: { DeptData: { deptLists } },
                  }}
                >
                  <Button className={classes.filledButton} variant="contained" color="primary">
                    Invite Examiner
                  </Button>
                </Link>}
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>Examiners Lobby</div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper}>
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} align="left" justify="left" style={{ display: "flex" }}>
                        <div className={classes.all_examiners_heading}>All Examiners</div>
                        <div className={classes.examiner_nos}>{count}</div>
                      </Grid>
                      <Grid item lg={4} xl={4} md={4} sm={4} align="center" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            onChange={(e) => searchExaminer(e)}
                            variant="outlined"
                            value={examinerSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: examinerSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={() => clear()}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                        </div>
                      </Grid>
                      <Grid item lg={5} xl={5} md={5} sm={5} align="right" justify="right" style={{ paddingRight: "24px" }}>
                        <div className={classes.examinerDept}>
                          <FormControl style={{ textAlign: "center", width: "162px", marginTop: "-5px" }}>
                            <Select displayEmpty id="demo-customized-select-native" value={deptSelectionVal} onChange={(e) => deptSelection(e)} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                key="0"
                                value=""
                              >
                                Department
                              </MenuItem>
                              {deptLists &&
                                deptLists.map((row, index) => (
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                    key={index + 1}
                                    value={row.id}
                                  >
                                    {capitalize(row.name)}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                        </div>
                      </Grid>
                    </Grid>
                    {loader ? <>{count ? (
                      <Grid container xs={12}>
                        <div className={classes.examiners_table_wrapper}>
                          <Commontable data={data} />
                        </div>
                      </Grid>
                    ) : (
                      <Emptyscreen image="/assets/Icons/${role}/examinerEmptyIcon.svg" />
                    )}</>:
                    <Paper className={classes.paper} style={{height:'380px',justifyContent:'center',alignItems:'center'}} ><CircularProgress  color="error"/></Paper>}
                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPages} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>
          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}