import { useEffect, CssBaseline, Topbar, Sidebar, React, useHistory } from "allImport";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function Dashboard(props) {
  const classes = useStyles();
  const history = useHistory();
  const role=localStorage.getItem("ROLE").toLowerCase()
  const examinerID = props && props.location && props.location.state && props.location.state.examinerID;
  const data = {
    Department: props.location.state.examinerDept,
    MainHeading: "Edit Examiner - Enter Details",
    MainSubheading1: "Examiners Home",
    MainSubheading2: "Edit Examiner",
    PaperHeading: "Select Exam",
    Labels: {
      label1: "Full Name",
      label2: "Phone Number",
      label3: "Email ID ",
      label4: "Designation",
      label5: "Department",
    },
    DetailBox: {
      box1: props.location.state.examinerName,
      box2: props.location.state.examinerNo,
      box3: props.location.state.examinerEmail,
      box4: props.location.state.examinerDesignation,
      box5: props.location.state.examinerDept,
    },
    Placeholder: {
      placeholder1: "Enter full name",
      placeholder2: "Enter phone number",
      placeholder3: "Enter registered email id",
      placeholder4: "Enter designation",
      placeholder5: "Choose Department",
    },
    Button: {
      button1: "Cancel",
      button2: "Update",
    },
    Type: "Examiner",
    height: "312px",
    examinerID: examinerID,
    field: "Edit",
    fieldType: "EditExaminer",
    home: `/${role}/examiner`,
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen data={data} />
    </div>
  );
}
