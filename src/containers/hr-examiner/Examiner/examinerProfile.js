import { Paper,CircularProgress,useHistory, CssBaseline, Topbar, Sidebar, React, fetchClient, apiUrl, useEffect } from "allImport";
import Commonprofile from "components/Commonprofile/Commonprofile";
import useStyles from "style/Profilestyle.js";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function Dashboard(props) {
  const classes = useStyles();
  const [examinerName, setName] = React.useState("");
  const [examinerEmail, setEmail] = React.useState("");
  const [contactNo, setContantNo] = React.useState("");
  const [loader, setLoader] = React.useState(false);
  const [examinerDesignation, setDesignation] = React.useState("");
  const [departmentName, setDepartment] = React.useState("");
  const examinerID = props && props.location && props.location.state && props.location.state.examinerID;
  const history = useHistory();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.edit = allPermissions.includes("EDIT_CANDIDATE");
      permission.delete = allPermissions.includes("DELETE_CANDIDATE");
    }
    return permission;
  }

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    fetchClient.get(`${apiUrl.examinerProfile}/` + examinerID).then((res) => {
      setLoader(true)
      setName(res && res.data && res.data.data && res.data.data.full_name);
      setContantNo(res && res.data && res.data.data && res.data.data.phone_number);
      setDesignation(res && res.data && res.data.data && res.data.data.designation);
      setEmail(res && res.data && res.data.data && res.data.data.email);
      setDepartment(res && res.data && res.data.data && res.data.data.department);
    });
  }, []);

  const data = {
    MainHeading: examinerName,
    MainSubheading1: "Examiners Lobby",
    MainSubheading2: "Examiner Profile",
    PaperHeading: " Personal Details",
    Labels: {
      label1: "Full Name",
      label2: "Contact Number",
      label3: "Designation",
      label4: "Email ID ",
      label5: "Department",
    },
    DetailBox: {
      box1: examinerName,
      box2: contactNo,
      box3: examinerDesignation,
      box4: examinerEmail,
      box5: departmentName,
    },
    examinerID: examinerID,
    EditPath: `/${role}/examiner/edit-profile`,
    permissions: getPermissionsForSubAdmin(),
    home: `/${role}/examiner`,
    ProfileType: "Examiner",
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content} >
      {loader ? <Commonprofile data={data}/> :
         (<Paper className={classes.paper} style={{ height: "450px", justifyContent: "center", alignItems: "center",marginTop:'200px' }}>
              <CircularProgress color="error" />
                   </Paper>
                    )}
    
    </main>
    </div>
  );
}
