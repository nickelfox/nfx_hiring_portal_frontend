import { useEffect, CssBaseline, Topbar, Sidebar, React, useHistory } from "allImport";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import {  ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
const role=localStorage.getItem('ROLE')?localStorage.getItem('ROLE').toLowerCase():null;
export default function CreateDepartment(props) {
  const classes = useStyles();
  const history = useHistory();
  const data = {
    MainHeading: "Add Department",
    MainSubheading1: "Departments Lobby",
    MainSubheading2: "Add Department",
    PaperHeading: "Enter Department Details",

    Labels: {
      label1: "Department Name",
    },
    DetailBox: {
      box1: "",
    },
    Placeholder: {
      placeholder1: "Enter department name",
    },
    Button: {
      button1: "Cancel",
      button2: "Save",
    },
    Type: "Department",
    field: "Add",
    fieldType: "AddDepartment",
    height: "312px",
    home: `/${role}/department`,
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen data={data} />
    </div>
  );
}
