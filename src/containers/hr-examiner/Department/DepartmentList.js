import {CircularProgress, useHistory, Emptyscreen, CancelRoundedIcon, Link, useEffect, fetchClient, apiUrl, Commontable, CssBaseline, Box, Container, Grid, Paper, Button, IconButton, InputAdornment, TextField, Topbar, Sidebar, Copyright, React, SearchIcon, Pagination } from "allImport";
import { useStyles } from "style/Liststyle.js";
import {convertUTCDateToLocalDate} from "Common/CommonFunction"
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
var url = new URL(`${apiUrl.viewDepartments}`);
var AS = [];
var searchValue = "";
var departmentIDs = [];
export default function DepartmentList() {
  const history = useHistory();
  const classes = useStyles();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const [data, setAllData] = React.useState({});
  const [totalPage, setTotalPage] = React.useState("");
  const [count, setCount] = React.useState("");
  const [departmentSearch, setDepartmentSearch] = React.useState("");
  const [page, setPage] = React.useState(1);
  const [loader, setLoader] = React.useState(false);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const handleChangePage = (event, value) => {
    fetchClient.get(`${apiUrl.viewDepartments}?page=` + value).then((res) => {
      var DepartmentsArray = res && res.data && res.data.data && res.data.data.results;
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(DepartmentsArray);
    });
    setPage(value);
  };

  function createData(ColA, ColB, ColC, ColD) {
    return { ColA, ColB, ColC, ColD };
  }

  const colmConversion = (arr) => {
    AS = [];
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].id, arr[index] && arr[index].name, arr[index] && convertUTCDateToLocalDate(new Date(arr[index].created_at)).toLocaleDateString("en-IN")));

      departmentIDs.push(arr[index] && arr[index].id);
    }
    processDataList();
  };

  const filterDepartments = (e) => {
    setDepartmentSearch(e.target.value);
    searchValue = e.target.value;
    const url =  `${apiUrl.viewDepartments}?search=${searchValue}`;
    fetchClient.get(url).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const DepartmentsArray = res && res.data && res.data.data && res.data.data.results;
      colmConversion(DepartmentsArray);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
    });
  };

  const clear = () => {
    setDepartmentSearch("");
    searchValue = "";
    fetchClient.get(url).then((res) => {
      setCount(res && res.data && res.data.data && res.data.data.count);
      const DepartmentsArray = res && res.data && res.data.data && res.data.data.results;
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(DepartmentsArray);
    });
  };

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if(role === "sub-admin"){
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.view = allPermissions.includes("VIEW_DEPARTMENT");
      permission.create = allPermissions.includes("CREATE_DEPARTMENT");
      permission.edit = allPermissions.includes("EDIT_DEPARTMENT");
      permission.delete = allPermissions.includes("DELETE_DEPARTMENT");
    }
    return permission;
  }

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    fetchClient.get(`${apiUrl.viewDepartments}`).then((res) => {
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      setCount(res && res.data && res.data.data && res.data.data.count);
      const DepartmentsArray = res && res.data && res.data.data && res.data.data.results;
      colmConversion(DepartmentsArray);
      setLoader(true);
    }).catch(() => {
      setLoader(true);
    });
  }, []);
  const processDataList = () => {
    const permissions = getPermissionsForSubAdmin();
    const data = {
      columnnames: {
        col1: "ID",
        col2: "Department",
        col3: "Date Added",
      },
      TableType: "Department",

      List: AS,
      ListKey: Object.keys(AS),
      EditPath: `/${role}/department/edit`,
      profile: `/${role}/department/view`,
      permissions: permissions,
      departmentIDs: departmentIDs,
    };

    setAllData(data);
  };

  // Hide and Show Top Bar
 useEffect(() => {
  let scrollPos = 0;
  const listenToScroll = () => {
    const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    if (winScroll > scrollPos) {
        setTopBarVisible(false);
    } else {
      setTopBarVisible(true);  
    }  
    scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
  };
  document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
  return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
}, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_heading}>Departments</div>
            </Grid>
            <Grid item xs={12} sm={6} align="right" justify="right">
              <div className="button_invite_examiner">
              {(role === "sub-admin" && data.permissions && !data.permissions.create) ?null: <Link
                  to={{
                    pathname: `/${role}/department/create`,
                  }}
                >
                  <Button className={classes.filledButton} variant="contained" color="primary">
                    Add Department
                  </Button>
                </Link>}
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>All Departments Lobby</div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item lg={12} xl={12} md={12} sm={12}>
              <Paper className={classes.paper}>
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} style={{ display: "flex" }} align="left" justify="left">
                        <div className={classes.all_examiners_heading}>All Departments</div>
                        <div className={classes.examiner_nos} style={{ marginLeft: "-10px" }}>
                          {count}
                        </div>
                      </Grid>

                      <Grid item lg={3} xl={3} md={3} sm={3} align="right" justify="center">
                        <div className={classes.searchExaminers}>
                          <TextField
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            variant="outlined"
                            onChange={(e) => filterDepartments(e)}
                            value={departmentSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: departmentSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={() => clear()}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                        </div>
                      </Grid>
                    </Grid>

                    {loader ? <>{count ? (
                      <Grid container xs={12}>
                        <div className={classes.examiners_table_wrapper}>
                          <Commontable data={data} />
                        </div>
                      </Grid>
                    ) : (
                      <Emptyscreen image="/assets/Icons/DepartmentEmptyIcon.svg" />
                    )}</>:
                    <Paper className={classes.paper} style={{height:'380px',justifyContent:'center',alignItems:'center'}} ><CircularProgress  color="error"/></Paper>}

                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
