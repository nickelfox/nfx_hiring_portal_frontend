import { useEffect, CssBaseline, Topbar, Sidebar, React, useHistory } from "allImport";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function EditDepartment(props) {
  const classes = useStyles();
  const role= localStorage.getItem("ROLE").toLowerCase()
  const history = useHistory();
  const dept = props.location.state.departmentName
  const data = {
    MainHeading: "Edit Department",
    MainSubheading1: "Departments Lobby",
    MainSubheading2: "Marketing",
    MainSubheading3: "Edit",
    PaperHeading: "Enter Department Details",

    Labels: {
      label1: "Department Name",
    },
    DetailBox: {
      box1: dept && dept?.charAt(0).toUpperCase() + dept.slice(1).toLowerCase(),
    },
    Placeholder: {
      placeholder1: "Enter department name",
    },
    Button: {
      button1: "Cancel",
      button2: "Update",
    },
    Type: "Department",
    field: "Edit",
    fieldType: "EditDepartment",
    height: "312px",
    home: `/${role}/department`,
    departmentID: props.location.state.departmentID,
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen data={data} />
    </div>
  );
}
