import { useHistory, CssBaseline, Box, Topbar, Sidebar, Copyright, React, makeStyles, Container, Grid, Paper, clsx, Link, useState, useEffect, fetchClient, apiUrl } from "allImport";
import { useStyles } from "style/Liststyle";
import NotificationList from "../Notification/NotificationList";
import Chart from "react-apexcharts";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";

const useDashboardStyles = makeStyles((theme) => ({
  outerContainer: {
    marginTop: "30px",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  leftContainer: {
    width: "70%",
    maxWidth: "1000px",
    marginRight: "30px",
  },
  rightContainer: {
    minWidth: "400px",
    minHeight: "768px",
    margin: 0,
  },
  topLeftContainer: {
    display: "flex",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  bottomLeftContainer: {
    width: "100%",
    maxWidth: "1000px",
    boxSizing: "border-box",
    marginBottom: "20px",
    overflow: "hidden",
  },
  topLeftBoxes: {
    width: 162,
    height: 162,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    padding: 5,
    margin: "0 3px 7px",
  },
  tLBText1: {
    fontFamily: "Mulish",
    fontSize: "32px",
    fontStyle: "normal",
    fontWeight: "700",
    lineHeight: "50px",
    letterSpacing: "0.25px",
    color: "#18273B",
    margin: 0,
  },
  tLBText2: {
    fontFamily: "Mulish",
    fontSize: "12px",
    fontStyle: "normal",
    fontWeight: "400",
    lineHeight: "18px",
    letterSpacing: "0.25px",
    color: "#6D6D6D",
    textAlign: "center",
    width: 95,
  },
  tLBText3: {
    fontFamily: "Mulish",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "400",
    lineHeight: "22px",
    letterSpacing: "0.25px",
    color: "#D9001D",
    width:'113%'
  },
  tLBInner: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
    height: "100px",
    width: "100%",
  },
  tLBCircular: {
    height: 110,
    width: 110,
    borderRadius: "50%",
    backgroundColor: "#383838",
    padding: 0,
    marginBottom: 20,
  },
  tLBContent: {
    height: 100,
    width: 100,
    padding: 5,
    borderRadius: "50%",
    backgroundColor: "#FFFFFF",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  percent: {
    position: "relative",
    width: "130px",
    height: "130px",
    borderRadius: "50%",
    background: "#ffffff",
    zIndex: "1000",
  },
  svg: {
    position: "relative",
    width: "130px",
    height: "130px",
    zIndex: "1000",
  },
  circle: {
    width: "100%",
    height: "100%",
    fill: "none",
    stroke: "#191919",
    strokeWidth: 6,
    strokeLinecap: "round",
    transform: "translate(5px, 5px)",
  },
  circle2: {
    strokeDasharray: "420",
    strokeDashoffset: "420",
  },
  progress: {
    stroke: "#D9001D",
  },
  tLBCircleInner: {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: 0,
    left: 0,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  graphText: {
    fontFamily: "Mulish",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: "22px",
    letterSpacing: "0.25px",
    padding: "16px 0 10px 20px",
  },
}));

export default function Dashboard() {
  const classes = useStyles();
  const history = useHistory();
  const role=localStorage.getItem('ROLE')?localStorage.getItem('ROLE').toLowerCase():null;
  const [options, setOptions] = useState({});
  const [series, setSeries] = useState([]);
  const [activeJobs, setActiveJobs] = useState("-");
  let jobsActive = 0;
  let totalJobs = 0;
  const [questionsAdded, setQuestionsAdded] = useState("-");
  const [examinersOnboarded, setExaminersOnboarded] = useState("-");
  const [examsSubmitted, setExamsSubmitted] = useState("-");
  const [activeExams, setActiveExams] = useState("-");
  const [candidatesAdded, setCandidatesAdded] = useState("-");
  const [topbarVisible, setTopBarVisible] = useState(true);
  let totalCandidates = 0;
  const [progress, setProgress] = useState(420);
  const dashStyles = useDashboardStyles();
  let graphData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  
  const setColumnChartOptions = () => {
    setSeries([
      {
        name: "Passed",
        data: graphData,
      },
    ]);
    let chartOptions = {
      chart: {
        type: "bar",
        height: "500px",
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "22px",
          endingShape: "rounded",
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 7,
        colors: ["transparent"],
      },
      xaxis: {
        categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      },
      colors: ["#383838", "#29CB1B"],
      yaxis: [
        {
          labels: {
            formatter: function(val) {
              return val.toFixed(0);
            }
          }
        }
      ],
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return val;
          },
        },
      },
    };
    setOptions(chartOptions);
  };

  const getProgressBarValue = () => {
    let percentage = 0;
    if (role === "examiner") {
      percentage = (candidatesAdded / totalCandidates) * 100;
    } else {
      percentage = (jobsActive / totalJobs) * 100;
    }
    return 420 - (420 * percentage) / 100;
  };

  const getStatistics = () => {
    return (
      <>
        {role === "examiner" ? (
          <div className={dashStyles.topLeftContainer}>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.percent}>
                <svg className={dashStyles.svg}>
                  <circle cx="60" cy="60" r="60" className={dashStyles.circle}></circle>
                  <circle cx="60" cy="60" r="60" className={clsx(dashStyles.progress, dashStyles.circle, dashStyles.circle2)} style={{ strokeDashoffset: `${progress}` }}></circle>
                </svg>
                <div className={dashStyles.tLBCircleInner}>
                  <h4 className={dashStyles.tLBText1}>{candidatesAdded}</h4>
                  <div className={dashStyles.tLBText2}>Candidates added</div>
                </div>
              </div>
              <Link to={{ pathname: `/${role}/candidate` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View all Candidates</div>
              </Link>
            </Paper>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.tLBInner}>
                <h4 className={dashStyles.tLBText1}>{activeExams}</h4>
                <div className={dashStyles.tLBText2}>Active Exams</div>
              </div>
              <Link to={{ pathname: `/${role}/exam` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View all Exams</div>
              </Link>
            </Paper>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.tLBInner}>
                <h4 className={dashStyles.tLBText1}>{questionsAdded}</h4>
                <div className={dashStyles.tLBText2}>New Questions Added</div>
              </div>
              <Link to={{ pathname: `/${role}/question-bank/questions` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View all Questions</div>
              </Link>
            </Paper>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.tLBInner}>
                <h4 className={dashStyles.tLBText1}>{examsSubmitted}</h4>
                <div className={dashStyles.tLBText2}>Exams Submitted</div>
              </div>
              <Link to={{ pathname: `/${role}/submitted-exam` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View Submitted Exams</div>
              </Link>
            </Paper>
          </div>
        ) : (
          <div className={dashStyles.topLeftContainer}>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.percent}>
                <svg className={dashStyles.svg}>
                  <circle cx="60" cy="60" r="60" className={dashStyles.circle}></circle>
                  <circle cx="60" cy="60" r="60" className={clsx(dashStyles.progress, dashStyles.circle, dashStyles.circle2)} style={{ strokeDashoffset: `${progress}` }}></circle>
                </svg>
                <div className={dashStyles.tLBCircleInner}>
                  <h4 className={dashStyles.tLBText1}>{activeJobs}</h4>
                  <div className={dashStyles.tLBText2}>Active Vacancies</div>
                </div>
              </div>
              <Link to={{ pathname: `/${role}/job` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View all Vacancies</div>
              </Link>
            </Paper>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.tLBInner}>
                <h4 className={dashStyles.tLBText1}>{questionsAdded}</h4>
                <div className={dashStyles.tLBText2}>New Questions Added</div>
              </div>
              <Link to={{ pathname: `/${role}/question-bank/questions` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View all Questions</div>
              </Link>
            </Paper>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.tLBInner}>
                <h4 className={dashStyles.tLBText1}>{examinersOnboarded}</h4>
                <div className={dashStyles.tLBText2}>New Examiners Onboarded</div>
              </div>
              <Link to={{ pathname: `/${role}/examiner` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View all Examiners</div>
              </Link>
            </Paper>
            <Paper className={clsx(classes.paper, dashStyles.topLeftBoxes)}>
              <div className={dashStyles.tLBInner}>
                <h4 className={dashStyles.tLBText1}>{examsSubmitted}</h4>
                <div className={dashStyles.tLBText2}>Exams Submitted</div>
              </div>
              <Link to={{ pathname: `/${role}/submitted-exam` }} style={{ textDecoration: "none" }}>
                <div className={dashStyles.tLBText3}>View Submited Exams</div>
              </Link>
            </Paper>
          </div>
        )}
      </>
    );
  };
  
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    
    if (role === "examiner") {
      fetchClient.get(`${apiUrl.getExaminerStatisticValues}`).then((res) => {
        setActiveExams(res.data.data["Active_Exams"]);
        setCandidatesAdded(res.data.data["Candidates_added"]);
        totalCandidates = res.data.data["all_candidates"];
        setExamsSubmitted(res.data.data["Exams_Submitted"]);
        setQuestionsAdded(res.data.data["New_Questions_Added"]);
        setProgress(getProgressBarValue());
        console.log(res.data.data);
      });
    } else {
     
      fetchClient.get(`${apiUrl.getHRStatisticValues}`).then((res) => {
        setActiveJobs(res.data.data["active_jobs"]);
        jobsActive = res.data.data["active_jobs"];
        totalJobs = res.data.data["all_jobs"];
        setQuestionsAdded(res.data.data["new_questions_added"]);
        setExamsSubmitted(res.data.data["exams_submitted"]);
        setExaminersOnboarded(res.data.data["new_examiners_onboarded"]);
        setProgress(getProgressBarValue());
      });
    }

    fetchClient.get(`${apiUrl.getDashboardSummary}`).then((res) => {
      console.log("candid summary", res.data.data["candidate summary"]);
      let temp = res.data.data["candidate summary"];
      let newData = [...graphData];
      for (let i of temp) {
        newData[i.month - 1] = i.total;
      }
      graphData = newData;
      setColumnChartOptions();
    });
  }, []);

  // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
           setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Grid container>
            <Grid item  lg={3} xl={3} md={3} sm={3}>
              <div className={classes.page_heading}>Dashboard</div>
              <div className={classes.page_subheading}>All Updates</div>
            </Grid>
          </Grid>
          <div className={dashStyles.outerContainer}>
            <div className={dashStyles.leftContainer}>
              {getStatistics()}
              <Paper className={clsx(classes.paper, dashStyles.bottomLeftContainer)}>
                <div className={dashStyles.graphText}>{new Date().getFullYear()} Candidate Summary</div>
                <Chart options={options} series={series} type="bar" height="500px" />
              </Paper>
            </div>
            <Paper className={clsx(classes.paper, dashStyles.rightContainer)}>
              <NotificationList external={true} />
            </Paper>
          </div>
        </Container>
        <Box pt={2}>
          <Copyright />
        </Box>
      </main>
    </div>
  );
}
