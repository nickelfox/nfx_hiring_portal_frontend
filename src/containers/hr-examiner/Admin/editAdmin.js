import { useEffect, SuccessDialog, apiUrl, fetchUpdate, Paper, CssBaseline, Box, Container, Grid, Button, Topbar, Sidebar, Copyright, React, useHistory } from "allImport";
import useStyles from "style/Profilestyle.js";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function Edithr(props) {
  const [box1, setBox1] = React.useState("");
  const [box2, setBox2] = React.useState("");
  const history = useHistory();
  const [successPrompt, setSuccessPromptToggle] = React.useState(false);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const adminID = localStorage.getItem("ADMIN_ID");
  const classes = useStyles();
  const editHrDetails = (e, type) => {
    const value = e.target.value;
    if (type === "fullname") {
      setBox1(value);
    } else if (type === "designation") {
      setBox2(value);
    }
  };
  const edit = () => {
    const data1 = {
      full_name: box1,
      designation: box2,
    };
    role==='admin'?
    fetchUpdate
      .patch(`${apiUrl.editHr}${adminID}`, data1)
      .then((res) => {
        StorageManager.put(ADMIN_NAME, res.data.data.full_name);
        setSuccessPromptToggle(true);
      })
      .catch((error) => {
        setSuccessPromptToggle(false);
      }):
      role==='examiner'?
      fetchUpdate
      .patch(`${apiUrl.updateExaminer}/${adminID}`, data1)
      .then((res) => {
        StorageManager.put(ADMIN_NAME, res.data.data.full_name);
        setSuccessPromptToggle(true);
      })
      .catch((error) => {
        setSuccessPromptToggle(false);
      }):
      fetchUpdate
      .patch(`${apiUrl.editSubadmin}${adminID}/`, data1)
      .then((res) => {
        StorageManager.put(ADMIN_NAME, res.data.data.full_name);
        setSuccessPromptToggle(true);
      })
      .catch((error) => {
        setSuccessPromptToggle(false);
      })
      
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    setBox1(props && props.location && props.location.state && props.location.state.name);
    setBox2(props && props.location && props.location.state && props.location.state.designation);
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          {successPrompt ? <SuccessDialog heading="Profile Updated Successfully"  /> : null}
          <Grid container xs={12}>
            <Grid item xl={6} lg={6} md={6} sm={12} align="left" justify="left">
              <div className={classes.page_heading}>Edit My Profile</div>
            </Grid>
            <Grid item xl={6} lg={6} md={6} sm={12} className={classes.actionbutton}>
              <div className="button_invite_examiner" style={{ paddingRight: "80px" }}>
                <Button className={classes.outlinedButton} variant="contained" color="primary">
                  Cancel
                </Button>
              </div>
              <div className="button_invite_examiner">
                <Button onClick={edit} className={classes.filledButton} variant="contained" color="primary">
                  Save
                </Button>
              </div>
            </Grid>
          </Grid>
          <Paper className={classes.paper} style={{ height: "180px" }}>
            <Grid container xs={12}>
              <Grid item xs={12} sm={6} align="left" justify="left">
                <div className={classes.examiners_select_exam_heading}> Personal Details</div>
              </Grid>
            </Grid>
            <Grid container xs={12}>
              <div style={{ display: "flex", marginLeft: "32px" }}>
                <input onChange={(e) => editHrDetails(e, "fullname")} type="text" placeholder="Full Name" className={classes.textfield} defaultValue={props.location.state.name} style={{ marginRight: "32px" }} />
                <input onChange={(e) => editHrDetails(e, "designation")} type="text" placeholder="Designation" className={classes.textfield} defaultValue={props.location.state.designation} />
              </div>
            </Grid>
          </Paper>

          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
