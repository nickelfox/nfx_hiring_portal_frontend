import { useEffect, fetchClient, apiUrl, CssBaseline, Topbar, Sidebar, React } from "allImport";
import Commonprofile from "components/Commonprofile/Commonprofile";
import useStyles from "style/Profilestyle.js";
import { useHistory, useLocation } from "react-router-dom";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
export default function HrProfile(props) {
  const classes = useStyles();
  const location = useLocation();
  const [Hrdetails, setHrdetails] = React.useState({});
  const history = useHistory();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const data = {
    MainHeading: Hrdetails.full_name,
    PaperHeading: "Personal Details",
    Labels: {
      label1: "Full Name",
      label2: "Designation",
    },
    DetailBox: {
      box1: Hrdetails.full_name,
      box2: Hrdetails.designation,
    },

    ProfileType: "Admin",
  };
  const adminID = location.state;
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    console.log(adminID);
   role==='admin'?
    fetchClient.get(`${apiUrl.adminProfile}${adminID}`).then((res) => {
      setHrdetails(res && res.data && res.data.data);
    })
    :
    role==='examiner'?
    fetchClient.get(`${apiUrl.examinerProfile}/${adminID}`).then((res) => {
      setHrdetails(res && res.data && res.data.data);
    }):
    fetchClient.get(`${apiUrl.subadminProfile}${adminID}`).then((res) => {
      setHrdetails(res && res.data && res.data.data);
    });

  
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonprofile data={data} />
    </div>
  );
}
