import React from "react";
import { Container, CssBaseline, Topbar, Sidebar, makeStyles, Emptyscreen } from "allImport";
import { commonStyle } from "commonStyle.js";

const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),
}));
export default function AdminEmptyScreen() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          <Emptyscreen />
        </Container>
      </main>
    </div>
  );
}
