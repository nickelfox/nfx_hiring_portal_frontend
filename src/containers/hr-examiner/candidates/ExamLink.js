import { withSnackbar, CircularProgress, useHistory, Select, fetchPost, CssBaseline, Box, Container, Grid, Paper, Button, FormControl, Topbar, Sidebar, Copyright, React, clsx, fetchClient, apiUrl, useEffect, Link, SuccessDialog } from "allImport";
import { useStyles } from "style/Editstyle.js";
import { MenuProps, MouseEnter, MouseLeave } from "Common/CommonFunction";
import "style/style.css";
import { MenuItem, OutlinedInput } from "@material-ui/core";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
var url = new URL(`${apiUrl.viewExams}`);
const ExamLink = (props) => {
  let candidateExamUrl = process.env.REACT_APP_CANDIDATE_URL ? process.env.REACT_APP_CANDIDATE_URL : "https://candidate.dev.nhp.foxlabs.in";
  const history = useHistory();
  const classes = useStyles();
  const [loader, setLoader] = React.useState(true);
  const [examlist, setExamslist] = React.useState([]);
  const role = localStorage.getItem("ROLE").toLowerCase();
  const [exam, setExam] = React.useState("");
  const [examID, setExamID] = React.useState("");
  const [select, setSelect] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const [Status, setExamStatus] = React.useState([]);
  const [statusID, setStatusID] = React.useState("");
  const [Level, setExamLevel] = React.useState("");
  const candidateID = props && props.location && props.location.state && props.location.state.candidateID;
  const candidateName = props && props.location && props.location.state && props.location.state.candidateName;
  const department = props && props.location && props.location.state && props.location.state.DeptID;
  const handleChange = (e) => {
    setExam(e.target.value);
    setExamID(e.currentTarget.getAttribute("id"));
    Status && Status.map((row, index) => (row.status === "LINK_SENT" ? setStatusID(row.id) : null));
  };
  const handleLevelChange = (e) => {
    setExamLevel(e.target.value);
    setShow(true);
    fetchClient.get(url + `?page_size=1000&status=publish&experience_level=${e.target.value}&department=${department}`).then((res) => {
      setExamslist(res && res.data && res.data.data && res.data.data.results);
    });
  };
  let candidate_exam_id;
  const SendExamLink = () => {
    setLoader(false);
    const data1 = {
      candidate: candidateID,
      exam: examID,
      exam_link: candidateExamUrl + `/candidate/examlink/` + examID + "/" + candidateID,
    };
    fetchPost
      .post(`${apiUrl.examLink}`, data1)
      .then((res) => {
        candidate_exam_id = res && res.data && res.data.data && res.data.data.id;
      })
      .then(() => {
        const data = {
          candidate: candidateID,
          candidate_exam: candidate_exam_id,
          exam_status: statusID,
        };
        fetchPost.post(`${apiUrl.ExamStatus}`, data).then(() => {
          setLoader(true);
          setSelect(true);
        });
      })
      .catch((error) => {
        setLoader(true);
        if (error.response) {
          if (error.response.data.code === 404) {
            props.enqueueSnackbar("Please fill all mandatory fields", {
              variant: "error",
              autoHideDuration: 3000,
            });
          } else if (error.response.data.error) {
            error.response.data.error.message.forEach((message) => {
              props.enqueueSnackbar(message, {
                variant: "error",
                autoHideDuration: 3000,
              });
            });
          }
        } else {
          props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
            variant: "error",
          });
        }
      });
  };

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }

    fetchClient.get(`${apiUrl.AllExamStatus}`).then((res) => {
      setExamStatus(res && res.data && res.data.data && res.data.data.results);
    });
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        {select ? <SuccessDialog type="profile" heading="Exam Link Sent" /> : null}
        <Container maxWidth="xl" className={classes.container}>
          <Grid container xs={12} style={{ paddingBottom: "10px" }}>
            <Grid item md={12} lg={7} align="left" justify="left">
              <div className={classes.page_heading}>Send Exam Link - {candidateName}</div>
            </Grid>
            <Grid item md={12} lg={5} align="right" justify="right" className={classes.actionbutton}>
              <div className="button_candidate">
                <Button
                  onClick={() => {
                    SendExamLink();
                  }}
                  className={classes.CandidateButton}
                  variant="contained"
                  color="primary"
                >
                  Send Exam Link
                </Button>
              </div>
            </Grid>
          </Grid>
          <Grid container xs={12}>
            <Grid item xs={12} sm={6} align="left" justify="left">
              <div className={classes.page_subheading}>
                <span className={classes.goBackURL}>
                  <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname: `/${role}/candidate` }}>
                    Candidates Lobby
                  </Link>
                </span>{" "}
                <span className="spc"></span> &#62; <span className="spc"></span> Select Exam
              </div>
            </Grid>
          </Grid>
          {!loader ? <Paper className={classes.paper} style={{height:"240px",justifyContent:'center',alignItems:'center', overflow: "hidden"}} >
            <CircularProgress color="error"/></Paper> :
          <Paper className={classes.paper} style={{ height: "240px", overflow: "hidden" }}>
            <Grid container xs={12}>
              <Grid item xs={12} sm={6} align="left" justify="left">
                <div className={classes.examiners_select_exam_heading}>Select Exam</div>
                </Grid>
                </Grid>
                <Grid container xs={12} style={{marginLeft:'32px',marginBottom:'50px'}} >
                <Grid item xl={3} lg={3} xs={12} sm={3} md={3} align="left" justify="left">
                  <div className="examiner_detail_wrapper">
                    <div className={classes.examiner_detail_label}>
                      <label>
                        Exam Level
                        <span className={classes.mandatory}>*</span>
                      </label>
                    </div>

                  <FormControl >
                    <Select className={classes.select_style} displayEmpty id="demo-customized-select-native" value={Level} onChange={(e) => handleLevelChange(e)} MenuProps={MenuProps} input={<OutlinedInput style={{width:'188px'}} />}>
                      <MenuItem
                        onMouseEnter={(e) => {
                          MouseEnter(e);
                        }}
                        onMouseLeave={(e) => {
                          MouseLeave(e);
                        }}
                        value=""
                      >
                       Choose Exam Level
                      </MenuItem>
                      <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="senior"
                              >
                                Senior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="junior"
                              >
                                Junior
                              </MenuItem>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="internship"
                              >
                                Internship
                              </MenuItem>
                           
                    </Select>
                  </FormControl>
                </div>
                </Grid>
               {show ? <Grid item xl={4} lg={4} xs={12} sm={4} align="left" justify="left">
                <div className="examiner_detail_wrapper">
                    <div className={classes.examiner_detail_label}>
                      <label>
                       Exam Name
                        <span className={classes.mandatory}>*</span>
                      </label>
                    </div>
              
                  <FormControl >
                    <Select className={classes.select_style} displayEmpty id="demo-customized-select-native" value={exam} onChange={(e) => handleChange(e)} MenuProps={MenuProps} input={<OutlinedInput style={{width:'220px'}} />}>
                      <MenuItem
                        onMouseEnter={(e) => {
                          MouseEnter(e);
                        }}
                        onMouseLeave={(e) => {
                          MouseLeave(e);
                        }}
                        value=""
                      >
                         Choose Exam Name
                      </MenuItem>
                      
                          {examlist &&
                            examlist.map((row, index) => (
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                key={row.id}
                                id={row.id}
                                value={row.exam_name}
                              >
                                {row.exam_name}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </div>
                  </Grid>
                 : null}
              </Grid>
            </Paper>
          }
          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
};
export default withSnackbar(ExamLink);
