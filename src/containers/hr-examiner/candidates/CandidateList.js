import  {CircularProgress,Emptyscreen,MenuItem,OutlinedInput,Select,CancelRoundedIcon,Link,useEffect, fetchClient, apiUrl,Commontable,CssBaseline,Box,Container,Grid,Paper,Button, IconButton,InputAdornment,TextField,FormControl,Topbar,Sidebar,Copyright,React,SearchIcon,Pagination, useHistory} from 'allImport';
import {useStyles} from "style/Liststyle.js"
import "style/style.css"
import { useBool } from "context/BoolContext";
import {MenuProps,MouseEnter,MouseLeave,capitalize} from "Common/CommonFunction"
import {ADMIN_ID, API_TOKEN, LOGOUT_TOKEN,ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";

var url= new URL(`${apiUrl.viewCandidates}`);
var AS = [];
var searchValue = "";

var candidateIDs = [];
var candidateName = [];
var candidateExperience = [];
var candidateNo = [];
var candidateEmail = [];
var candidatesArray = [];
const candidateStatus = [];

export default function CandidateList() {
  const { setHireRejectVisible } = useBool();
  const classes = useStyles();
  const [loader, setLoader] = React.useState(false);
  const history = useHistory();
  const [data, setAllData] = React.useState([]);
  const [deptLists, setDeptlist] = React.useState([]);
 const [val,setVal]=React.useState("");
  const [totalPage, setTotalPage] = React.useState("");
  const [examinerDepartment, setExaminerDepartment] = React.useState("");
  const [attemptLevel, setAttemptLevel] = React.useState("");
  const [examLevel, setExamLevel] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [count, setCount] = React.useState("");
  const [candidateSearch, setCandidateSearch] = React.useState("");
  const [page, setPage] = React.useState(1);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const examiner_name = localStorage.getItem("ADMIN_ID");
  const handleChangePage = (event, value) => {
    let url = `${apiUrl.viewCandidates}?page=` + value;
    url += status === "" ? "" : `&candidate_status=${status}`;
    url += examinerDepartment === "" ? "" : `&department=${examinerDepartment}`;
    url += examLevel === "" ? "" : `&exam_level=${examLevel}`;
    url += attemptLevel === "" ? "" : `&attempt_level=${attemptLevel}`;
    url += candidateSearch === "" ? "" : `&search=${candidateSearch}`;
    url += val === "" ? "" : `&is_hired=${val}`;
    if(role==="examiner")
    {
    url += examiner_name === "" ? "" : `&examiner=${examiner_name}`;
    }
    
      fetchClient.get(url).then((res) => {
        var candidatesList = res && res.data && res.data.data && res.data.data.results;
        setTotalPage(res && res.data && res.data.data && res.data.data.pages);
        setCount(res && res.data && res.data.data && res.data.data.count);
        colmConversion(candidatesList);
      });
  
    setPage(value);
  };

  function createData(ColA, ColB, ColC, ColD, ColE, ColF, ColG, ColH, ColI, ColJ, ColK, ColL,ColM) {
    return { ColA, ColB, ColC, ColD, ColE, ColF, ColG, ColH, ColI, ColJ, ColK, ColL,ColM };
  }

  const colmConversion = (arr) => {
    AS = [];
    console.log(arr.length);
    for (var index = 0; index < arr.length; index++) {
      AS.push(createData(arr[index] && arr[index].id, arr[index] && arr[index].user.full_name, arr[index] && arr[index].job, arr[index] && arr[index].examiner.examiner_name, arr[index].examiner.examiner_department, arr[index].attempt_level, capitalize(arr[index].exam.exam_level), arr[index] && arr[index].latest_status && arr[index].latest_status.candidate_status, arr[index] && arr[index].user.email, arr[index] && arr[index].user.phone_number, arr[index] && arr[index].experience, arr[index] && arr[index].examiner.examiner_department_id,arr[index] && arr[index].is_hired));
      candidateIDs.push(arr[index] && arr[index].id);
      candidateExperience.push(arr[index] && arr[index].experience);
      candidateNo.push(arr[index] && arr[index].user.phone_number);
      candidateEmail.push(arr[index] && arr[index].user.email);
      candidateStatus.push(arr[index] && arr[index].latest_status && arr[index].latest_status.candidate_status);
    }
    processDataList();
  };

  const filterCandidates = (e) => {
    setStatus("")
    setExaminerDepartment("")
    setAttemptLevel("")
    setExamLevel("")
    setCandidateSearch(e.target.value);
    url.searchParams.delete("department");
    url.searchParams.delete("attempt_level");
    url.searchParams.delete("candidate_status");
    url.searchParams.delete("exam_level");
    searchValue = e.target.value;
    url.searchParams.set("is_hired", val);
    url.searchParams.set("search",searchValue);
    if (role === "examiner")
    {
      url.searchParams.set("examiner",examiner_name);
    }
      fetchClient.get(url).then((res) => {
        var candidatesList = res && res.data && res.data.data && res.data.data.results;
        setCount(res && res.data && res.data.data && res.data.data.count);
        setTotalPage(res && res.data && res.data.data && res.data.data.pages);
        colmConversion(candidatesList);
      });
    
  };

  const candidateFilterSelection = (e, type) => {
    url.searchParams.delete("search");
    setCandidateSearch("");
    url.searchParams.set("is_hired", val);
    if (type === "examStatus") {
      setStatus(e.target.value);

      if (e.target.value !== "") {
        url.searchParams.set("candidate_status", e.target.value);
      } else {
        url.searchParams.delete("candidate_status");
      }
    } else if (type === "examinerDepartment") {
      setExaminerDepartment(e.target.value);

      if (e.target.value !== "") {
        url.searchParams.set("department", e.target.value);
      } else {
        url.searchParams.delete("department");
      }
    } else if (type === "attemptLevel"){
      setAttemptLevel(e.target.value);

      if (e.target.value !== "") {
        url.searchParams.set("attempt_level", e.target.value);
      } else {
        url.searchParams.delete("attempt_level");
      }
    } else if(type === "examLevel"){
      setExamLevel(e.target.value);

      if (e.target.value !== "") {
        url.searchParams.set("exam_level", e.target.value);
      } else {
        url.searchParams.delete("exam_level");
      }
    }
    if (role === "examiner") {
      url.searchParams.set("examiner", examiner_name);
    }
        fetchClient.get(url).then((res) => {
          var candidatesList = res && res.data && res.data.data && res.data.data.results;
          setCount(res && res.data && res.data.data && res.data.data.count);
          setTotalPage(res && res.data && res.data.data && res.data.data.pages);
          colmConversion(candidatesList);
        });
      
    
  };

  const clear = () => {
    url.searchParams.delete("search");
    url.searchParams.set("is_hired", val);
    setCandidateSearch("");
    
    if(role==="examiner")
    {
      url.searchParams.set("examiner",examiner_name);
    }
    fetchClient.get(url).then((res) => {
      var candidatesList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
      colmConversion(candidatesList);
    });
   
  
}

const getPermissionsForSubAdmin = () => {
  let permission = {};
  if(role === "sub-admin"){
    let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
    permission.view = allPermissions.includes("VIEW_CANDIDATE");
    permission.create = allPermissions.includes("CREATE_CANDIDATE");
    permission.edit = allPermissions.includes("EDIT_CANDIDATE");
    permission.delete = allPermissions.includes("DELETE_CANDIDATE");
    permission.generateLink = allPermissions.includes("GENERATE_EXAM_LINK");
  }
  return permission;
}

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    let url= new URL(`${apiUrl.viewCandidates}`);
    if(role==="examiner")
    {
      url.searchParams.set("examiner",examiner_name);
    }
    
      fetchClient.get(url).then((res) => {
        console.log(res);
        setLoader(true);
        setTotalPage(res && res.data && res.data.data && res.data.data.pages);
        setCount(res && res.data && res.data.data && res.data.data.count);
        console.log(res && res.data && res.data.data && res.data.data.next);
        candidatesArray = res && res.data && res.data.data && res.data.data.results;
        colmConversion(candidatesArray);
        console.log(candidatesArray);
      });
    
    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
    });
  }, []);
  const attempt_level=['L1','L2','L3','L4']
  const  handleChange =(e)=>{
    setStatus("")
    setExaminerDepartment("")
    setAttemptLevel("")
    setExamLevel("")
    setCandidateSearch("");
    url.searchParams.delete("search");
    url.searchParams.delete("department");
      url.searchParams.delete("attempt_level");
      url.searchParams.delete("candidate_status");
      url.searchParams.delete("exam_level");
    setVal(e.target.value)
    console.log(e.target.value)
    if(role==="examiner")
      {
      url.searchParams.set("examiner", examiner_name);
      }
    if(e.target.value!=="")
    {
      setHireRejectVisible(true)
      url.searchParams.set("is_hired", e.target.value);
    }
    else
    {
      setHireRejectVisible(false)
      url.searchParams.delete("is_hired");
    }
    fetchClient.get(url).then((res) => {
      var candidatesList = res && res.data && res.data.data && res.data.data.results;
      setCount(res && res.data && res.data.data && res.data.data.count);
      colmConversion(candidatesList);
      setTotalPage(res && res.data && res.data.data && res.data.data.pages);
    });
   
  
  
  }
  const processDataList = () => {
  const permissions = getPermissionsForSubAdmin();
  const data ={
    columnnames:{
      col1: "ID",
      col2: "Name",
      col4: "Examiner",
      col5: "Job Assosiated",
      col6: "Level",
      col7: "Exam Level",
      col8: "Exam Status",
    }
    ,
    TableType:"Candidate",
    candidateIDs: candidateIDs,
    candidateName: candidateName,
    candidateNo: candidateNo ,
    candidateEmail: candidateEmail,
    candidateExperience:candidateExperience,
    candidateStatus:candidateStatus,
    permissions: permissions,
    deptLists: deptLists,
   
    List:AS,
    ListKey:Object.keys(AS),
    EditPath:`/${role}/candidate/edit`,
    profile:`/${role}/candidate/profile`,
    AssignExam:`/${role}/candidate/examlink`,
   
    };

    setAllData(data);
  };
 const ExamStatus=['LINK_SENT','EXAM_ATTEMPTED','EXAM_SUBMITTED','PASSED','FAILED','DISQUALIFY']

 // Hide and Show Top Bar
 useEffect(() => {
  let scrollPos = 0;
  const listenToScroll = () => {
    const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    if (winScroll > scrollPos) {
         setTopBarVisible(false);
    } else {
      setTopBarVisible(true);  
    }  
    scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
  };
  document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
  return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
}, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} id="main-content">
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
       
        <Grid container >
        <Grid item xs={12} sm={6} align="left">
           <div  className = "candidate_dropdown">
           <Select className="fieldset" displayEmpty id="demo-customized-select-native" value={val} onChange={(e) => handleChange(e)} MenuProps={MenuProps} input={<OutlinedInput style={{fontSize:'24px'}} />}>
                              <MenuItem
                             
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                key="0"
                                value=""
                              >
                                
                                All Candidates
                              </MenuItem>
                             
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                   
                                    value="True"
                                  >
                                   Hired Candidates
                                  </MenuItem>
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                   
                                    value="False"
                                  >
                                  Rejected Candidates
                                  </MenuItem>
                                
                            </Select>
           </div>
         </Grid>
         <Grid item xs={12} sm={6}  align="right" justify="right">
            <div className = "button_invite_examiner">
           {role==="examiner" || (role === "sub-admin" && data.permissions && !data.permissions.create) ?null: <Link
                  to={{
                    pathname: `/${role}/candidate/add`,
                   
                  }}
                >
           {val ? null: <Button  className = {classes.filledButton} variant="contained" color="primary">
             Add Candidate
            </Button>}
            </Link>}
            </div>
         </Grid>
        </Grid>
        <Grid container xs={12}>
        <Grid item xs={12} sm={6} align="left" justify="left">
           <div className = {classes.page_subheading}>
             Candidates Lobby
           </div>
         </Grid>
        </Grid>
        <Grid container >
        <Grid item lg={12} xl ={12} md={12} sm={12}>
        <Paper className={classes.paper} >
                <Grid container xs={12}>
                  <Grid item lg={12} xl={12}>
                    <Grid container className={classes.allExam_container} lg={12} xl={12}>
                      <Grid item lg={3} xl={3} md={3} sm={3} style={{ display: "flex" }} align="left" justify="left">
                        <div className={classes.all_examiners_heading}> Candidates</div>
                        <div className={classes.examiner_nos} style={{ marginLeft: "-10px" }}>
                          {count}
                        </div>
                      </Grid>

                      <Grid item lg={3} xl={3} md={3} sm={3} align="center" justify="center">
                      
                          <TextField
                            size="medium"
                            placeholder="Search by Keywords"
                            id="outlined-basic"
                            variant="outlined"
                            onChange={(e) => filterCandidates(e)}
                            value={candidateSearch}
                            className={classes.searchExaminerfield}
                            InputProps={{
                             
                              startAdornment: (
                                <InputAdornment>
                                  <IconButton>
                                    <SearchIcon />
                                  </IconButton>
                                </InputAdornment>
                              ),
                              endAdornment: candidateSearch && (
                                <IconButton aria-label="toggle password visibility" onClick={() => clear()}>
                                  <CancelRoundedIcon />
                                </IconButton>
                              ),
                            }}
                          />
                       
                      </Grid>
                      <Grid item lg={6} xl={6} md={6} sm={6} align="right" justify="right">
                        <div className={classes.multiDropdown}>
                         {val ==="" ? <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "130px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={status} onChange={(e) => candidateFilterSelection(e, "examStatus")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Exam Status
                              </MenuItem>

                              {ExamStatus &&
                                ExamStatus.map((row, index) => (
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                    key={row}
                                    value={row}
                                  >
                                    {row}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>:null}

                         { role==="examiner"? null: <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "124px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={examinerDepartment} onChange={(e) => candidateFilterSelection(e, "examinerDepartment")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Department
                              </MenuItem>

                              {deptLists &&
                                deptLists.map((row, index) => (
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                    key={row.id}
                                    value={row.id}
                                  >
                                    {capitalize(row.name)}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>}

                          <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "122px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={attemptLevel} onChange={(e) => candidateFilterSelection(e, "attemptLevel")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Level
                              </MenuItem>
                              {attempt_level &&
                                attempt_level.map((row, index) => (
                                  <MenuItem
                                    onMouseEnter={(e) => {
                                      MouseEnter(e);
                                    }}
                                    onMouseLeave={(e) => {
                                      MouseLeave(e);
                                    }}
                                    key={row}
                                    value={row}
                                  >
                                    {row}
                                  </MenuItem>
                                ))}
                            </Select>
                          
                          </FormControl>

                          <FormControl style={{ textAlign: "center", marginTop: "-16px", width: "122px" }}>
                            <Select id="demo-customized-select-native" displayEmpty value={examLevel} onChange={(e) => candidateFilterSelection(e, "examLevel")} MenuProps={MenuProps} input={<OutlinedInput style={{height:'48px'}} />}>
                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value=""
                              >
                                Exam Level
                              </MenuItem>

                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value='senior'
                              >
                                Senior
                              </MenuItem>

                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value='junior'
                              >
                                Junior
                              </MenuItem>

                              <MenuItem
                                onMouseEnter={(e) => {
                                  MouseEnter(e);
                                }}
                                onMouseLeave={(e) => {
                                  MouseLeave(e);
                                }}
                                value="internship"
                              >
                                Internship
                              </MenuItem>

                            </Select>
                          </FormControl>

                        </div>
                      </Grid>
                    </Grid>

                    {loader ? (
                      <>
                        {count ? (
                          <Grid container xs={12}>
                            <div className={classes.examiners_table_wrapper}>
                              <Commontable data={data} />
                            </div>
                          </Grid>
                        ) : (
                          <Emptyscreen image="/assets/Icons/CandidateEmptyIcon.svg" />
                        )}
                      </>
                    ) : (
                      <Paper className={classes.paper} style={{ height: "380px", justifyContent: "center", alignItems: "center" }}>
                       
                        <img src="/assets/Icons/nickelfox_logo.svg" width='31.33px' height="44.56px" alt="nickelfoxlogo" className={classes.sidebar_nickelfoxlogo} />
                      </Paper>
                    )}
                  </Grid>
                </Grid>
              </Paper>
              <div className={classes.pagination}>
                <Pagination count={totalPage} page={page} onChange={handleChangePage} />
              </div>
            </Grid>
          </Grid>
          <Box pt={2}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
