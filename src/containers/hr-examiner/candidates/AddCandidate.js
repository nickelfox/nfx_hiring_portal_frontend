import { useEffect, useHistory, CssBaseline, Topbar, Sidebar, React } from "allImport";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import {  ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";

export default function AddCandidate(props) {
  const classes = useStyles();
  const history = useHistory();
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const data = {
    MainHeading: "Add New Candidate",
    MainSubheading1: "Candidates Lobby",
    MainSubheading2: "Add New Candidate",
    PaperHeading: "Personal Details",

    Labels: {
      label1: "Full Name",
      label2: "Contact Number ",
      label3: "Email ID",
      label4: "Associated Vacancy",
      label5: "Department",
      label6: "Total Experience (In Years)",
      label7: "Examiner",
    },
    DetailBox: {
      box1: "",
      box2: "",
      box3: "",
      box4: "",
      box5: "",
      box6: "",
      box7: "",
    },
    Placeholder: {
      placeholder1: "Enter candidate’s full name",
      placeholder2: "Enter candidate’s phone number",
      placeholder3: "Enter candidate’s email",
      placeholder4: "Choose job associated",
      placeholder5: "Choose Department",
      placeholder6: "Enter total experience",
      placeholder7: "Choose examiner",
    },
    Button: {
      button1: "Cancel",
      button2: "Save",
    },
    height: "368px",
    field: "Add",
    fieldType: "AddCandidate",
    Type: "Candidate",
    home: `/${role}/candidate`,
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen  data={data} />
    </div>
  );
}
