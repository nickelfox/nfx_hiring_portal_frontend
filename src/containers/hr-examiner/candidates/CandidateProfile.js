import { useHistory, useEffect, fetchClient, apiUrl, CssBaseline, Topbar, Sidebar, React, Emptyscreen } from "allImport";
import Commonprofile from "components/Commonprofile/Commonprofile";
import useStyles from "style/Profilestyle.js";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME, SUB_ADMIN_PERMISSION } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";
import { CircularProgress, Paper } from "@material-ui/core";
export default function CandidateProfile(props) {
  const classes = useStyles();
  const history = useHistory();
  const [loader, setLoader] = React.useState(false);
  const [candidateProfileDetails, setCandidateProfile] = React.useState([]);
  const [candidateProfileDetails1, setCandidateProfile1] = React.useState([]);
  const [candidateProfileDetails2, setCandidateProfile2] = React.useState([]);
  const [latest_status, set_latest_status] = React.useState([]);
  const [examStatus, setExamstatus] = React.useState([]);
  const [examTimelineData, setExamTimelineData] = React.useState([]);
  const [topbarVisible, setTopBarVisible] = React.useState(true);
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
     const candidateID = props && props.location && props.location.state && props.location.state.candidateID;

  const getPermissionsForSubAdmin = () => {
    let permission = {};
    if (role === "sub-admin") {
      let allPermissions = StorageManager.getJSON(SUB_ADMIN_PERMISSION);
      permission.edit = allPermissions.includes("EDIT_CANDIDATE");
      permission.delete = allPermissions.includes("DELETE_CANDIDATE");
      permission.hire_reject = allPermissions.includes("HIRE_REJECT_CANDIDATE");
      permission.generateLink = allPermissions.includes("GENERATE_EXAM_LINK");
    }
    return permission;
  };

  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
    fetchClient.get(`${apiUrl.candidateProfile}${candidateID}/`).then((res) => {
      setLoader(true);
      setCandidateProfile(res && res.data && res.data.data);
      setCandidateProfile1(res && res.data && res.data.data && res.data.data.user);
      setCandidateProfile2(res && res.data && res.data.data && res.data.data.examiner);
      setExamstatus(res && res.data && res.data.data && res.data.data.exam_status);
      set_latest_status(res && res.data && res.data.data && res.data.data.latest_status);
      setExamTimelineData(res && res.data && res.data.data && res.data.data.exam_status);
    });
  }, []);
  console.log(examStatus);
  const data = {
    MainHeading: candidateProfileDetails1.full_name,
    MainSubheading1: "Candidate Lobby",
    MainSubheading2: "Candidate Profile",
    PaperHeading: " Candidate Details",
    Labels: {
      label1: "Full Name",
      label2: "Contact Number",
      label3: "Email ID",
      label4: "Examiner",
      label5: "Job Associated",
      label6: "Department",
      label7: "Status",
      label8: "Attempt Level",
    },
    DetailBox: {
      box1: candidateProfileDetails1.full_name,
      box2: candidateProfileDetails1.phone_number,
      box3: candidateProfileDetails1.email,
      box4: candidateProfileDetails2.examiner_name,
      box5: candidateProfileDetails.job,
      box6: candidateProfileDetails2.examiner_department,
      box7: latest_status.candidate_status,
      box8: candidateProfileDetails.attempt_level,
    },
    home: `/${role}/candidate`,
    button1: "View Gallery",
    button2: "View Result",
    EditPath: `/${role}/candidate/edit`,
    ProfileType: "Candidate",
    candidateProfileDetails: candidateProfileDetails,
    examTimelineData: examTimelineData,
    candidateID: candidateID,

    permissions: getPermissionsForSubAdmin(),
    candidateExperience: candidateProfileDetails.experience,
    candidateUserData: candidateProfileDetails1,
    examStatus: examStatus,
    latest_status: latest_status.candidate_status,
    candidateName: candidateProfileDetails1.full_name,
    candidateDept: candidateProfileDetails2.examiner_department_id,
    hiring_status:candidateProfileDetails.is_hired
  }
  
  // Hide and Show Top Bar
  useEffect(() => {
    let scrollPos = 0;
    const listenToScroll = () => {
      const winScroll = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
      if (winScroll > scrollPos) {
           setTopBarVisible(false);
      } else {
        setTopBarVisible(true);  
      }  
      scrollPos = document.querySelector('#main-content')?.scrollTop || document.documentElement.scrollTop;
    };
    document.querySelector('#main-content')?.addEventListener('scroll', listenToScroll);
    return () => document.querySelector('#main-content')?.removeEventListener('scroll', listenToScroll);
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      {topbarVisible ? <Topbar /> : null}
      <Sidebar />
      <main className={classes.content} >

         {loader ? <Commonprofile data={data}/> :
         (<Paper className={classes.paper} style={{ height: "450px", justifyContent: "center", alignItems: "center",marginTop:'200px' }}>
              <CircularProgress color="error" />
                   </Paper>
                    )}
      </main>
    </div>
  );
}
