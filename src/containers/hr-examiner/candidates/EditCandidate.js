import { useEffect, CssBaseline, Topbar, Sidebar, React, useHistory } from "allImport";
import Commonfieldscreen from "components/CommonScreen/Commonfieldscreen";
import { useStyles } from "style/Editstyle.js";
import "style/style.css";
import { ADMIN_ID, API_TOKEN, LOGOUT_TOKEN, ADMIN_NAME } from "Storage/StorageKeys.js";
import StorageManager from "Storage/StorageManager.js";

export default function EditCandidate(props) {
  const classes = useStyles();
  const history = useHistory();
  const candidateName = props && props.location && props.location.state && props.location.state.candidateName;
  const candidateID = props && props.location && props.location.state && props.location.state.candidateID;
const role=localStorage.getItem('ROLE').toLowerCase()
  const data = {
    candidateJob:  props.location.state.candidateJob,
    Department: props.location.state.candidateDept,
    candidateExaminer: props.location.state.candidateExaminer,
    DeptID:props.location.state.DeptID,
    MainHeading: "Edit Candidate -" + candidateName,
    MainSubheading1: "Candidate Lobby",
    MainSubheading2: "Edit Candidate",
    PaperHeading: "Personal Details",
    Labels: {
      label1: "Full Name",
      label2: "Contact Number ",
      label3: "Email ID",
      label4: "Associated Vacancy",
      label5: "Department",
      label6: "Total Experience (In Years)",
      label7: "Examiner",
    },
    DetailBox: {
      box1: props.location.state.candidateName,
      box2: props.location.state.candidateNo,
      box3: props.location.state.candidateEmail,
      box4: props.location.state.candidateJob,
      box5: props.location.state.candidateDept,
      box6: props.location.state.candidateExperience,
      box7: props.location.state.candidateExaminer,
    },
    Placeholder: {
      placeholder1: "Enter candidate’s full name",
      placeholder2: "Enter candidate’s phone number",
      placeholder3: "Enter candidate’s email",
      placeholder4: "Choose job associated",
      placeholder5: "Choose Department",
      placeholder6: "Enter total experience",
      placeholder7: "Choose examiner",
    },
    Button: {
      button1: "Cancel",
      button2: "Update",
    },
    height: "368px",
    field: "Edit",
    candidateID: candidateID,
    Type: "Candidate",
    fieldType: "EditCandidate",
    home: `/${role}/candidate`,
  };
  useEffect(() => {
    if (!StorageManager.get(API_TOKEN) && !StorageManager.get(ADMIN_ID) && !StorageManager.get(ADMIN_NAME) && !StorageManager.get(LOGOUT_TOKEN)) {
      history.push("/login");
    }
  }, []);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Sidebar />
      <Commonfieldscreen  data={data} />
    </div>
  );
}
