import React, {useEffect} from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { menuItem,menuItem1, menuItemExaminer } from "./routes";
import { onMessageListener } from "firebaseOpenNotification";
import Login from "./containers/hr-examiner/Login/LogIn";
require('dotenv').config()

const App = ()=> {
 
 const role= localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;

 useEffect(() => {
  onMessageListener().then(payload => {
    console.log("Notification ::: ", payload.notification.body);
  }).catch((err) => { })
 },[])
   
   
    return (
      <Router>
        {/* <Redirect to="/login" /> */}
        <Switch>
          {role === "examiner" ? menuItemExaminer.map((i) => (
            <Route exact path={`/${role}`+i.path} key={`/${role}`+i.path} component={i.component} />
          )) :
          menuItem.map((i) => (
            <Route exact path={`/${role}`+i.path} key={`/${role}`+i.path} component={i.component} />
            // <DashboardRoute key={menu.path} path={menu.path} component={menu.component} heading={menu.headingText} isAuthenticated={this.props.isAuthenticated} permissions={this.props.permissions} />
          ))}
          {menuItem1.map((i) => (
            <Route exact path={i.path} key={i.path} component={i.component} />
            // <DashboardRoute key={menu.path} path={menu.path} component={menu.component} heading={menu.headingText} isAuthenticated={this.props.isAuthenticated} permissions={this.props.permissions} />
          ))}
          <Route component={Login}/>
        </Switch>
      </Router>
    );
 
}

export default App;
