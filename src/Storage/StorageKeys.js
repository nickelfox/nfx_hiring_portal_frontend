const API_TOKEN = "API_TOKEN";
const LOGOUT_TOKEN = "LOGOUT_TOKEN"
const ADMIN_ID = "ADMIN_ID"
const ADMIN_NAME ="ADMIN_NAME"
const EDITOR_ID ="EDITOR_ID"
const EXAMINER_DEPT ="EXAMINER_DEPT"
const ROLE="ROLE"
const SUB_ADMIN_PERMISSION = "SUB_ADMIN_PERMISSION"
export {EXAMINER_DEPT,ROLE, ADMIN_ID,API_TOKEN ,LOGOUT_TOKEN,ADMIN_NAME,EDITOR_ID, SUB_ADMIN_PERMISSION};
