export const commonStyle = (theme) => ({
    
    root:{
        display:"flex"
    },
    paper: {
        //padding: theme.spacing(2),
        display: 'flex',
        overflow: 'hidden',
        boxShadow: 'none',
        border: '1px solid #E5E5E5',
        flexDirection: 'column',
        marginTop:'32px',
        borderRadius:'12px'
      },
    fixedHeight: {
        height: 640,
      },
      appBarSpacer: theme.mixins.toolbar,
      content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
      },
      EditContainer: {
        paddingTop: '110px',
        paddingBottom: theme.spacing(4),
      },
      container: {
        paddingTop: '48px',
        paddingBottom: theme.spacing(4),
      },
      page_heading: {
        fontFamily: 'Mulish',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '32px',
        lineHeight: '50px',
        letterSpacing: '0.25px',
        color: '#18273B',
        [theme.breakpoints.down("md")]: {
          width: "109%",
        },
        [theme.breakpoints.down("sm")]: {
          width: "120%",
        },
       
      },
      dropdown_style:{
        fontFamily: 'Mulish',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '14px',
        lineHeight: '22px',
        letterSpacing: '0.25px',
      },
       page_subheading: {
        fontFamily: 'Mulish',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '14px',
        lineHeight: '22px',
        letterSpacing: '0.25px',
        color: '#6D6D6D',
        paddingTop: '4px',
        width:"176%",
      
      },
      all_examiners_heading: {
        fontFamily: 'Mulish',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: '20px',
        lineHeight: '32px',
        letterSpacing: '0.1px',
        color: '#000000',
        paddingRight:'19px'
      },
      filledButton: {
        background: '#D9001D',
        borderRadius: '8px',
        textTransform: 'capitalize',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '18px',
        lineHeight: '20px',
        textAlign: 'center',
        letterSpacing: '0.15px',
        color: '#FFFFFF',
        fontFamily: 'Mulish',
        width: '184px',
        height: '48px',
        transform: 'translateY(10px)',
      },
      
      normalButton:{
        background: '#fafafa',
        color: 'black',
        textTransform: 'capitalize',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '18px',
        lineHeight: '20px',
        textAlign: 'center',
        letterSpacing: '0.15px',
        fontFamily: 'Mulish',
        width: '184px',
        height: '48px',
        transform: 'translateY(10px)',
        marginRight: '-54px',
      },
    
      outlinedButton: {
        background: '#fafafa',
        color: 'black',
        border: '1px solid black',
        borderRadius: '4px',
        textTransform: 'capitalize',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '18px',
        lineHeight: '20px',
        textAlign: 'center',
        letterSpacing: '0.15px',
        fontFamily: 'Mulish',
        width: '184px',
        height: '45px',
        transform: 'translateY(10px)',
        marginRight: '-54px',
      },
      examiner_nos: {
        fontFamily: 'Mulish',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '14px',
        lineHeight: '20px',
        letterSpacing: '0.15px',
        color: '#FFFFFF',
        width: '61px',
        height: '26px',
        borderRadius: '20px',
        background: '#D9001D',
        textAlign: 'center',
        padding: '3px',
        transform: 'translateY(3px)',
       
      },
      CandidateButton: {

        background: '#D9001D',
        borderRadius: '4px',
        textTransform: 'capitalize',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '18px',
        lineHeight: '20px',
        textAlign: 'center',
        letterSpacing: '0.15px',
        color: '#FFFFFF',
        fontFamily: 'Mulish',
        width: '208px',
        height: '45px',
        transform: 'translateY(10px)',
  
       
  
      },
      hr_section_heading: {
        fontFamily: 'Mulish',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: '32px',
        lineHeight: '50px',
        letterSpacing: '0.25px',
        color: '#18273B',
        paddingBottom:'32px'
      },
    
      hr_detailbox:
      {
         
          width:'1150px',
          height:'180px'
          
      },
      
      hr_change_pass_heading:{
           
       fontFamily: 'Open Sans',
       fontSize: '20px',
       fontStyle: 'normal',
         fontWeight: 600,
        lineWeight: '32px',
       letterSpacing: '0.10000000149011612px',
       textAlign: 'left',
       color:'#000000',
       width:'213px',
       height:'32px',
       marginTop:'32px',
           marginLeft:'32px'
    
      }
    ,
      
     textfield:{
          // marginTop:'38px',
          // marginLeft:'32px',
          width:'291px',
          height:'46px'
      },
      leftbox: {
        marginLeft: "11px",
        marginTop: "18px",
    
        background: `url(${"/assets/Icons/signInChair.svg"})`,
        width: "100%",
        height: "873px",
        // backgroundColor: 'white',
        borderRadius: 10,
        backgroundSize: "568px 860px",
        backgroundRepeat: "no-repeat",
      },
    
      heading: {
        height: "35.74468231201172px",
        width: "128.68084716796875px",
        marginLeft: "99.04PX",
        marginTop: "-56px",
        fontSize: "26px",
        color: "#FFFFFF",
    
        fontWeight: "800",
      },
      subtext: {
        marginLeft: "100.23PX",
        color: "#FFFFFF",
        fontWeight: "400",
      },
      leftbigtext: {
        fontFamily: "Mulish",
        fontSize: "50px",
        fontStyle: "normal",
        fontWeight: "800",
        lineHeight: "65px",
        letterSpacing: "0px",
        textAlign: "left",
        color: "#FFFFFF",
        marginLeft: "49px",
        paddingTop: "157px",
        width: "458px",
       
      },
    
      lefttext: {
        fontFamily: "Mulish",
        fontSize: "24px",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "30px",
        letterSpacing: "0px",
        textAlign: "left",
        color: "#FFFFFFCC",
        width:"419px",
        marginLeft: "49px",
        paddingTop:"24px"
       
      },
      LinkStyle:{
        textDecoration:'none'
      }  ,
      nickelfoxlogo: {
        marginLeft: "49.02px",
        marginTop: "49.87px",
      },
      sidebar_nickelfoxlogo:{
        marginLeft: "52.02px",
        marginTop: "23.72px",
      },
      sidebar_subtext: {
        marginLeft: "100.23PX",
        color: "#FFFFFF",
        fontWeight: "400",
        marginBottom:'27px',
        fontSize:'12px'
      },
      sidebar_heading: {
        height: "20px",
        width: "72pxpx",
        marginLeft: "99.04PX",
        marginTop: "-42px",
        fontSize: "16px",
        color: "#FFFFFF",
    
        fontWeight: "800",
      },
      actionbutton:{
        display:'flex',
        justifyContent:'flex-end',
        [theme.breakpoints.down('md')]: {
          justifyContent: 'flex-start',
  
        },
        ButtonPosition:{
          display:'flex',
          justifyContent: "flex-end",
            [theme.breakpoints.down("sm")]: {
              justifyContent: "flex-start",
            },
        },     
      },
      exam_selection:{
          marginLeft:'32px',
          marginTop:'20px'
      }
    
})