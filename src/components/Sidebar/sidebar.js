import {Typography, ListItemIcon, ListItem, ListItemText, Drawer, CssBaseline, useEffect, makeStyles, React, clsx } from "allImport";
import "style/style.css";
import { useHistory } from "react-router-dom";
import { commonStyle } from "commonStyle";
import { Divider } from "@material-ui/core";

const drawerWidth = 245;

const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft:'52.02px',
    paddingTop:'23.72px',
    height: "100px",
    marginBottom: "14px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `87%`,
    height: "940px",
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    height: "950px",
    background: "#383838",
    borderRadius: "5px",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    margin: "10px",
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },

  anchorLinksSidebar: {
    textDecoration: "none",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  ListItemText: {
    color: "#FFFFFF",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "2rem",
    lineHeight: "16px",
    letterSpacing: "0.4px",
  },
}));

export default function Dashboard(props) {
  var role= localStorage.getItem('ROLE').toLowerCase();
 
  let history = useHistory();
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [optionValue, setOption] = React.useState("dashboard");
  
  useEffect(() => {
    console.log(` document.location.href`, document.location.href);
    if (window.location.pathname.includes(`/${role}/dashboard`)) {
      setOption(`/${role.toLowerCase()}/dashboard`);
    } else if (window.location.pathname.includes(`/${role}/candidate`)) {
      setOption(`/${role}/candidate`);
    } else if (window.location.pathname.includes(`/${role}/examiner`) && localStorage.getItem('ROLE')!=="EXAMINER") {
      setOption(`/${role}/examiner`);
    } else if (window.location.pathname.includes(`/${role}/exam`)) {
      setOption(`/${role.toLocaleLowerCase()}/exam`);
    } else if (window.location.pathname.includes(`/${role}/question-bank/questions`)) {
      setOption(`/${role}/question-bank/questions`);
    } else if (window.location.pathname.includes(`/${role}/submitted-exam`)) {
      setOption(`/${role}/submitted-exam`);
    } else if (window.location.pathname.includes(`/${role}/job`)) {
      setOption(`/${role}/job`);
    } else if (window.location.pathname.includes(`/${role}/subadmins`)) {
      setOption(`/${role}/subadmins`);
    } else if (window.location.pathname.includes(`/${role}/department`)) {
      setOption(`/${role}/department`);
    }
  }, []);

  const activeStateOption = (val) => {
    history.push({
      pathname: val,
    })
    setOption(val);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <img src="/assets/Icons/nickelfox_logo.svg" width='31.33px' height="44.56px" alt="nickelfoxlogo" className={classes.sidebar_nickelfoxlogo} />
          <Typography className={classes.sidebar_heading}>NickelFox</Typography>
          <Typography className={classes.sidebar_subtext}>Hiring Portal</Typography>
           <Divider style={{border:'1px solid rgba(255, 255, 255, 0.1)',marginBottom:'21px'}}/>
          <div className={optionValue === `/${role}/dashboard` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role.toLowerCase()}/dashboard`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/dashboard` ? "/assets/Icons/active_dash.svg" : "/assets/Icons/dashboard_inactive.svg"} alt="dashboard" />
              </ListItemIcon>
              <ListItemText className={{ primary: classes.listItemText }} primary="Dashboard" />
            </ListItem>
          </div>

          <div className={optionValue === `/${role}/candidate` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/candidate`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/candidate` ? "/assets/Icons/active_candidates.svg" : "/assets/Icons/candidates_inactive.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Candidates" />
            </ListItem>
          </div>

      {role==="examiner"?null:
          <div className={optionValue === `/${role}/examiner` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/examiner`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/examiner` ? "/assets/Icons/active_examiner.svg" : "/assets/Icons/examiner_inactive.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Examiners" />
            </ListItem>
          </div>}

          <div className={optionValue === `/${role}/exam` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/exam`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/exam` ? "/assets/Icons/active_exam.svg" : "/assets/Icons/inactive_exam.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Exam" />
            </ListItem>
          </div>

          <div className={optionValue === `/${role}/question-bank/questions` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/question-bank/questions`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/question-bank/questions` ? "/assets/Icons/active_question_bank.svg" : "/assets/Icons/questionBank_inactive.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Question Bank" />
            </ListItem>
          </div>

          <div className={optionValue === `/${role}/submitted-exam` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/submitted-exam`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/submitted-exam` ? "/assets/Icons/active_results.svg" : "/assets/Icons/results_inactive.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Submitted Exams" />
            </ListItem>
          </div>

        {role==="examiner"?null:<>
          <div className={optionValue === `/${role}/job` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/job`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/job` ? "/assets/Icons/active_jobs.svg" : "/assets/Icons/job_inactive.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Vacancies" />
            </ListItem>
          </div>

          <div className={optionValue === `/${role}/department` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/department`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/department` ? "/assets/Icons/active_departments.svg" : "/assets/Icons/active_departments.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="Department" />
            </ListItem>
          </div>
        
          <div className={optionValue === `/${role}/subadmins` ? "side_button_wrapper activeSelection" : "side_button_wrapper"} onClick={() => activeStateOption(`/${role}/subadmins`)}>
            <ListItem button className="sidebar_button">
              <ListItemIcon>
                <img src={optionValue === `/${role}/subadmins` ? "/assets/Icons/active_admins.svg" : "/assets/Icons/sub_admin_inactive.svg"} alt="" />
              </ListItemIcon>
              <ListItemText className={classes.sidebar_font} primary="SubAdmins" />
            </ListItem>
          </div>
        </>}
      </Drawer>
    </div>
  );
}
