import { clsx, makeStyles, Paper, React, useEffect, useState, Link, useHistory, apiUrl, fetchPost, Zoom } from "allImport";
import { commonStyle } from "commonStyle";
import { withSnackbar } from "notistack";
import {convertUTCDateToLocalDate } from "Common/CommonFunction";
const useStyles = makeStyles((theme) => ({
    ...commonStyle(theme),
    container: {
        padding: "32px",
    },
    topContainer: {
        display: "flex",
        justifyContent: "space-between",
    },
    heading : {
        fontFamily: "Mulish",
        fontSize: "20px",
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: "32px",
        letterSpacing: "0.10000000149011612px",
        color: "#0000004D",
    },
    resendLinkButton: {
        height: 23,
        width: 107,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        fontFamily: "Mulish",
        fontsize: "12px",
        fontStyle: "normal",
        fontWeight: "700",
        lineHeight: "20px",
        letterSpacing: "0.15000000596046448px",
        color: "#FFFFFF",
        backgroundColor: "#D9001D",
        borderRadius: "4px",
        cursor: "pointer",
    },
    timelineContainer: {
        width: "100%",
        minWidth: 650,
    },
    stepperContainer: {
        width: "100%",
        paddingRight: 195,
        boxSizing: "border-box",
        marginTop: 60,
    },
    stepper: {
        display: "flex",
        height: "4px",
        width: "100%",
        justifyContent: "space-between",
        backgroundColor: "#0000004D",
        position: "relative"    
    },
    stepperProgress: {
        position: "absolute",
        height: "100%",
        width: "33.33%",
        backgroundColor: "#000000",
    },
    stepContainer: {
        height: 16,
        width: 16,
        borderRadius: "50%",
        position: "relative",
        top: "-6px",
        backgroundColor: "#ffffff"
    },
    step: {
        height: 16,
        width: 16,
        borderRadius: "50%",
    },
    stepActive: {
        backgroundColor: "#000000",
    },
    stepInactive: {
        backgroundColor: "#0000004D",
    },
    timelineStatusContainer: {
        display: "flex",
        justifyContent: "space-between",
        width: "100%",
        paddingRight: 20,
    },
    timelineStatus: {
        marginRight: 10,
        width: 175,
    },
    statusInactive: {
        color: "#0000004D",
    },
    timelineStatusName: {
        fontFamily: "Open Sans",
        fontSize: "20px",
        fontStyle: "normal",
        fontWeight: "700",
        lineHeight: "20px",
        letterSpacing: "0.15000000596046448px",
        marginTop: 26,
        textTransform: "lowercase",
        textTransform: "capitalize",
    },
    timelineStatusDate: {
        fontFamily: "Mulish",
        fontSize: "14px",
        fontStyle: "normal",
        fontWeight: "400",
        lineHeight: "20px",
        letterSpacing: "0px",
        marginTop: 17,
    },
    buttonContainer: {
        width:"100%",
        display: "flex",
        justifyContent: "center",
        marginTop: 84,
    },
    viewResultButton: {
        border: "1px solid #D9001D",
        borderRadius: 4,
        width: 208,
        height: 45,
        fontFamily: "Mulish",
        fontSize: "18px",
        fontStyle: "normal",
        fontWeight: "700",
        lineHeight: "20px",
        letterSpacing: "0.15000000596046448px",
        color: "#D9001D",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer",
    },
    green:{
        color: "#29CB1B",
    },
    red: {
        color: "#D9001D",
    },
    copyExamLink: {
        cursor: "pointer",
        color: "blue",
        textDecoration: "underline",
    }
}))

const ExamTimeLine = (props) => {
    const classes = useStyles();
    const [stepperProgress, setStepperProgress] = useState(0);
    const [activeSteps, setActiveSteps] = useState(0);
    const [examLevel, setExamLevel] = useState("L1");
    const [data, setData] = useState({isSet: false});
    const [resendLinkText, setResendLinkText] = useState("Resend Link");
    const history = useHistory();
    const role=localStorage.getItem('ROLE')?localStorage.getItem('ROLE').toLowerCase():null;


    const initializeStepper = (data) => {
      
        if(data.length === 0){
            setStepperProgress(0);
        }
        else if(data.length === 3 && data[2].status==="DISQUALIFY")
        {
            setStepperProgress(((data.length-1)/2)*100);
        }
        else{

            setStepperProgress(((data.length-1)/3)*100);
        }
        setActiveSteps(data.length);
    }

    const initializeData = (data) => {
        console.log(data);
        
        initializeStepper(data)
        const timeLineData = {
            isSet: true,
            ExamID: data[0].exam_id,
            AttemptDate: data[0].created_at,
            examLinkSent: {
                linkSent: data.length <3,
                examLink: data[0].exam_link,
                time: data.length >=1 ? `${convertUTCDateToLocalDate(new Date(data[0].created_at)).toLocaleString()}` : "",
            },
            examAttempted: {
                time: data.length >=2 ? `${convertUTCDateToLocalDate(new Date(data[1].created_at)).toLocaleString()}` : "",
            },
            examSubmitted: {
                text: data.length >=3 ? data[2].status === "DISQUALIFY" ? "Disqualified" : "Exam Submitted" : "Exam Submitted",
                time: data.length >=3 ? `${convertUTCDateToLocalDate(new Date(data[2].created_at)).toLocaleString()}` : "",
            },
            evaluation: {
                text: data.length >=4 ? `${data[3].status}` : "To be evaluated",
                time: data.length >=4 ? `${convertUTCDateToLocalDate(new Date(data[3].created_at)).toLocaleString()}` : "",
            }
        }
        setData(timeLineData);
    }

    const resendLink = () => {
        setResendLinkText("Sending...");
        const data1 = {
        candidate: props.candidateID,
        exam: data.ExamID,
        exam_link: data.examLinkSent.examLink,
        };
        console.log(data1)
        fetchPost.post(`${apiUrl.resendLink}`, data1).then((res) => {
            props.enqueueSnackbar(`${res.data.data.message}` , { 
                variant: res.data.success ? 'success' : 'error',
                anchorOrigin: {
                    vertical: 'top',
                    horizontal: 'right',
                },
                TransitionComponent: Zoom,
                autoHideDuration:3000,    
            })
        }).catch(() => {
            props.enqueueSnackbar('Failed to send email', { 
                variant: 'error',
                anchorOrigin: {
                    vertical: 'top',
                    horizontal: 'right',
                },
                TransitionComponent: Zoom,
                autoHideDuration:5000,
                resumeHideDuration: 2000
            })
        }).finally(() => {
            setResendLinkText("Resend Link");
        })
    }

    const viewResult = () => {
        console.log(props.candidateID)
        console.log(data.ExamID)
        console.log(data.AttemptDate)
        history.push({
            pathname: `/${role}/submitted-exam/result`,
            state: {
                ExamCandidateID: props.candidateID,
                ExamIDr: data.ExamID,
                AttemptDate: data.AttemptDate,
            }
        })
    }

    const copyExamLink = () => {
        navigator.clipboard.writeText(data.examLinkSent.examLink)
        props.enqueueSnackbar(`Exam Link Copied` , { 
            variant:'success',
            anchorOrigin: {
                vertical: 'top',
                horizontal: 'right',
            },
            TransitionComponent: Zoom,
            autoHideDuration:3000,    
        })
    }

    useEffect(() => {
        const level = Object.keys(props.data)[0];
        const data = props.data[level];
        setExamLevel(level);
        initializeData(data);
    }, [])

    return(
        <Paper className={clsx(classes.paper, classes.container)}>
            {data.isSet ?
                <><div className={classes.topContainer}>
                    <div className={classes.heading}>{examLevel} Exam Timeline</div>
                    {props.isLast && data.examLinkSent.linkSent ? <div onClick={() => resendLink()} className={classes.resendLinkButton}>{resendLinkText}</div> : null}
                </div>
                <div className={classes.timelineContainer}>
                    <div className={classes.stepperContainer}>
                        <div className={classes.stepper}>
                            <div className={classes.stepperProgress} style={{width: `${stepperProgress}%`}}/>
                            <div className={classes.stepContainer}><div className={clsx(classes.step, activeSteps >= 1 ? classes.stepActive : classes.stepInactive)}/></div>
                            <div className={classes.stepContainer}><div className={clsx(classes.step, activeSteps >= 2 ? classes.stepActive : classes.stepInactive)}/></div>
                            <div className={classes.stepContainer}><div className={clsx(classes.step, activeSteps >= 3 ? classes.stepActive : classes.stepInactive)}/></div>
                            {data.examSubmitted.text === "Disqualified" ? null:<div className={classes.stepContainer}><div className={clsx(classes.step, activeSteps >= 4 ? classes.stepActive : classes.stepInactive)}/></div>}
                        </div>
                    </div>
                    <div className={classes.timelineStatusContainer}>
                        <div className={clsx(classes.timelineStatus, activeSteps >= 1 ? null : classes.statusInactive)}>
                            <div className={classes.timelineStatusName}>Exam Link Sent</div>
                            <div className={classes.timelineStatusDate}>{data.examLinkSent.time}</div>
                            {props.isLast && data.examLinkSent.linkSent ? 
                            <div className={classes.copyExamLink} onClick={()=>copyExamLink()}>Copy Exam Link</div> : null}
                        </div>
                        <div className={clsx(classes.timelineStatus, activeSteps >= 2 ? null : classes.statusInactive)}>
                            <div className={classes.timelineStatusName}>Exam Attempted</div>
                            <div className={classes.timelineStatusDate}>{data.examAttempted.time}</div>
                        </div>
                        <div className={clsx(classes.timelineStatus, activeSteps >= 3 ? null : classes.statusInactive)}>
                            <div className={clsx(classes.timelineStatusName, data.examSubmitted.text === "Disqualified" ? classes.red : null)}>
                                {data.examSubmitted.text}
                            </div>
                            <div className={classes.timelineStatusDate}>{data.examSubmitted.time}</div>
                        </div>
                        {data.examSubmitted.text === "Disqualified" ? null: <div className={clsx(classes.timelineStatus, activeSteps >= 4 ? null : classes.statusInactive)}>
                            <div className={clsx(classes.timelineStatusName, data.evaluation.text === "PASSED" ? classes.green : data.evaluation.text ==="FAILED" ? classes.red : null)}>
                                {data.evaluation.text.toLowerCase()}
                            </div>
                            <div className={classes.timelineStatusDate}>{data.evaluation.time}</div>
                        </div>}
                    </div> 
                    {!data.examLinkSent.linkSent ? <div onClick={() => viewResult()} className={classes.buttonContainer}>
                        <div className={classes.viewResultButton}>View Result</div>
                    </div> : null}
                </div></> :
             null}
        </Paper>
    )
}

export default withSnackbar(ExamTimeLine);
