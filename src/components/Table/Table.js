import React from "react";
import "style/style.css"
import { DeleteAlert, Paper, Table, TableBody, TableCell, useEffect, TableContainer, TableHead, TableRow, Link } from "allImport";
import { useStyles } from "style/Liststyle.js";
import { withSnackbar } from "notistack";
import { useBool } from "context/BoolContext";
import { capitalize } from "Common/CommonFunction";
const Commontable = (props) => {
  const {hireRejectVisible,select, setSelect } = useBool();
  
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const classes = useStyles();
console.log("ii",props.data.permissions)
  const col1 = props && props.data && props.data.columnnames && props.data.columnnames.col1;
  const col2 = props && props.data && props.data.columnnames && props.data.columnnames.col2;
  const col3 = props && props.data && props.data.columnnames && props.data.columnnames.col3;
  const col4 = props && props.data && props.data.columnnames && props.data.columnnames.col4;
  const col5 = props && props.data && props.data.columnnames && props.data.columnnames.col5;
  const col6 = props && props.data && props.data.columnnames && props.data.columnnames.col6;
  const col7 = props && props.data && props.data.columnnames && props.data.columnnames.col7;
  const col8 = props && props.data && props.data.columnnames && props.data.columnnames.col8;

  const [arrayList, setArrayList] = React.useState([]);

  const editpath = props.data.EditPath;
  const profile = props.data.profile;
  const assignexam = props.data.AssignExam;
  const [id, setid] = React.useState();
  const [Name, setName] = React.useState();
  var [Type, setType] = React.useState();
  
  useEffect(() => {
    props.data && props.data.List && setArrayList(props.data.List);
  });

  var Delete = (index, name, table) => {
    console.log(table);
    if (table === "Job") {
      setSelect(true);
      setid(index);
      setType(table);
      setName("Vacancy");
    } else if (table === "Candidate") {
      setSelect(true);
      setid(index);
      setType(table);
      setName(name + " Profile");
    } else if (table === "Examiner") {
      setName(capitalize(name) + " Profile");
      setSelect(true);
      setid(index);
      setType(table);
    } else if (table === "Exam") {
      setName(capitalize(name) + " Exam");
      setSelect(true);
      setid(index);
      setType(table);
    } else if (table === "ExaminerD") {
      setName(capitalize(name) + " Exam");
      setSelect(true);
      setid(index);
      setType(table);
    } else if (table === "JobD") {
      setName(capitalize(name) + " Exam");
      setSelect(true);
      setid(index);
      setType(table);
    } else if (table === "Subadmin") {
      setName(capitalize(name) + " Profile");
      setSelect(true);
      setid(index);
      setType(table);
    }
  };


  return (
    <>
    {select ?  <DeleteAlert  type={Type} de="t" heading={`Are you sure you want to Delete the ${Name} ?` } id= {id} />:null}
  
    <TableContainer component={Paper}>
      <Table className={classes.table}  aria-label="simple table">
        <TableHead>
          <TableRow>
          {props.data.TableType === "Candidate" ? <TableCell align="left">{col6} </TableCell>:null}
            <TableCell>{col1}</TableCell>
            <TableCell align="left">{col2}</TableCell>
            {props.data.TableType ==="JobD" || (props.data.TableType ==="ExamCandidate" && role==='examiner')||props.data.TableType ==="Candidate" ? null:<TableCell align="left">{col3}</TableCell>}
            {props.data.TableType ==="ExaminerD" ? null: props.data.TableType === "Candidate"&& role==="examiner" ? null: <TableCell align="left">{col4}</TableCell>}
            {props.data.TableType === "Candidate"&& role==="examiner"? null :<TableCell align="left">{col5} </TableCell>}
            {props.data.TableType === "Candidate" ? null:<TableCell align="left">{col6} </TableCell>}
            {props.data.TableType === "Candidate" || props.data.TableType === "ExamCandidate"? <TableCell align="left">{col7}</TableCell> : null}
            {props.data.TableType === "Candidate" ? <TableCell align="left">{col8}</TableCell> : null}
            <TableCell/>
           
            {hireRejectVisible ? null:props.data.TableType === "Candidate" ? <TableCell/>:null}
          </TableRow>
        </TableHead>
        <TableBody>
          {arrayList &&
            arrayList.length > 0 &&
            arrayList.map((row, index) => (
              <TableRow
                key={index}
              >
                  {props.data.TableType==="Candidate" ?<TableCell component="th" scope="row" style={{color:"black"}}><div className={classes.level_style} >{row && row.ColF}</div></TableCell>:null}
                {role === "sub-admin" && props.data.permissions && !props.data.permissions.view ? 
                 <TableCell component="th" scope="row" style={{color:"black"}}>{row && row.ColA.slice(3,8)}</TableCell> :
               <>{props.data.TableType==="Subadmin" ? <TableCell component="th" scope="row"><Link
                  to={{
                    pathname: profile,
                    state: {
                      SubAdminID: row.ColA,
                    },
                  }}
                  style={{color:"black"}}
                > {row && row.ColA.slice(3,8)} </Link> </TableCell>
                     
                   :
                    <TableCell component="th" scope="row">
                    <Link
                    to={{
                      pathname: profile,
                      state: {
                        examinerID: row && row.ColA,
                        examinerName: row && row.ColB,
                        examinerEmail:row && row.ColF ,
                        examinerDept: row && row.ColD,
                        examinerDesignation: row && row.ColC,
                        examinerNo : row &&row.ColG,
                        candidateNo:row && row.ColI,
                        candidateEmail:row && row.ColH ,
                        DeptData: props.data.DeptData,
                        candidateID:row && row.ColA ,
                        candidateStatus:row.ColF ,
                        jobID: row && row.ColA,
                        ExamID: row && row.ColA,
                        ExamName: row && row.ColB,
                        departmentID: row && row.ColA,
                        departmentName: row && row.ColB,
                        dateAdded: row && row.ColC,
                        departmentStatus: row && row.ColD,
                        SubmitExamID: row.ColG,
                        ExamCandidateID: row.ColA,
                        ExamIDr:row.ColI,
                        AttemptDate:props.data.AttemptDate,
                        ExamDept:row && row.ColE
                      },
                    }}
                    style={{color:"black"}}
                  >
                    {row && row.ColA.slice(3,8)} 
                    </Link>
                    </TableCell>}</>}

                
                   <TableCell align="left">{capitalize(row && row.ColB)}</TableCell>
                {props.data.TableType ==="JobD" ||(props.data.TableType ==="ExamCandidate" && role==='examiner')||props.data.TableType ==="Candidate" ? null:<TableCell align="left">{capitalize(row && row.ColC)}</TableCell>}
               
                {props.data.TableType ==="ExaminerD" ? null: props.data.TableType === "Candidate"&& role==="examiner" ? null:<TableCell align="left">{capitalize(row && row.ColD)}</TableCell>}
                {props.data.TableType === "Candidate"&& role==="examiner"  ? null :props.data.TableType === "Candidate"? <TableCell align="left">{capitalize(row && row.ColC)} <br/> {capitalize(row && row.ColE)}</TableCell>: <TableCell align="left">{capitalize(row && row.ColE)}</TableCell>}
                {props.data.TableType ==="Examiner" || props.data.TableType ==="ExaminerD" ?<TableCell/>:props.data.TableType ==="Candidate" ? null : <TableCell align="left">{capitalize(row && row.ColF)  }</TableCell>}

                {props.data.TableType ==="ExamCandidate" || props.data.TableType ==="Candidate" ? <>
                <TableCell align="left">{row && row.ColG }</TableCell>
                {props.data.TableType ==="Candidate" ? <TableCell align="left">{row && row.ColH ? row && row.ColH :"LINK_NOT_SENT" }</TableCell> : null}
                </>:null
                }
               {hireRejectVisible ? null:props.data.TableType === "Candidate" ? <TableCell style={{width:'107px'}}>{row.ColM===true ? <div className={classes.hire_style} >Hired </div> :row.ColM===false ? <div  className={classes.reject_style} >Rejected</div>:null}</TableCell>:null}
               {props.data.TableType === "Result" || props.data.TableType==="ExamCandidate"  ? <TableCell/> :
                <TableCell align="right">
                  <div className={classes.tableIcons}>
                  {props.data.TableType === "Candidate" && row.ColM===null&& role !=="examiner" ? <>{(role === "admin" || (role === "sub-admin" && props.data.permissions.generateLink)) && (!(row && row.ColH) || (row.ColH === "PASSED")) ? <Link
                  to={{
                    pathname:assignexam,
                    state: {
                      candidateID: row && row.ColA,
                      candidateName: row && row.ColB,
                      DeptID: row && row.ColL,
                    }
                  }}
                  >
                  <img className={classes.assignAction} src="/assets/Icons/assign_exam_active.svg" alt="Assign Exam" /></Link> : 
                  <img className={classes.assignAction} src="/assets/Icons/assign_exam_inactive.svg" alt="Assign Exam" />}</>:null}
                  
                 {(role === "sub-admin" && !props.data.permissions.edit) || (props.data.TableType === "Candidate" && role === "examiner") || props.data.TableType === "Subadmin" || props.data.TableType === "ExaminerD" || props.data.TableType === "JobD"?null :<Link
                   to={{
                     pathname: editpath,
                    state: {
                      candidateID: row && row.ColA,
                      candidateName: row && row.ColB,
                      candidateJob: row && row.ColC,
                      candidateDept:row && row.ColE,
                      candidateExaminer: row && row.ColD,
                      candidateNo:row && row.ColJ,
                      candidateEmail:row && row.ColI ,
                      candidateExperience: row && row.ColK,
                      examinerID: row && row.ColA,
                       examinerName: row && row.ColB,
                       examinerEmail:row && row.ColF ,
                        examinerNo : row &&row.ColG,
                       examinerDept: row && row.ColD,
                       examinerDesignation: row && row.ColC,
                       DeptData:row && row.ColD ,
                       jobID: row && row.ColA,
                       jobTitle:row && row.ColB,
                       jobDepts:row && row.ColC,
                       jobLevel:row && row.ColD,
                       jobExperience: row &&row.ColG,
                       examName: row && row.ColB,
                       examLevel: row && row.ColC,
                       examID:  row && row.ColA,
                       departmentID: row && row.ColA,
                       departmentName: row && row.ColB,
                       examDept: row && row.ColE,
                       DeptID: row && row.ColL,
                     },
                   }}
                  >
                   <img className="editAction" src="/assets/Icons/editIcon.svg" alt="" />
                  </Link>}

                  {(role === "sub-admin" && props.data.permissions.edit && props.data.TableType === "Subadmin") ||
                  ( role !== "sub-admin" && props.data.TableType === "Subadmin") ? <Link
                    to={{
                      pathname: editpath,
                    state: {
                      SubAdminID: row && row.ColA,
                    }}}
                  >
                    <img className="editAction" src="/assets/Icons/editIcon.svg" alt="" />
                  </Link> : null}

                  {(role === "sub-admin" && !props.data.permissions.delete) || (props.data.TableType==="Result" || props.data.TableType==="Department" || props.data.TableType === "ExaminerD" || props.data.TableType === "JobD" ||  (props.data.TableType === "Candidate" && role === "examiner" ))?  null : <img onClick={() => {Delete(row.ColA,row.ColB,props.data.TableType)}} className={classes.deleteAction} src="/assets/Icons/deleteIcon.svg" alt="" />}
                 
                 
                 
                  </div>
                </TableCell>}
                
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
    </>
  );
};
export default withSnackbar(Commontable);
