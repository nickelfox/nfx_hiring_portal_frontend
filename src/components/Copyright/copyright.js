import  {React,Typography,Link} from 'allImport';

function copyright() {
    return (
        <div>
             <Typography variant="body2" color="textSecondary" align="center">
                {'Copyright © '}
                <Link color="inherit" href="https://www.nickelfox.com/">
                    Nickelfox
                </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
             </Typography>
        </div>
    )
}

export default copyright
