
import {useHistory,DialogContent,Dialog,MuiDialogTitle,IconButton,withStyles, React,Typography,CloseIcon} from "allImport";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  ModelSize:{
    height:'283px',
    minWidth: "416px"
  }
 
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});




const  CustomizedDialogs = withStyles(styles)((props)=> {
  const [open, setOpen] = React.useState(true);
  const {classes}=props;
   const history =useHistory();
  const handleClose = () => {
    setOpen(false);
    history.goBack()
  };

  return (
    <div>
    
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}  classes={{paper:classes.ModelSize}}>
        <DialogTitle id="customized-dialog-title" onClose={handleClose}/>
         
        <DialogContent  >
          <Typography gutterBottom style={{paddingBottom:"16px",paddingTop:'21px'}}>
            <div className = "success_bar">
            <img src="/assets/Icons/success_icon.svg" alt="" />
            </div>
            
          </Typography>
          <Typography gutterBottom style={{paddingBottom:0}}>
          {props.heading}
          </Typography>
          
          
        </DialogContent>
    
      </Dialog>
    </div>
  );
});
export default CustomizedDialogs;