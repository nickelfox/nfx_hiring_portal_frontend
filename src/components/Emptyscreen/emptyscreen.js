import React from "react";
import { Paper, makeStyles } from "allImport";
export const useStyles = makeStyles((theme) => ({
  emptyicon: {
    marginTop: "100px",
    fontStyle: "normal",
    fontWeight: "600",
    fontSize: "20px",
    lineHeight: "32px",
    letterSpacing: "0.1px",
    color: "rgba(0, 0, 0, 0.3)",
    fontFamily: "Mulish",
  },

  empty_wrapper: {
    textAlign: "center",
    marginTop: "40px",
  },
}));
export default function Emptyscreens(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Paper className={classes.paper} style={{ height: "380px" }}>
        <div className={classes.empty_wrapper}>
          <img src="/assets/Icons/no_entry.svg" className={classes.emptyicon} alt="" />
        </div>
      </Paper>
    </div>
  );
}
