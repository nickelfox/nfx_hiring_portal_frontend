import { OutlinedInput, makeStyles, withStyles, Checkbox, FormControlLabel, Typography, fetchPost, clsx, Link, useHistory, React, fetchClient, FormControl, fetchUpdate, apiUrl, useEffect, SuccessDialog, Grid, Container, Box, Copyright, Button, Paper, CircularProgress, TextField } from "allImport";
import { commonStyle } from "commonStyle.js";
import { withSnackbar } from "notistack";
import { Autocomplete } from "@material-ui/lab";
import { capitalize } from "Common/CommonFunction";
import "style/style.css";
const useStyles = makeStyles((theme) => ({
  ...commonStyle(theme),
  select_style: {
    marginTop: "10px",
    height: "47px",
    width: "307px",
    fontSize:"14px", 
   fontFamily : 'Mulish',
   fontStyle : 'normal',
   fontWeight : 'normal',
   lineHeight : '22px',
   letterSpacing: '0.25px',
  },
  paper_heading: {
    fontStyle: "normal",
    fontWeight: "600",
    fontSize: "20px",
    lineHeight: "32px",
    letterSpacing: "0.1px",
    color: "rgba(0, 0, 0, 0.3)",
    fontFamily: "Mulish",
    paddingTop: "32px",
    paddingLeft: "32px",
  },
  permission_heading: {
    fontStyle: "normal",
    paddingLeft: "144px",
    fontWeight: "700",
    fontSize: "14px",
    lineHeight: "17.57px",
    letterSpacing: "0.1px",
    color: "rgba(0, 0, 0, 0.3)",
    fontFamily: "Mulish",
  },
  section_heading: {
    fontStyle: "normal",
    paddingTop: "15px",
    paddingLeft: "32px",
    fontWeight: "700",
    fontSize: "14px",
    lineHeight: "17.57px",
    letterSpacing: "0.1px",
    color: "#000000",
    fontFamily: "Mulish",
    width: "133.33px",
  },
  permission_heading_box: {
    display: "flex",
    paddingLeft: "102px",
    marginTop: "24px",
    marginBottom: "6px",
  },
  examiner_detail_box: {
    marginTop: "10px",
  },
  examiner_box: {
    background: "#FFFFFF",
    border: "1px solid #BBBBBB",
    boxSizing: "border-box",
    borderRadius: "4px",
    width: "307px",
    height: "46px",
    //textTransform: 'capitalize'
  
  },
  permission_paper: {
    backgroundColor: "rgba(196, 196, 196, 0.12)",
    height: "42px",
    marginTop: "14px",
    width: "1150px",
  },
  mandotory: {
    color: "#D9001D",
    paddingLeft: "5px",
  },
  label: {
    fontFamily: "Open Sans",
    fontSize: "14px",
    fontStyle: "normal",
    fontWeight: "400",
    lineHeight: "20px",
    letterSpacing: "0.15000000596046448px",
    color: "#000000",
  },
  checkboxContainer: {
    display: "flex",
    margin: "22px 0 32px 32px",
  },
  permissionContainer: {
    marginTop:"32px",
    borderBottom: "1px solid #0000001A",
  },
  loaderContainer: {
    position: "absolute",
    height: "100%",
    width: "100%",
    top: "0",
    left: "0",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: "100",
  }
}));
const Checkboxblack = withStyles({
  root: {
    "& .MuiSvgIcon-root": {
      fill: "white",
      height: "15px",
      width: "15px",
      "&:hover": {
        backgroundColor: "white",
      },
    },
    "&$checked": {
      "& .MuiIconButton-label": {
        position: "relative",
        zIndex: 0,
        border: "2px solid black",
      },
      "& .MuiIconButton-label:after": {
        content: '""',
        height: 15,
        width: 15,
        position: "absolute",
        backgroundColor: "black",
        border: "2px solid white",
        zIndex: -1,
      },
    },
    "&:not($checked) .MuiIconButton-label": {
      position: "relative",
      zIndex: 0,
      border: "2px solid black",
    },
    "&:not($checked) .MuiIconButton-label:after": {
      content: '""',
      height: 15,
      width: 15,
      position: "absolute",
      backgroundColor: "white",
      zIndex: -1,
    },
  },
  checked: {},
})(Checkbox);
const Commonfieldscreen = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const DeptID=props.data.DeptID;
  const candidateJob = props.data.candidateJob;
  const JobLevel = props.data.JobLevel;
  const heading = props.data.MainHeading;
  const paperheading = props.data.PaperHeading;
  const subheading1 = props.data.MainSubheading1;
  const [box1, setBox1] = React.useState("");
  const [box2, setBox2] = React.useState([]);
  const [box3, setBox3] = React.useState();
  const [box4, setBox4] = React.useState();
  const [box5, setBox5] = React.useState();
  const [box6, setBox6] = React.useState("");
  const [box7, setBox7] = React.useState();
  const subheading2 = props.data.MainSubheading2;
  const subheading3 = props.data.MainSubheading3;
  const button1 = props.data.Button.button1;
  const button2 = props.data.Button.button2;
  const label1 = props.data.Labels.label1;
  const label2 = props.data.Labels.label2;
  const label3 = props.data.Labels.label3;
  const label4 = props.data.Labels.label4;
  const label5 = props.data.Labels.label5;
  const label7 = props.data.Labels.label7;
  const ph1 = props.data.Placeholder.placeholder1;
  const ph2 = props.data.Placeholder.placeholder2;
  const ph3 = props.data.Placeholder.placeholder3;
  const ph4 = props.data.Placeholder.placeholder4;
  const [jobToggle, setJobToggle] = React.useState(false);
  const [UpdatejobToggle, setUpdateJobToggle] = React.useState(false);
  const [examinerToggle, setExaminerToggle] = React.useState(false);
  const [UpdateexaminerToggle, setUpdateExaminerToggle] = React.useState(false);
  const [candidateToggle, setCandidateToggle] = React.useState(false);
  const [UpdatecandidateToggle, setUpdateCandidateToggle] = React.useState(false);
  const [departmentToggle, setDepartmentToggle] = React.useState(false);
  const [UpdateDepartmentToggle, setUpdateDepartmentToggle] = React.useState(false);
  const [subAdminToggle, setSubAdminToggle] = React.useState(false);
  const [UpdateSubAdminToggle ,setUpdateSubAdminToggle] = React.useState(false);
  const [deptLists, setDeptlist] = React.useState([]);
  const [jobLists, setJoblist] = React.useState([]);
  const [examinerLists, setExaminerlist] = React.useState([]);
  const [permissionLists, setpermissionlist] = React.useState([]);
  const [subAdminPermissions, setsubAdminPermissions] = React.useState([]);
  const [allowedSubAdminPermissions, setAllowedSubAdminPermissions] = React.useState({});
  const [loader, setLoader] = React.useState(true);
  const candidateID = props && props.data && props.data.candidateID;
  const jobID = props && props.data && props.data.jobID;
  const examinerID = props && props.data && props.data.examinerID;
  const departmentID = props && props.data && props.data.departmentID;
  const Level = ["Senior", "Junior", "Internship"];
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const editExaminerDetails = (e, type) => {
    const value =  e.target.value;
    if (type === "fullname") {
      setBox1(value);
      console.log(`value11`, value);
    } else if (type === "email") {
      console.log(`value11`, value);
      setBox3(value);
    } else if (type === "designation") {
      setBox4(value);
    } else if (type === "department") {
      console.log(`value`, value);
      setBox5(value);
    } else if (type === "experience") {
      console.log(`value`, value);
      setBox6(value);
    } else if (type === "examiner") {
      console.log(`value11`, value);
      setBox7(value);
    
    } else if (type === "jobassociated") {
      console.log(`value`, value);
      setBox4(value);
    } else if (type === "contact number") {
      console.log(`value`, value);
      setBox2(value);
    } else if (type === "examassociated") {
      console.log(`value`, value);
      setBox2(typeof value === "string" ? value.split(",") : value);
    } else if (type === "experiencelevel") {
      console.log(`value`, value);
      setBox3(value);
    }
  };
  const cancel = () => {
     if (props.data.fieldType === "AddJob") {
      const data1 = {
        job_title: box1,
        exam_associated: box2.slice(1),
        job_level: box3,
        department: box5,
        is_publish: false,
        status: "draft",
      };
      fetchPost
        .post(`${apiUrl.addJob}`, data1)
        .then((res) => {
          setJobToggle(true);
          setBox1("");
          setBox2([]);
          setBox3("Level");
          setBox4("");
          setBox5("department");
        })
        .catch((error) => {
          setJobToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    }
    else
    {
      history.goBack()
    }
  };
  const add = () => {
    setLoader(false);
    if (props.data.fieldType === "AddCandidate") {
      const data1 = {
        full_name: box1,
        email: box3,
        phone_number: box2,
        department: box5,
        job: box4.id,
        examiner: box7.id,
        experience: 0,
      };
      fetchPost
        .post(`${apiUrl.addCandidate}`, data1)
        .then((res) => {
          setLoader(true);
          setCandidateToggle(true);
          setBox1("");
          setBox2("");
          setBox3("");
          setBox4("jobs");
          setBox5("department");
          setBox6("");
          setBox7("examiner");
        })
        .catch((error) => {
          setLoader(true);
          setCandidateToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "AddJob") {
      const data1 = {
        job_title: box1,
        exam_associated: box2,
        job_level: box3.toLowerCase(),
        experience: box4,
        department: box5.id,
        is_publish: true,
        status: "publish",
      };
      fetchPost
        .post(`${apiUrl.addJob}`, data1)
        .then((res) => {
          setLoader(true);
          setJobToggle(true);
          setBox1("");
          setBox2([]);
          setBox3("Level");
          setBox4("");
          setBox5("department");
        })
        .catch((error) => {
          setLoader(true);
          setJobToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "AddExaminer") {
      const data1 = {
        full_name: box1,
        phone_number: box2,
        email: box3,
        designation: box4,
        department: box5.id,
      };
      fetchPost
        .post(`${apiUrl.inviteExaminer}`, data1)
        .then((res) => {
          setExaminerToggle(true);
          setLoader(true)
        })
        .catch((error) => {
          setLoader(true);
          setExaminerToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "AddDepartment") {
      const data1 = {
        name: box1,
      };
      fetchPost
        .post(`${apiUrl.viewDepartments}`, data1)
        .then((res) => {
          setLoader(true);
          setDepartmentToggle(true);
          setBox1("");
        })
        .catch((error) => {
          setLoader(true);
          setDepartmentToggle(false);
        });
    } else if (props.data.fieldType === "addSubadmin") {
      const data1 = {
        full_name: box1,
        email: box3,
        designation: box4,
        permission: subAdminPermissions
      }
      fetchPost
        .post(`${apiUrl.addSubadmin}`, data1)
        .then(() => {
          setLoader(true);
          setSubAdminToggle(true);
          setBox1("");
          setBox3("");
          setBox4("");
        }).catch((error) => {
          setLoader(true);
          setSubAdminToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    }
  };
  const edit = () => {
    if (props.data.fieldType === "EditCandidate") {
      var deptid;
      const data1 = {
        full_name: box1,
        email: box3,
        phone_number: box2,
        department: box5 ? box5 : deptid,
        job: box4.id,
        examiner: box7.id,
        experience: box6,
      };
      fetchUpdate
        .patch(`${apiUrl.editCandidate}${candidateID}/`, data1)
        .then((res) => {
          setUpdateCandidateToggle(true);
          setBox1("");
          setBox2("");
          setBox3("");
          setBox4("jobs");
          setBox5("department");
          setBox6("");
          setBox7("examiner");
        })
        .catch((error) => {
          setUpdateCandidateToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "EditJob") {
      const data1 = {
        job_title: box1,
        exam_associated: box2,
        job_level: box3.toLowerCase(),
        experience: box4,
        department: box5.id,
      };
      fetchUpdate
        .patch(`${apiUrl.editJob}${jobID}`, data1)
        .then((res) => {
          setUpdateJobToggle(true);
          setBox1("");
          setBox2([]);
          setBox3("Level");
          setBox4("");
          setBox5("department");
        })
        .catch((error) => {
          setUpdateJobToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "EditExaminer") {
      const data1 = {
        full_name: box1,
        phone_number: box2,
        email: box3,
        department: box5.id,
        designation: box4,
      };
      fetchUpdate
        .patch(`${apiUrl.updateExaminer}/` + examinerID, data1)
        .then((res) => {
          setUpdateExaminerToggle(true);
          setBox1("");
          setBox2("");
          setBox3("");
          setBox4("");
          setBox5("");
        })
        .catch((error) => {
          setUpdateExaminerToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "EditDepartment") {
      const data1 = {
        name: box1,
      };
      fetchUpdate
        .patch(`${apiUrl.viewDepartments}${departmentID}/`, data1)
        .then((res) => {
          setUpdateDepartmentToggle(true);
          setBox1("");
        })
        .catch((error) => {
          setUpdateDepartmentToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    } else if (props.data.fieldType === "editSubadmin") {
      const data1 = {
        full_name: box1,
        email: box3,
        designation: box4,
        department: box5,
        permission: subAdminPermissions
      };
      fetchUpdate
        .patch(`${apiUrl.editSubadmin}${props.data.SubAdminID}/`, data1)
        .then((res) => {
          setUpdateSubAdminToggle(true);
          setBox1("");
          setBox3("");
          setBox4(""); 
        })
        .catch((error) => {
          setUpdateDepartmentToggle(false);
          if (error.response) {
            if (error.response.data.code === 404) {
              props.enqueueSnackbar("Oops! Something went wrong. We are working on fixing the problem.", {
                variant: "error",
                 autoHideDuration:3000,
              });
            } else if (error.response.data.error) {
              error.response.data.error.message.forEach((message) => {
                props.enqueueSnackbar(message, {
                  variant: "error",
                  autoHideDuration: 3000,
                });
              });
            }
          } else {
            props.enqueueSnackbar("Oops! Something went wrong. Please check your network connection and try again", {
              variant: "error",
            });
          }
        });
    }
  };
  const handleJobChange=(newValue)=>{
    setBox4(newValue)
    fetchClient.get(`${apiUrl.jobProfile}${newValue.id}/profile/`).then((res) => {
      const dept = res.data.data.department.id
      fetchClient.get(`${apiUrl.examinerList}?page_size=1000&department=${dept}`).then((res) => {
        setExaminerlist(res && res.data && res.data.data && res.data.data.results);
        setBox7(res.data.data.results?.find((e) => e.full_name === props.data.DetailBox.box7));
      });
    })
  }
  const addSubAdminPermission = (e, permissionName) => {
    let permissions = [...subAdminPermissions];
    let allowedPermissions = {...allowedSubAdminPermissions};
    let permissionsData = [];
    permissionsData.push(permissionLists.find((data) => data.permission === e.target.value));
    permissionsData.push(permissionLists.find((data) => data.permission === permissionName));
    if(permissions.includes(permissionsData[0].id)){
      permissions.splice(permissions.indexOf(permissionsData[0].id), 1);
      allowedPermissions[permissionsData[0].permission] = false;
    }else{
      permissions.push(permissionsData[0].id);
      allowedPermissions[permissionsData[0].permission] = true;
      if(!allowedPermissions[permissionsData[1].permission]){
        permissions.push(permissionsData[1].id);
        allowedPermissions[permissionsData[1].permission] = true;
      }
    }
    setsubAdminPermissions([...permissions]); 
    setAllowedSubAdminPermissions({...allowedPermissions});
  }
  const removeSubAdminPermission = (e, permissionNames) => {
    let permissions = [...subAdminPermissions];
    let allowedPermissions = {...allowedSubAdminPermissions};
    let permissionsDataSelected = permissionLists.find((data) => data.permission === e.target.value);
    let permissionDataOther = [];
    permissionNames.map((permissionName) => {
      permissionDataOther.push(permissionLists.find((data) => data.permission === permissionName));
    })
    if(permissions.includes(permissionsDataSelected.id)){
      permissions.splice(permissions.indexOf(permissionsDataSelected.id), 1);
      allowedPermissions[permissionsDataSelected.permission] = false;
      permissionDataOther.map((data) => {
        if(permissions.includes(data.id)){
          permissions.splice(permissions.indexOf(data.id), 1);
          allowedPermissions[data.permission] = false;
        }
      })
    } else{
      permissions.push(permissionsDataSelected.id);
      allowedPermissions[permissionsDataSelected.permission] = true;
    }
    setsubAdminPermissions([...permissions]); 
    setAllowedSubAdminPermissions({...allowedPermissions});
  }
  useEffect(() => {
    const initializePermissions = (allPermissions) => {
      if(props.data.fieldType === "editSubadmin"){
        let array = [];
        for(let p of props.data.Permissions){
          for(let data of allPermissions) {
            if(data.permission === p){
              array.push(data.id);
            }
          }
        }
        setsubAdminPermissions([...array]);
        let allowedPermissions = {...allowedSubAdminPermissions};
        props.data.Permissions.map((e) => {
          allowedPermissions[e] = true;
        })
        setAllowedSubAdminPermissions(allowedPermissions);
      }
    }
    fetchClient.get(`${apiUrl.permission}`).then((res) => {
      setpermissionlist(res && res.data && res.data.data);
      initializePermissions(res.data.data);
    });
  },[])
  useEffect(() => {
    setBox1(props && props.data && props.data.DetailBox && props.data.DetailBox.box1);
    setBox2(props && props.data && props.data.DetailBox && props.data.DetailBox.box2);
    setBox3(props && props.data && props.data.DetailBox && props.data.DetailBox.box3);
    setBox4(props && props.data && props.data.DetailBox && props.data.DetailBox.box4);
    setBox6(props && props.data && props.data.DetailBox && props.data.DetailBox.box6);
    fetchClient.get(`${apiUrl.deptList}?page_size=1000`).then((res) => {
      setDeptlist(res && res.data && res.data.data && res.data.data.results);
      setBox5(res.data.data.results?.find((e) => e.name === props.data.DetailBox.box5));
    });
    fetchClient.get(`${apiUrl.jobList}?page_size=1000&status=publish`).then((res) => {
      setJoblist(res && res.data && res.data.data && res.data.data.results);
      setBox4(res.data.data.results?.find((e) => e.job_title === props.data.DetailBox.box4));
    });
   if(DeptID )
   {
   fetchClient.get(`${apiUrl.examinerList}?page_size=1000&department=${DeptID}`).then((res) => {
      setExaminerlist(res && res.data && res.data.data && res.data.data.results);
      setBox7(res.data.data.results?.find((e) => e.full_name === props.data.DetailBox.box7));
    })
  }
   
  }, []); 
  return (
      <Container maxWidth="xl" className={classes.EditContainer}>
        <Grid container xs={12} style={{ paddingBottom: "10px" }}>
          <Grid item lg={8} md={12} align="left" justify="left">
            <div className={classes.page_heading}>{heading}</div>
          </Grid>
          <Grid item lg={4} md={12} className={classes.actionbutton}>
            <div className="button_invite_examiner" style={{ paddingRight: "80px" }}>
              <Button onClick={cancel} className={classes.outlinedButton} variant="contained" color="primary">
                {button1}
              </Button>
            </div>
            <div className="button_invite_examiner">
              {props.data.field === "Add" ? (
                <Button onClick={add} className={classes.filledButton} variant="contained" color="primary">
                  {button2}
                </Button>
              ) : (
                <Button onClick={edit} className={classes.filledButton} variant="contained" color="primary">
                  {button2}
                </Button>
              )}
            </div>
          </Grid>
        </Grid>
        {subAdminToggle ? <SuccessDialog type="field" heading="Sub Admin Added" path={`/${role}/subadmins`} /> : null}
        {examinerToggle ? <SuccessDialog type="field" heading="Invitation Sent" path={`/${role}/examiner`} /> :   null}
        {departmentToggle ? <SuccessDialog type="field" heading="Department Added" path={`/${role}/departments`} /> : null}
        {jobToggle ? <SuccessDialog type="field" heading="Job Published" path={`/${role}/job`} /> : null}
        {candidateToggle ? <SuccessDialog type="field" heading="Candidate Added" path={`/${role}/candidates`} /> : null}
        {UpdateexaminerToggle ? <SuccessDialog type="field" heading="Examiner Updated Successfully" path={`/${role}/examiner`} /> : null}
        {UpdatejobToggle ? <SuccessDialog type="field" heading="Job Updated Successfully" path={`/${role}/job`} /> : null}
        {UpdatecandidateToggle ? <SuccessDialog type="field" heading="Candidate Updated Successfully" path={`/${role}/candidates`} /> : null}
        {UpdateDepartmentToggle ? <SuccessDialog type="department" heading="Department Updated Successfully" /> : null}
        {UpdateSubAdminToggle ? <SuccessDialog type="field" heading="Sub Admin Updated Successfully" path={`/${role}/subadmins`} /> : null}
        <Grid container xs={12}>
          <Grid item xs={12} sm={6} align="left" justify="left">
            <div className={classes.page_subheading} style={{ width: "121%" }}>
              <Link className={clsx(classes.page_subheading, classes.LinkStyle)} to={{ pathname: props.data.home }}>
                {subheading1}
              </Link>{" "}
              <span className="spc"></span> &#62; <span className="spc"></span>
              {subheading2}
              {props.data.fieldType === "editSubadmin" ? (
                <>
                  {" "}
                  <span className="spc"></span> &#62; <span className="spc"></span>
                  {subheading3}
                </>
              ) : null}
            </div>
          </Grid>
        </Grid>
        {!loader ? <Paper className={classes.paper} style={{height:props.data.height,justifyContent:'center',alignItems:'center'}} >
            <CircularProgress color="error"/></Paper> :
        <Paper className={classes.paper} style={{ height: props.data.height, overflow: "hidden" }}>
          <div className={classes.paper_heading}>{paperheading}</div>
          <Grid container style={{ marginTop: "24px", marginLeft: "32px" }} xs={12}>
            <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left">
              <div className="deptSelect_wrapper">
                <div className="examiner_detail_wrapper">
                  <div className={classes.examiner_detail_label}>
                    <label className={classes.exam_detail_label_font}>
                      {label1}
                      <span className={classes.mandotory}>*</span>{" "}
                    </label>
                  </div>
                  <div className={classes.examiner_detail_box}>
                    <input onChange={(e) => editExaminerDetails(e, "fullname")} value={box1} placeholder={ph1}    className={classes.examiner_box} type="text"></input>
                  </div>
                </div>
              </div>
            </Grid>
            {props.data.Type === "Department" ? null : (
              <>
                {props.data.Type === "Subadmin" || props.data.Type === "Job"? null : (
                  <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left">
                    <div className="examiner_detail_wrapper">
                      <div className={classes.examiner_detail_label}>
                        <label>{label2}</label>
                      </div>
                      <div className={classes.examiner_detail_box}>
                        <input  onChange={(e) => editExaminerDetails(e, "contact number")} value={box2} placeholder={ph2}  className={classes.examiner_box} type="text" maxlength="13" required></input>
                      </div>
                    
                    </div>
                  </Grid>
                )}
                <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left">
                  <div className="examiner_detail_wrapper">
                    <div className={classes.examiner_detail_label}>
                      <label>
                        {label3} <span className={classes.mandotory}>*</span>
                      </label>
                    </div>
                    {props.data.Type === "Job" ? (
                      <FormControl>
                        <Autocomplete
                        disableClearable
                          input={<OutlinedInput /> }
                          value={box3}
                          options={Level}
                          style={{marginTop:'10px',
                          width: "338px"}}                  
                          getOptionLabel={option => option || ""}
                          defaultValue = {JobLevel}
                          onChange={(e, newValue) => newValue ? setBox3(newValue):null }
                          renderInput={params => (
                            <TextField
                              {...params}
                              variant="outlined"
                            />)}
                        />
                      </FormControl>
                    ) : (
                      <div className={classes.examiner_detail_box}>
                        <input onChange={(e) => editExaminerDetails(e, "email")} value={box3} placeholder={ph3} className={classes.examiner_box} type="text"></input>
                      </div>
                    )}
                  </div>
                </Grid>
               { props.data.Type === "Job" ? null: <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left">
                  <div className="examiner_detail_wrapper">
                    <div className={classes.examiner_detail_label}>
                      <label>
                        {label4} <span className={classes.mandotory}>*</span>
                      </label>
                    </div>
                    {props.data.Type === "Candidate" ? (
                      <FormControl>
                        <Autocomplete
                        disableClearable
                          input={<OutlinedInput /> }
                            value={box4 ? jobLists?.find((e) => e.id === box4?.id) : null }
                            options={jobLists}
                            style={{marginTop:'10px',
                            width: "338px",zIndex:10}}
                            getOptionLabel={option => capitalize(option.job_title) || ""}
                            defaultValue = {candidateJob}
                            onChange={(e, newValue) => newValue ? handleJobChange(newValue):null }
                            renderInput={params => (
                              <TextField
                                {...params}
                                variant="outlined"
                          />)}
                      />
                    </FormControl> 
                    ) : (
                      <div className={classes.examiner_detail_box}>
                        <input onChange={(e) => editExaminerDetails(e, "designation")} value={box4} placeholder={ph4} className={classes.examiner_box} type="text"></input>
                      </div>
                    )}
                  </div>
                </Grid>}
                {props.data.Type === "Candidate" || props.data.Type === "Subadmin" ? null:<Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left">
                  <div className="examiner_detail_wrapper">
                    <div className={classes.examiner_detail_label}>
                      <label>
                        {label5}
                        <span className={classes.mandotory}>*</span>
                      </label>
                    </div>
                    <FormControl>
                      <Autocomplete
                      disableClearable
                        input={<OutlinedInput /> }
                          value={box5 ? deptLists?.find((e) => e.id === box5?.id) : null}
                          options={deptLists}
                          style={{marginTop:'10px',
                            width: "338px"}}
                          getOptionLabel={option => capitalize(option.name) || ""}
                          onChange={(e, newValue) => newValue ? setBox5(newValue):null }
                          renderInput={params => (
                            <TextField
                              {...params}
                              variant="outlined"
                         />)}
                      />
                    </FormControl> 
                  </div>
                </Grid>}
                {props.data.Type === "Candidate" ? (
                  <>
                    <Grid item xl={4} lg={4} xs={12} sm={6} align="left" justify="left">
                      <div className="examiner_detail_wrapper">
                        <div className={classes.examiner_detail_label}>
                          <label>
                            {label7} <span className={classes.mandotory}>*</span>
                          </label>
                        </div>
                        <FormControl>
                          <Autocomplete
                          disableClearable
                            input={<OutlinedInput /> }
                              value={box7 ? examinerLists?.find((e)=>e.id === box7?.id) : null }
                              options={examinerLists}
                            style={{marginTop:'10px',
                            width: "338px"}}
                            getOptionLabel={option => option.full_name || ""}
                            onChange={(e, newValue) => newValue ? setBox7(newValue):null }
                            renderInput={params => (
                              <TextField
                                {...params}
                                variant="outlined"
                            />)}
                          />
                        </FormControl> 
                      </div>
                    </Grid>
                  </>
                ) : null}
              </>
            )}
          </Grid>
        </Paper> }
        {(role === "sub-admin" && props.data.fieldType === "editSubadmin" && !props.data.SubAdminUserPermission.givePermissions) ? null:
          <>{props.data.Type === "Subadmin" ? (
          <>
            <Paper className={classes.paper}>
              <div className={classes.paper_heading}>Permissions</div>
              <div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Candidates</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View Profile</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["VIEW_CANDIDATE"] ? "checked" : ''}
                          onChange={(e) => removeSubAdminPermission(e, 
                            ["CREATE_CANDIDATE",
                            "EDIT_CANDIDATE", 
                            "DELETE_CANDIDATE",
                            "GENERATE_EXAM_LINK",
                            "HIRE_REJECT_CANDIDATE"])}
                          value="VIEW_CANDIDATE"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Create</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["CREATE_CANDIDATE"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_CANDIDATE")}
                          value="CREATE_CANDIDATE"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["EDIT_CANDIDATE"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_CANDIDATE")}
                          value="EDIT_CANDIDATE"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["DELETE_CANDIDATE"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_CANDIDATE")}
                          value="DELETE_CANDIDATE"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Generate Exam Link</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["GENERATE_EXAM_LINK"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_CANDIDATE")}
                          value="GENERATE_EXAM_LINK"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Reject/Hire</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["HIRE_REJECT_CANDIDATE"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_CANDIDATE")}
                          value="HIRE_REJECT_CANDIDATE"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Examiners</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View Profile</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["VIEW_EXAMINER"] ? "checked" : ''}
                          onChange={(e) => removeSubAdminPermission(e, ["CREATE_EXAMINER", "EDIT_EXAMINER", "DELETE_EXAMINER"])}
                          value="VIEW_EXAMINER"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Invite</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["CREATE_EXAMINER"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_EXAMINER")}
                          value="CREATE_EXAMINER"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["EDIT_EXAMINER"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_EXAMINER")}
                          value="EDIT_EXAMINER"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["DELETE_EXAMINER"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_EXAMINER")}
                          value="DELETE_EXAMINER"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Exams</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["VIEW_EXAM"] ? "checked" : ''}
                          onChange={(e) => removeSubAdminPermission(e, ["CREATE_EXAM", "EDIT_EXAM", "DELETE_EXAM"])}
                          value="VIEW_EXAM"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Create</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["CREATE_EXAM"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_EXAM")}
                          value="CREATE_EXAM"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["EDIT_EXAM"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_EXAM")}
                          value="EDIT_EXAM"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete</div>}
                      control={
                        <Checkboxblack
                          checked={allowedSubAdminPermissions["DELETE_EXAM"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_EXAM")}
                          value="DELETE_EXAM"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Question Bank</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["VIEW_QUESTION_BANK"] ? "checked" : ''}
                        onChange={(e) => removeSubAdminPermission(e, ["CREATE_QUESTION_BANK", "EDIT_QUESTION_BANK", "DELETE_QUESTION_BANK"])}
                          value="VIEW_QUESTION_BANK"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Create Question</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["CREATE_QUESTION_BANK"] ? "checked" : ''}
                        onChange={(e) => addSubAdminPermission(e, "VIEW_QUESTION_BANK")}
                          value="CREATE_QUESTION_BANK"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit Question</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["EDIT_QUESTION_BANK"] ? "checked" : ''}
                        onChange={(e) => addSubAdminPermission(e, "VIEW_QUESTION_BANK")}
                          value="EDIT_QUESTION_BANK"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete Question</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["DELETE_QUESTION_BANK"] ? "checked" : ''}
                        onChange={(e) => addSubAdminPermission(e, "VIEW_QUESTION_BANK")}
                          value="DELETE_QUESTION_BANK"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Results</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["VIEW_RESULT"] ? "checked" : ''}
                        onChange={(e) => removeSubAdminPermission(e, ["PASS_FAIL_CANDIDATE"])}
                          value="VIEW_RESULT"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Pass/Fail</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["PASS_FAIL_CANDIDATE"] ? "checked" : ''}
                        onChange={(e) => addSubAdminPermission(e, "VIEW_RESULT")}
                          value="PASS_FAIL_CANDIDATE"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Vacancies</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View Vacancies </div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["VIEW_JOB"] ? "checked" : ''}
                        onChange={(e) => removeSubAdminPermission(e, ["CREATE_JOB", "EDIT_JOB", "DELETE_JOB"])}
                          value="VIEW_JOB"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Create Vacancy </div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["CREATE_JOB"] ? "checked" : ''}
                        onChange={(e) => addSubAdminPermission(e, "VIEW_JOB")}
                          value="CREATE_JOB"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit Vacancy </div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["EDIT_JOB"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_JOB")}
                          value="EDIT_JOB"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete Vacancy</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["DELETE_JOB"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_JOB")}
                          value="DELETE_JOB"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Departments</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["VIEW_DEPARTMENT"] ? "checked" : ''}
                          onChange={(e) => removeSubAdminPermission(e, ["CREATE_DEPARTMENT", "EDIT_DEPARTMENT", "DELETE_DEPARTMENT"])}
                          value="VIEW_DEPARTMENT"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Create</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["CREATE_DEPARTMENT"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_DEPARTMENT")}
                          value="CREATE_DEPARTMENT"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["EDIT_DEPARTMENT"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_DEPARTMENT")}
                          value="EDIT_DEPARTMENT"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["DELETE_DEPARTMENT"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_DEPARTMENT")}
                          value="DELETE_DEPARTMENT"
                        />
                      }
                    />
                  </div>
                </div>
                <div className={classes.permissionContainer}>
                  <Typography className={classes.section_heading}>Sub Admins</Typography>
                  <div className={classes.checkboxContainer}>
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>View</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["VIEW_SUBADMIN"] ? "checked" : ''}
                          onChange={(e) => removeSubAdminPermission(e, ["CREATE_SUBADMIN", "EDIT_SUBADMIN", "DELETE_SUBADMIN", "GIVE_PERMISSIONS"])}
                          value="VIEW_SUBADMIN"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Create</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["CREATE_SUBADMIN"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_SUBADMIN")}
                          value="CREATE_SUBADMIN"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Edit</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["EDIT_SUBADMIN"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_SUBADMIN")}
                          value="EDIT_SUBADMIN"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Delete</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["DELETE_SUBADMIN"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_SUBADMIN")}
                          value="DELETE_SUBADMIN"
                        />
                      }
                    />
                    <FormControlLabel
                      style={{ marginRight: "51px" }}
                      label={<div className={classes.label}>Give Permission</div>}
                      control={
                        <Checkboxblack
                        checked={allowedSubAdminPermissions["GIVE_PERMISSIONS"] ? "checked" : ''}
                          onChange={(e) => addSubAdminPermission(e, "VIEW_SUBADMIN")}
                          value="GIVE_PERMISSIONS"
                        />
                      }
                    />
                  </div>
                </div>
              </div>
            </Paper>
          </>
        ) : null}</>}
        <Box pt={2}>
          <Copyright />
        </Box>
      </Container>
  );
};
export default withSnackbar(Commonfieldscreen)