import { fetchDelete, apiUrl, useHistory, CssBaseline, makeStyles, React, clsx, withStyles, MenuItem, Menu, AppBar, Toolbar, ArrowDropDownIcon, Button } from "allImport";
import "style/style.css";
import { MouseEnter, MouseLeave } from "Common/CommonFunction";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  profile_button: {
    marginLeft: "22px",
  },
  toolbar: {
    height: "12px",
    paddingRight: 24,
    paddingTop: "32px",
    width: "100%",
    display: "flex",
    justifyContent: "flex-end",
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  topProfileAction: {
    width: "15%",
    marginLeft: "38px",
    justifyContent: "flex-end",
  },

  profileButton: {
    background: "#fafafa",

    border: "1px solid #E5E5E5",
    boxSizing: "border-box",
    borderRadius: "4px",
    margin: "0",
    width: "200px",
    height: "49px",
    whiteSpace: "nowrap",

    textAlign: "left",
  },

  formControl: {
    width: "100%",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: "transparent",
    boxShadow: "none",
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `100px`,
    height:'73px',
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },

  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  notification: {
    padding: 8,
    marginRight: 32.33,
    borderRadius: "50%",
    marginTop:'10px',
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "rgba(0, 0, 0, 0.04)",
    },
  },
}));

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

export default function Dashboard(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const adminID = localStorage.getItem("ADMIN_ID");
  const adminName = localStorage.getItem("ADMIN_NAME");
  const role = localStorage.getItem("ROLE")?localStorage.getItem("ROLE").toLowerCase():null;
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const logout = () => {
    fetchDelete.delete(`${apiUrl.logout}`);
    localStorage.clear()
    history.push("/login");
  };
  const changepassword = () => {
    history.push(`/${role}/change-password`);
  };
  const myprofile = () => {

    history.push({
      pathname:`/${role}/profile`,
      state: adminID,
    });
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const MuiMenuItem = withStyles({
    root: {
      justifyContent: "left",
    },
  })(MenuItem);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" elevation={0} className={clsx("topBar_width", classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <div onClick={() => history.push(`/${role}/notifications`)} className={classes.notification}>
            {props.notification ? <img src="/assets/Icons/active_notification.svg" alt="" /> : <img src="/assets/Icons/notification.svg" alt=""/>}
          </div>

          <div className="profile_button">
            <Button color="#fafafa" className={classes.profileButton} onClick={handleClick} endIcon={<ArrowDropDownIcon />}>
              <span>{adminName}</span>
            </Button>

            <StyledMenu id="customized-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
              <MuiMenuItem
                onMouseEnter={(e) => {
                  MouseEnter(e);
                }}
                onMouseLeave={(e) => {
                  MouseLeave(e);
                }}
                onClick={myprofile}
              >
                My Profile
              </MuiMenuItem>
              <MuiMenuItem
                onMouseEnter={(e) => {
                  MouseEnter(e);
                }}
                onMouseLeave={(e) => {
                  MouseLeave(e);
                }}
                onClick={changepassword}
              >
                Change password
              </MuiMenuItem>
              <MuiMenuItem
                onMouseEnter={(e) => {
                  MouseEnter(e);
                }}
                onMouseLeave={(e) => {
                  MouseLeave(e);
                }}
                onClick={logout}
              >
                Logout
              </MuiMenuItem>
            </StyledMenu>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
