import Dashboard from "./containers/hr-examiner/Dashboard/dashboard";
import Examiner from "./containers/hr-examiner/Examiner/examiner";
import Invite from "./containers/hr-examiner/Examiner/inviteExaminer";
import ExaminerProfile from "./containers/hr-examiner/Examiner/examinerProfile";
import EditProfile from "./containers/hr-examiner/Examiner/editExaminer";
import ForgotPassword from "./containers/hr-examiner/Forgotpassword/Forgotpassword";
import Login from "./containers/hr-examiner/Login/LogIn";
import AdminProfile from "./containers/hr-examiner/Admin/AdminProfile";
import EditAdmin from "./containers/hr-examiner/Admin/editAdmin";
import AdminChangePassword from "./containers/hr-examiner/Admin/AdminChangePassword";
import Examlist from "./containers/hr-examiner/Exams/Examlist";
import Createexam from "./containers/hr-examiner/Exams/Createexam";
import Resetpassword from "./containers/hr-examiner/Resetpassword/Resetpassword";
import ExamquestionList from "./containers/hr-examiner/Exams/ExamquestionList";
import SelectedQuestionlist from "./containers/hr-examiner/Exams/SelectedQuestionlist";
import Questionbank from "./containers/hr-examiner/Questionsbank/questionsbank";
import Editjob from "./containers/hr-examiner/Jobs/Editjob";
import Createvacancy from "./containers/hr-examiner/Jobs/Createvacancy";
import Joblist from "./containers/hr-examiner/Jobs/Joblist";
import Jobprofile from "./containers/hr-examiner/Jobs/Jobprofile";
import AdminEmptyScreen from "./containers/hr-examiner/Admin/AdminEmptyScreen";
import CandidateList from "containers/hr-examiner/candidates/CandidateList";
import CandidateProfile from "containers/hr-examiner/candidates/CandidateProfile";
import AddCandidate from "containers/hr-examiner/candidates/AddCandidate";
import EditCandidate from "containers/hr-examiner/candidates/EditCandidate";
import ExamLink from "containers/hr-examiner/candidates/ExamLink";
import CandidateGallery from "containers/hr-examiner/candidates/CandidateGallery";
import CandidateResult from "containers/hr-examiner/Results/CandidateResult";
import ExamCandidatelist from "containers/hr-examiner/Results/ExamCandidateList";
import SubmittedExamlist from "containers/hr-examiner/Results/SubmittedExamsList";
import QuestionList from "containers/hr-examiner/Questionsbank/QuestionList";
import QuestionBankEditQuestion from "containers/hr-examiner/Questionsbank/QuestionBankEditQuestion";
import AddNewQuestion from "containers/hr-examiner/Questionsbank/AddNewQuestion";
import SubadminList from "containers/hr-examiner/Subadmins/SubadminList";
import CreateSubadmin from "containers/hr-examiner/Subadmins/CreateSubadmin";
import EditSubadmin from "containers/hr-examiner/Subadmins/EditSubadmin";
import SubadminProfile from "containers/hr-examiner/Subadmins/SubadminProfile";
import CreateDepartment from "containers/hr-examiner/Department/createDepartment";
import EditDepartment from "containers/hr-examiner/Department/editDepartment";
import DepartmentList from "containers/hr-examiner/Department/DepartmentList";
import ViewDepartment from "containers/hr-examiner/Department/viewDepartment";
import Editexam from "containers/hr-examiner/Exams/EditExam";
import NotificationList from "containers/hr-examiner/Notification/NotificationList";


export const menuItem1 = [
  { path: "/", component: Login },
  { path: "/login", component: Login },
  { path: "/forgot-password", component: ForgotPassword },
  { path: "/reset-password/:id", component: Resetpassword },
]
export const menuItem = [
 
  
  { path: `/dashboard`, component: Dashboard },
  { path: '/examiner', component: Examiner },
  { path: "/examiner/invite", component: Invite },
  { path: "/examiner/profile", component: ExaminerProfile },
  { path: "/examiner/edit-profile", component: EditProfile },
  { path: "/profile", component: AdminProfile },
  { path: "/edit-profile", component: EditAdmin },
  { path: "/change-password", component: AdminChangePassword },
  { path: `/exam`, component: Examlist },
  { path: `/exam/create`, component: Createexam },
  { path: `/exam/edit`, component: Editexam},
  { path: `/exam/question/select-question`, component: SelectedQuestionlist },
  { path: `/exam/question`, component: ExamquestionList },
  { path: "/question-bank", component: Questionbank },
  { path: "/question-bank/questions", component: QuestionList },
  { path: "/question-bank/questions/edit", component: QuestionBankEditQuestion },
  { path: "/question-bank/questions/add", component: AddNewQuestion },
  { path: "/job", component: Joblist },
  { path: "/job/edit", component: Editjob },
  { path: "/job/create", component: Createvacancy },
  { path: "/job/profile", component: Jobprofile },
  { path: "/department/create", component: CreateDepartment},
  { path: "/department/edit", component: EditDepartment},
  { path: "/department/view", component: ViewDepartment},
  { path: "/department", component: DepartmentList},
  { path: "/subadmins", component: SubadminList },
  { path: "/subadmins/create-subadmin", component: CreateSubadmin },
  { path: "/subadmins/edit-subadmin", component:EditSubadmin },
  { path: "/subadmins/profile", component:SubadminProfile },
  { path: "/emptyscreen", component: AdminEmptyScreen },
  { path: "/candidate", component: CandidateList },
  { path: "/candidate/profile", component: CandidateProfile },
  { path: "/candidate/add", component: AddCandidate},
  { path: "/candidate/edit", component:EditCandidate},
  { path: "/candidate/examlink", component: ExamLink},
  { path: "/candidate/profile/gallery", component: CandidateGallery},
  { path: "/submitted-exam/result", component: CandidateResult},
  { path: "/submitted-exam", component: ExamCandidatelist},
  { path: "/result/exam/list", component: SubmittedExamlist},
  { path: "/notifications", component: NotificationList},
  

];

export const menuItemExaminer = [
  { path: `/dashboard`, component: Dashboard },
  { path: "/profile", component: AdminProfile },
  { path: "/edit-profile", component: EditAdmin },
  { path: "/change-password", component: AdminChangePassword },
  { path: `/exam`, component: Examlist },
  { path: `/exam/create`, component: Createexam },
  { path: `/exam/edit`, component: Editexam},
  { path: `/exam/question/select-question`, component: SelectedQuestionlist },
  { path: `/exam/question`, component: ExamquestionList },
  { path: "/question-bank", component: Questionbank },
  { path: "/question-bank/questions", component: QuestionList },
  { path: "/question-bank/questions/edit", component: QuestionBankEditQuestion },
  { path: "/question-bank/questions/add", component: AddNewQuestion },
  { path: "/emptyscreen", component: AdminEmptyScreen },
  { path: "/candidate", component: CandidateList },
  { path: "/candidate/profile", component: CandidateProfile },
  { path: "/candidate/add", component: AddCandidate},
  { path: "/candidate/edit", component:EditCandidate},
  { path: "/candidate/examlink", component: ExamLink},
  { path: "/candidate/profile/gallery", component: CandidateGallery},
  { path: "/submitted-exam/result", component: CandidateResult},
  { path: "/submitted-exam", component: ExamCandidatelist},
  { path: "/result/exam/list", component: SubmittedExamlist},
  { path: "/notifications", component: NotificationList},
];